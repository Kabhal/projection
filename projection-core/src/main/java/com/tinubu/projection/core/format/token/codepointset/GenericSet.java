/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token.codepointset;

import static java.util.stream.Collectors.toSet;

import java.util.Iterator;
import java.util.Set;

import com.tinubu.commons.ddd2.invariant.Validate.Check;

/**
 * Generic code point set based on specified character set.
 * <p>
 * Specified characters can contain Unicode surrogate pairs that will be correctly managed as a single code
 * point.
 */
public class GenericSet extends CodePointSet {

   protected final Set<Integer> set;

   protected GenericSet(Set<Integer> set) {
      this.set = Check.notNull(set, "set");
   }

   public static GenericSet of(Set<Integer> set) {
      return new GenericSet(set);
   }

   public static GenericSet of(String characters) {
      return new GenericSet(codePointSet(characters));
   }

   protected static Set<Integer> codePointSet(String characters) {
      if (characters == null) {
         return null;
      }
      return characters.codePoints().boxed().collect(toSet());
   }

   @Override
   public boolean containsCodePoint(int i) {
      return set.contains(i);
   }

   @Override
   public Iterator<Integer> iterator() {
      return set.iterator();
   }

   @Override
   public int size() {
      return set.size();
   }
}
