/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.projection.core.format.token.matcher.TokenMatcher;

/**
 * Default tokenizer. Splits input in tokens, each one associated with first matching {@link TokenMatcher}.
 * <p>
 * If a text section does not match any token matcher, it is discarded automatically.
 */
public class DefaultTokenizer extends IteratorTokenizer {
   private final List<TokenMatcher> tokenMatchers;

   protected DefaultTokenizer(List<TokenMatcher> tokenMatchers) {
      this.tokenMatchers = Check.noNullElements(tokenMatchers, "tokenMatchers");
   }

   public static DefaultTokenizer of(List<TokenMatcher> tokenMatchers) {
      return new DefaultTokenizer(tokenMatchers);
   }

   public static DefaultTokenizer of(TokenMatcher... tokenMatchers) {
      return new DefaultTokenizer(list(tokenMatchers));
   }

   public List<TokenMatcher> tokenMatchers() {
      return tokenMatchers;
   }

   @Override
   protected Iterator<Token> iterator(String input, int fromIndex, int toIndex) {
      return new TokenIterator(input, fromIndex, toIndex);
   }

   /**
    * Tokenizes the given input and returns an iterator of matched tokens among specified token classes.
    * Characters that does not match any specified token class are discarded.
    */
   public class TokenIterator implements Iterator<Token> {
      protected final String input;
      protected final int toIndex;

      protected int fromIndex;
      protected Boolean hasNext;
      protected Token next;

      protected TokenIterator(String input, int fromIndex, int toIndex) {
         this.input = Check.notNull(input, "input");
         this.fromIndex = Check.validate(fromIndex, "fromIndex", isPositive());
         this.toIndex = Check.validate(toIndex,
                                       "toIndex",
                                       isGreaterThanOrEqualTo(value(fromIndex, "fromIndex")).andValue(
                                             isLessThanOrEqualTo(lazyValue(input::length, "input.length"))));

         this.next = null;
      }

      @Override
      public boolean hasNext() {
         if (hasNext == null) {
            do {
               next = nextToken().orElse(null);

               if (next != null) {
                  fromIndex = next.toIndex();
               } else {
                  fromIndex++;
               }
            } while (next == null && fromIndex < toIndex);

            hasNext = next != null;
         }

         return hasNext;
      }

      @Override
      public Token next() {
         hasNext = null;
         return next;
      }

      private Optional<Token> nextToken() {
         return stream(tokenMatchers)
               .map(tm -> tm.matches(input, fromIndex, toIndex))
               .filter(Optional::isPresent)
               .map(Optional::get)
               .findFirst();
      }

   }

   @Override
   public String toString() {
      return new StringJoiner(", ", DefaultTokenizer.class.getSimpleName() + "[", "]")
            .add("tokenMatchers=" + tokenMatchers)
            .toString();
   }
}
