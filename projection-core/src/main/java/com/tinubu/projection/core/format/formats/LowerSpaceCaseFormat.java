/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.formats;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StringUtils.capitalize;

import java.util.List;

import com.tinubu.projection.core.format.AbstractFormat;
import com.tinubu.projection.core.format.token.IdentifierTokenizer;

/**
 * Lower space case. Identifiers are separated with space, all characters are lowercase.
 * <p>
 * The use case for this format is to transform any identifier, including a camel case identifier, into a
 * space separated identifier.
 * <p>
 * Non-alphanumeric characters are removed.
 */
public class LowerSpaceCaseFormat extends AbstractFormat {

   private static final List<String> NAMES = list("lower-spacecase", "spacecase", "space");
   private static final boolean SPLIT_NUMBERS = true;
   public static final boolean DEFAULT_WORD_CAPITALIZE = false;
   public static final boolean DEFAULT_CAPITALIZE = false;

   private final boolean capitalize;
   private final boolean wordCapitalize;

   public LowerSpaceCaseFormat(boolean capitalize, boolean wordCapitalize) {
      this.capitalize = capitalize;
      this.wordCapitalize = wordCapitalize;
   }

   public LowerSpaceCaseFormat() {
      this(DEFAULT_CAPITALIZE, DEFAULT_WORD_CAPITALIZE);
   }

   @Override
   public List<String> names() {
      return list(NAMES);
   }

   @Override
   public int inferenceOrder() {
      return 7000;
   }

   @Override
   public String lowFormat(String content) {
      return IdentifierTokenizer.of(SPLIT_NUMBERS).tokens(content).reduce("", (s, token) -> {
         var ts = token.subString(content).toLowerCase();
         if ((capitalize && s.isEmpty()) || wordCapitalize) {
            ts = capitalize(ts);
         }
         return s + (s.isEmpty() ? "" : " ") + ts;
      }, String::concat);
   }

}
