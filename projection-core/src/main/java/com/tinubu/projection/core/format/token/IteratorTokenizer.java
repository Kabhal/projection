/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.util.Iterator;
import java.util.stream.Stream;

/**
 * Specialized tokenizer based on an abstract iterator.
 */
public abstract class IteratorTokenizer implements Tokenizer {

   protected abstract Iterator<Token> iterator(String input, int fromIndex, int toIndex);

   @Override
   public Stream<Token> tokens(String input, int fromIndex, int toIndex) {
      return stream(iterator(input, fromIndex, toIndex));
   }

   @Override
   public Stream<Token> tokens(String input, int fromIndex) {
      return stream(iterator(input, fromIndex, nullable(input).map(String::length).orElse(0)));
   }

   @Override
   public Stream<Token> tokens(String input) {
      return stream(iterator(input, 0, nullable(input).map(String::length).orElse(0)));
   }

}
