/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token.matcher;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.projection.core.format.token.codepointset.CodePointSet.mergeCodePointSet;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.projection.core.format.token.Token;
import com.tinubu.projection.core.format.token.codepointset.CodePointSet;

/**
 * A named "class" of token.
 * Include and exclude sets usage is exclusive and are not used together.
 * <p>
 * Token class is also a {@link TokenMatcher}.
 */
public class TokenClass implements TokenMatcher {
   protected final String name;
   protected final CodePointSet includeSet;
   protected final CodePointSet excludeSet;

   protected TokenClass(String name, CodePointSet includeSet, CodePointSet excludeSet) {
      this.name = Check.notBlank(name, "name");
      this.includeSet = nullable(includeSet, Check::noNullElements);
      this.excludeSet = nullable(excludeSet, Check::noNullElements);
   }

   public static TokenClass ofIncludeSets(String name, CodePointSet... includeSets) {
      return new TokenClass(name, mergeCodePointSet(includeSets), null);
   }

   public static TokenClass ofExcludeSets(String name, CodePointSet... excludeSets) {
      return new TokenClass(name, null, mergeCodePointSet(excludeSets));
   }

   @Override
   public String name() {
      return name;
   }

   @Override
   public Optional<Token> matches(String input, int fromIndex, int toIndex) {
      var matches = matchesUntil(input, fromIndex, toIndex);

      if (matches != -1) {
         return optional(Token.of(this, fromIndex, matches));
      } else {
         return optional();
      }
   }

   /**
    * Returns Unicode code point at specified index if matching, otherwise returns {@code -1}.
    */
   public int matchesAt(String input, int index) {
      Check.notNull(input, "input");
      Check.validate(index, "index", isPositive());

      if (index < input.length()) {
         var cp = input.codePointAt(index);
         if ((includeSet != null && includeSet.contains(cp)) || (excludeSet != null
                                                                 && !excludeSet.contains(cp))) {
            return cp;
         }
      }
      return -1;
   }

   /**
    * Returns last Unicode code unit index matching this token class, start from specified index (inclusive)
    * and until input as been read, or {@code toIndex} (exclusive) is reached.
    * If first matched index does not match this token class, or is out of input bound, returns {@code -1}.
    */
   public int matchesUntil(String input, int fromIndex, int toIndex) {
      Check.notNull(input, "input");
      Check.validate(fromIndex, "fromIndex", isPositive());
      Check.validate(toIndex,
                     "toIndex",
                     isGreaterThanOrEqualTo(value(fromIndex, "fromIndex")).andValue(isLessThanOrEqualTo(
                           lazyValue(input::length,
                           "input.length"))));

      var index = fromIndex;
      while (index < toIndex) {
         var cp = input.codePointAt(index);
         if ((includeSet != null && includeSet.contains(cp)) || (excludeSet != null
                                                                 && !excludeSet.contains(cp))) {
            index += Character.charCount(cp);
         } else {
            return index == fromIndex ? -1 : index;
         }
      }
      return index == fromIndex ? -1 : index;
   }

   /**
    * Returns last Unicode code unit index matching this token class, start from specified index (inclusive)
    * and until input as been read.
    * If first matched index does not match this token class, or is out of input bound, returns {@code -1}.
    */
   public int matchesUntil(String input, int fromIndex) {
      return matchesUntil(input, fromIndex, nullable(input).map(String::length).orElse(0));
   }

   /**
    * Returns first matching Unicode code unit index, starting from specified index (inclusive).
    */
   public int nextMatch(String input, int fromIndex) {
      Check.notNull(input, "input");
      Check.validate(fromIndex,
                     "fromIndex",
                     isPositive().andValue(isLessThanOrEqualTo(lazyValue(input::length, "input.length"))));

      for (int i = fromIndex; i < input.length(); ) {
         var cp = matchesAt(input, i);
         if (cp != -1) {
            return i;
         }
         i += Character.charCount(cp);
      }
      return -1;
   }

   /**
    * Returns first matching Unicode code unit index, starting from index {@code 0}.
    */
   public int nextMatch(String input) {
      return nextMatch(input, 0);
   }

   /**
    * Returns {@code true} is specified token class as the same name (case-sensitive) as this token class.
    */
   public boolean sameEntityAs(TokenClass tc) {
      if (this == tc) return true;
      return Objects.equals(name, tc.name);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", TokenClass.class.getSimpleName() + "[", "]")
            .add("name='" + name + "'")
            .add("includeSet=" + includeSet)
            .add("excludeSet=" + excludeSet)
            .toString();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      TokenClass that = (TokenClass) o;
      return Objects.equals(name, that.name) && Objects.equals(includeSet, that.includeSet) && Objects.equals(
            excludeSet,
            that.excludeSet);
   }

   @Override
   public int hashCode() {
      return Objects.hash(name, includeSet, excludeSet);
   }
}
