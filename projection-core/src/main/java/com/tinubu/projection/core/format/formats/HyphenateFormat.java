/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.formats;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.util.List;

import com.tinubu.commons.lang.util.StringUtils;
import com.tinubu.projection.core.format.AbstractFormat;
import com.tinubu.projection.core.format.token.DefaultTokenizer;
import com.tinubu.projection.core.format.token.codepointset.CodePointSet;
import com.tinubu.projection.core.format.token.codepointset.GenericSet;
import com.tinubu.projection.core.format.token.matcher.TokenClass;

/**
 * Replaces any blank sequence with a single hyphen.
 * <p>
 * Leading/trailing hyphens are stripped depending on {@link #stripStart()}/{@link #stripEnd()} flags.
 */
public class HyphenateFormat extends AbstractFormat {

   private static final List<String> NAMES = list("hyphenate");
   private static final GenericSet HYPHEN_SET = GenericSet.of("-");
   private static final GenericSet DEFAULT_BLANK_SET = GenericSet.of(" \t\r\n");

   private final CodePointSet blankSet;
   private final boolean stripStart;
   private final boolean stripEnd;

   public HyphenateFormat() {
      this(null, true, true);
   }

   protected HyphenateFormat(String blankSet, boolean stripStart, boolean stripEnd) {
      this.blankSet = nullable(blankSet).map(GenericSet::of).orElse(DEFAULT_BLANK_SET);
      this.stripStart = stripStart;
      this.stripEnd = stripEnd;
   }

   public static HyphenateFormat of(String blankSet, boolean stripStart, boolean stripEnd) {
      return new HyphenateFormat(blankSet, stripStart, stripEnd);
   }

   public static HyphenateFormat of(String blankSet, boolean strip) {
      return new HyphenateFormat(blankSet, strip, strip);
   }

   public static HyphenateFormat ofStrict() {
      return new HyphenateFormat(null, true, true);
   }

   public static HyphenateFormat ofSpaceReplacer(boolean stripStart, boolean stripEnd) {
      return of(" ", stripStart, stripEnd);
   }

   public static HyphenateFormat ofSpaceReplacer(boolean strip) {
      return ofSpaceReplacer(strip, strip);
   }

   public static HyphenateFormat ofSpaceReplacer() {
      return ofSpaceReplacer(true);
   }

   @Override
   public List<String> names() {
      return NAMES;
   }

   @Override
   public int inferenceOrder() {
      return 20000;
   }

   public CodePointSet blankSet() {
      return blankSet;
   }

   public boolean stripStart() {
      return stripStart;
   }

   public boolean stripEnd() {
      return stripEnd;
   }

   @Override
   public String lowFormat(String content) {
      var hyphenate = TokenClass.ofIncludeSets("hyphenate", blankSet, HYPHEN_SET);
      var notHyphenate = TokenClass.ofExcludeSets("nothyphenate", blankSet, HYPHEN_SET);

      var tokens = DefaultTokenizer.of(hyphenate, notHyphenate).tokens(content);

      var result = stream(tokens).reduce("", (s, token) -> {
         if (token.hasTokenMatcher(hyphenate)) {
            if (stripStart && s.isEmpty()) {
               return s;
            } else {
               return s + "-";
            }
         } else {
            return s + token.subString(content);
         }
      }, String::concat);

      if (stripEnd) {
         result = StringUtils.removeEnd(result, "-");
      }

      return result;
   }

}
