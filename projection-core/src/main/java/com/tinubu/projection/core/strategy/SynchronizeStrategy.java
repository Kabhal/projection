/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.strategy;

import java.util.Optional;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;

/**
 * Synchronization strategy used to create or synchronize documents in a projection.
 */
public interface SynchronizeStrategy {

   /**
    * Strategy unique name.
    *
    * @return strategy name
    */
   String name();

   /**
    * Synchronization between a template repository document and the corresponding project document.
    *
    * @param templateDocument template document
    * @param projectDocument project document
    *
    * @return synchronized document entries or {@link Optional#empty} if documents not synchronized
    */
   Optional<DocumentEntry> synchronizeDocument(Document templateDocument, Document projectDocument)
         throws SynchronizeStrategyException;

   /**
    * Saves a document in project repository.
    *
    * @param projectDocument project document
    *
    * @return saved document entry or {@link Optional#empty} if document not saved
    */
   Optional<DocumentEntry> saveDocument(Document projectDocument) throws SynchronizeStrategyException;

   /**
    * Saves a document in project repository.
    *
    * @param projectDocument project document
    * @param overwrite force overwrite option
    *
    * @return saved document entry or {@link Optional#empty} if document not saved
    */
   Optional<DocumentEntry> saveDocument(Document projectDocument, boolean overwrite)
         throws SynchronizeStrategyException;

   // FIXME doc
   default void finish() throws SynchronizeStrategyException { }
}
