/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoBlankElements;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.lang.util.Pair;

/**
 * Special alias format, delegating to one or several formats.
 * <p>
 * By convention, alias format inference order is the mean of delegate's inference orders.
 */
public class AliasFormat extends AbstractFormat {

   /**
    * Separator for multiple format names in alias format specification (e.g.:
    * {@code name1;name2=format1}).
    */
   public static final String FORMAT_NAMES_SEPARATOR = ";";
   /**
    * Separator for compound formats delegates in alias format specification (e.g.:  (e.g.:
    * {@code name1=format1|format2}).
    */
   public static final String FORMAT_COMPOUND_SEPARATOR = "|";

   private final List<String> names;
   private final List<Format> delegates;
   private final int inferenceOrder;

   protected AliasFormat(List<String> names, List<Format> delegates, Integer inferenceOrder) {
      this.names = Check.validate(names, "names", hasNoBlankElements());
      this.delegates = Check.validate(delegates,
                                      "delegates",
                                      CollectionRules
                                            .<List<Format>, Format>hasNoNullElements()
                                            .andValue(allSatisfies(hasNoAliasLoop())));
      this.inferenceOrder = nullable(inferenceOrder, () -> inferenceOrder(delegates));
   }

   public AliasFormat(List<String> names, List<Format> delegates) {
      this(names, delegates, null);
   }

   public AliasFormat(String name, List<Format> delegates) {
      this(list(name), delegates, null);
   }

   public AliasFormat(List<String> names, Format delegate) {
      this(names, list(delegate), null);
   }

   public AliasFormat(String name, Format delegate) {
      this(list(name), list(delegate), null);
   }

   public Pair<String, String> toSpecification() {
      return Pair.of(stream(names).collect(joining(FORMAT_NAMES_SEPARATOR)),
                     stream(delegates).map(Format::name).collect(joining(FORMAT_COMPOUND_SEPARATOR)));
   }

   /**
    * @param registeredFormats indexed registered formats
    * @param nameSpecification alias format names, separated by
    *       {@value #FORMAT_NAMES_SEPARATOR}
    * @param delegatesSpecification delegates format names, separated by
    *       {@value #FORMAT_COMPOUND_SEPARATOR}
    *
    * @return new alias format instance
    *
    * @throws IllegalStateException if a delegate format is not in registered formats.
    */
   public static AliasFormat ofSpecification(Map<String, Format> registeredFormats,
                                             String nameSpecification,
                                             String delegatesSpecification) {
      Check.notBlank(nameSpecification, "nameSpecification");
      Check.notBlank(delegatesSpecification, "delegatesSpecification");

      var names = list(StringUtils.split(nameSpecification, FORMAT_NAMES_SEPARATOR));
      var delegates = list(StringUtils.split(delegatesSpecification, FORMAT_COMPOUND_SEPARATOR));
      List<Format> delegatesFormats = list();
      for (String delegate : delegates) {
         var delegateFormat = registeredFormats.get(delegate);
         if (delegateFormat == null) {
            throw new IllegalStateException(String.format("Unknown '%s' format name for '%s' format",
                                                          delegate,
                                                          nameSpecification));
         } else {
            delegatesFormats.add(delegateFormat);
         }
      }
      return new AliasFormat(names, delegatesFormats);
   }

   @Override
   public List<String> names() {
      return names;
   }

   public List<Format> delegates() {
      return delegates;
   }

   public AliasFormat inferenceOrder(int inferenceOrder) {
      return new AliasFormat(names, delegates, inferenceOrder);
   }

   /**
    * Shift current inference order by specified amount (positive, or negative), ensuring that no overflow
    * occurs.
    *
    * @param shift shift amount, positive or negative
    *
    * @return new alias format
    */
   public AliasFormat shiftInferenceOrder(int shift) {
      if (shift < 0 && !(inferenceOrder >= Integer.MIN_VALUE - shift)) {
         return new AliasFormat(names, delegates, Integer.MIN_VALUE);
      } else if (shift >= 0 && !(inferenceOrder <= Integer.MAX_VALUE - shift)) {
         return new AliasFormat(names, delegates, Integer.MAX_VALUE);
      } else {
         return new AliasFormat(names, delegates, inferenceOrder + shift);
      }
   }

   @Override
   public int inferenceOrder() {
      return inferenceOrder;
   }

   @Override
   public String lowFormat(String content) {
      var formattedContent = content;

      for (Format format : delegates) {
         formattedContent = format.format(formattedContent);
      }

      return formattedContent;
   }

   protected <T extends Format> InvariantRule<T> hasNoAliasLoop() {
      return notSatisfiesValue(format -> {
         if (format instanceof AliasFormat alias) {
            if (anyNameMatch(alias)) {
               return true;
            } else {
               return stream(alias.delegates()).reduce(false,
                                                       (b, delegateFormat) -> b || hasNoAliasLoop()
                                                             .negate()
                                                             .test(delegateFormat),
                                                       Boolean::logicalOr);
            }
         } else {
            return false;
         }
      }, FastStringFormat.of("'", validatingObject(), "' must not have loop on ", this.names(), ""));
   }

   protected static int inferenceOrder(List<Format> delegates) {
      return delegates.stream().reduce(0, (v, f) -> v + f.inferenceOrder(), Integer::sum) / delegates.size();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", AliasFormat.class.getSimpleName() + "[", "]")
            .add("names=" + names)
            .add("delegates=" + delegates)
            .toString();
   }

}
