/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import static com.tinubu.commons.lang.log.ExtendedLogger.lazy;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ServiceLoader} format factory.
 * <p>
 * Service loader can load either :
 * <ul>
 * <li>Single {@link Format} from registered {@code com.tinubu.projection.core.format.Format}</li>
 * <li>All {@link Format} from {@link FormatFactory} from registered {@code com.tinubu.projection.core.format.FormatFactory}</li>
 * </ul>
 */
public class ServiceLoaderFormatFactory implements FormatFactory {

   private static final Logger log = LoggerFactory.getLogger(ServiceLoaderFormatFactory.class);

   private final List<Format> formats;

   public ServiceLoaderFormatFactory() {
      this.formats = listConcat(loadFormats(), loadFormatFactoryFormats());
   }

   private static List<Format> loadFormats() {
      ServiceLoader<Format> formatLoader = ServiceLoader.load(Format.class);

      var formats = list(stream(formatLoader));

      if (log.isDebugEnabled()) {
         formats.forEach(format -> {
            log.debug("Load '{}' format [{}] names: {}",
                      format.name(),
                      lazy(() -> format.getClass().getSimpleName()),
                      lazy(() -> stream(format.names()).collect(joining(", "))));
         });
      }

      return formats;
   }

   private static List<Format> loadFormatFactoryFormats() {
      ServiceLoader<FormatFactory> formatFactoryLoader = ServiceLoader.load(FormatFactory.class);

      var formatFactories = list(stream(formatFactoryLoader));

      List<Format> formats = list();
      formatFactories.forEach(formatFactory -> {
         var factoryFormats = formatFactory.formats();
         factoryFormats.forEach(format -> log.debug("Load '{}' format [{}] from [{}] factory names: {} ",
                                                    format.name(),
                                                    lazy(() -> format.getClass().getSimpleName()),
                                                    lazy(() -> formatFactory.getClass().getSimpleName()),
                                                    lazy(() -> stream(format.names()).collect(joining(", ")))));
         formats.addAll(factoryFormats);
      });

      return formats;
   }

   public List<Format> formats() {
      return formats;
   }

}
