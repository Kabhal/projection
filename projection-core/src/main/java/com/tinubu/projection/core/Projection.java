/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoBlankElements;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.uri.Uri.compatibleUri;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.CollectionUtils.set;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableBoolean;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.processStream;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamUpdateOrAdd;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import static com.tinubu.commons.ports.document.domain.DocumentEntrySpecification.allDocuments;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.defaultParameters;
import static com.tinubu.commons.ports.document.domain.uri.DefaultExportUriOptions.sensitiveParameters;
import static com.tinubu.projection.core.config.ProjectionConfiguration.BiDirectionalFilter.ofDefaultFilter;
import static com.tinubu.projection.core.config.ProjectionConfiguration.BiDirectionalFilter.ofDownstreamFilter;
import static com.tinubu.projection.core.config.ProjectionConfiguration.BiDirectionalFilter.ofUpstreamFilter;
import static com.tinubu.projection.core.config.ProjectionConfiguration.Filter.ofExcludes;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ddd2.uri.uris.FileUri.FileUriRestrictions;
import com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader;
import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.DocumentRepositoryFactory;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.git.GitDocumentRepository;
import com.tinubu.projection.core.config.ConfigurableProjectionConfiguration;
import com.tinubu.projection.core.config.ConfigurableProjectionConfiguration.Builder;
import com.tinubu.projection.core.config.DocumentProjectionConfiguration;
import com.tinubu.projection.core.config.ProjectionConfiguration;
import com.tinubu.projection.core.config.ProjectionConfiguration.BiDirectionalFilter;
import com.tinubu.projection.core.config.ProjectionConfiguration.Filter;
import com.tinubu.projection.core.format.AliasFormat;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.ServiceLoaderFormatFactory;
import com.tinubu.projection.core.strategy.DirectSynchronizeStrategy.DirectSynchronizeStrategyFactory;
import com.tinubu.projection.core.strategy.SynchronizeStrategy;
import com.tinubu.projection.core.strategy.SynchronizeStrategyFactory;

/**
 * Projection main class.
 */
public class Projection {

   /** GIT exclusion pattern for synchronization. */
   private static final Path GIT_SYNCHRONIZE_EXCLUDE_FILTER = Path.of("glob:.git/**");
   /** GIT exclusion pattern for content replacement. */
   private static final Path GIT_REPLACE_CONTENT_EXCLUDE_FILTER = Path.of("glob:.git/objects/**");
   /** Whether to exclude GIT for initial configuration generation. */
   private static final boolean DEFAULT_EXCLUDE_GIT = true;
   /** Default synchronization exclude filter for initial configuration generation. */
   private static final List<Path> DEFAULT_EXCLUDE_FILTER = list(Path.of("glob:{.DS_Store,**/.DS_Store}"));

   private static final ExtendedLogger log = ProjectionLogger.of(Projection.class);
   private static final ProjectionRepository projectionRepository = new ProjectionRepository();

   private final List<Format> formats;
   private final List<Format> restrictedFormats;
   private final List<AliasFormat> extraFormats;

   private DocumentRepository projectRepository;
   private URI projectUri;
   private boolean internalProjectRepository = false;
   private boolean overwrite = false;
   private ProjectionProperties properties = new ProjectionProperties();

   public Projection(List<Format> formats) {
      this.formats = Check.noNullElements(formats, "formats");
      this.extraFormats = list();
      this.restrictedFormats = list();
   }

   public Projection() {
      this(new ServiceLoaderFormatFactory().formats());
   }

   public Projection addFormats(List<Format> formats) {
      Check.noNullElements(formats, "formats");

      this.formats.addAll(formats);

      return this;
   }

   public Projection extraFormats(List<AliasFormat> extraFormats) {
      Check.noNullElements(extraFormats, "extraFormats");

      this.extraFormats.addAll(extraFormats);

      return this;
   }

   public Projection restrictedFormats(List<String> restrictedFormats) {
      Check.validate(restrictedFormats, "restrictedFormats", hasNoBlankElements());

      if (!restrictedFormats.isEmpty()) {
         Map<String, Format> indexedFormats =
               map(stream(formats).flatMap(f -> stream(f.names()).map(name -> entry(name, f))));

         restrictedFormats.forEach(restrictedFormatName -> {
            var restrictedFormat = indexedFormats.get(restrictedFormatName);

            if (restrictedFormat == null) {
               throw new IllegalStateException(String.format("Unknown '%s' format name for restricted format",
                                                             restrictedFormatName));
            } else {
               this.restrictedFormats.add(restrictedFormat);
            }
         });
      }

      return this;
   }

   public Projection properties(Map<String, String> properties) {
      this.properties = new ProjectionProperties();
      this.properties.registerProperties(map(properties), true);
      return this;
   }

   public Projection failOnWarning(boolean failOnWarning) {
      ProjectionLoggerCallback.failOnWarning(failOnWarning);
      return this;
   }

   public ProjectionSynchronize synchronize() {
      return new ProjectionSynchronize();
   }

   public ProjectionUpdate update() {
      return new ProjectionUpdate();
   }

   /** Synchronize sub-command. */
   public class ProjectionSynchronize implements AutoCloseable {

      private DocumentRepository templateRepository;
      private URI templateUri;
      private boolean internalTemplateRepository = false;
      private ProjectionConfiguration templateConfiguration;
      private ProjectionConfiguration projectConfiguration;
      private List<Attribute> templateAttributes = list();
      private List<Attribute> projectAttributes = list();
      private PlaceholderFormat placeholderFormat = PlaceholderFormat.ofDefault();
      private Boolean inferPlaceholders;
      private boolean noPlaceholders = false;
      private Boolean excludeGit;
      private boolean generateProjectionConfiguration = true;
      private boolean overwriteProjectionConfiguration = false;
      private boolean generateProjectionMetadata = true;
      private SynchronizeStrategyFactory synchronizeStrategyFactory = new DirectSynchronizeStrategyFactory();

      public ProjectionSynchronize templateRepository(DocumentRepository templateRepository) {
         Check.validate(this.templateUri, isNull("template URI is already set"));

         this.templateRepository = validate(templateRepository, "templateRepository", isNotNull()).orThrow();

         return this;
      }

      public ProjectionSynchronize templateRepository(URI templateUri) {
         Check.validate(this.templateRepository, isNull("template repository is already set"));

         this.templateUri = Check.validate(templateUri, "templateUri", isNotNull());
         this.internalTemplateRepository = true;

         return this;
      }

      public ProjectionSynchronize templateRepository(String templateUri) {
         return templateRepository(URI.create(validate(templateUri, "templateUri", isNotBlank()).orThrow()));
      }

      public ProjectionSynchronize projectRepository(DocumentRepository projectRepository) {
         Check.validate(Projection.this.projectUri, isNull("project URI is already set"));

         Projection.this.projectRepository =
               Check.validate(projectRepository, "projectRepository", isNotNull());

         return this;
      }

      public ProjectionSynchronize projectRepository(URI projectUri) {
         Check.validate(Projection.this.projectRepository, isNull("project repository is already set"));

         Projection.this.projectUri = Check.validate(projectUri, "projectUri", isNotNull());
         Projection.this.internalProjectRepository = true;

         return this;
      }

      public ProjectionSynchronize projectRepository(String projectUri) {
         return projectRepository(URI.create(Check.validate(projectUri, "projectUri", isNotBlank())));
      }

      public ProjectionSynchronize templateConfiguration(ProjectionConfiguration templateConfiguration) {
         this.templateConfiguration = templateConfiguration;
         return this;
      }

      public ProjectionSynchronize projectConfiguration(ProjectionConfiguration projectConfiguration) {
         this.projectConfiguration = projectConfiguration;
         return this;
      }

      public ProjectionSynchronize templateAttributes(List<Attribute> templateAttributes) {
         this.templateAttributes = templateAttributes;
         return this;
      }

      public ProjectionSynchronize templateAttributes(Attribute... templateAttributes) {
         return templateAttributes(list(templateAttributes));
      }

      public ProjectionSynchronize projectAttributes(List<Attribute> projectAttributes) {
         this.projectAttributes = projectAttributes;
         return this;
      }

      public ProjectionSynchronize projectAttributes(Attribute... projectAttributes) {
         return projectAttributes(list(projectAttributes));
      }

      public ProjectionSynchronize overwrite(boolean overwrite) {
         Projection.this.overwrite = overwrite;
         return this;
      }

      /** Configure placeholders format. */
      public ProjectionSynchronize placeholderFormat(PlaceholderFormat placeholderFormat) {
         this.placeholderFormat = placeholderFormat;
         return this;
      }

      /**
       * Whether to replace template attribute values with an inferred placeholder, to create a
       * placeholdered template for example.
       * Retrieves value from project projection configuration, or default to {@code false }if unset.
       */
      public ProjectionSynchronize inferPlaceholders(Boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
         return this;
      }

      /** Whether to disable placeholders replacement which are then left as-is. */
      public ProjectionSynchronize noPlaceholders(boolean noPlaceholders) {
         this.noPlaceholders = noPlaceholders;
         return this;
      }

      /**
       * Whether to exclude {@code .git/} from synchronization.
       * Retrieves value from project projection configuration, or default to
       * {@value #DEFAULT_EXCLUDE_GIT} if unset.
       */
      public ProjectionSynchronize excludeGit(Boolean excludeGit) {
         this.excludeGit = excludeGit;
         return this;
      }

      public ProjectionSynchronize generateProjectionConfiguration(boolean generateProjectionConfiguration) {
         this.generateProjectionConfiguration = generateProjectionConfiguration;
         return this;
      }

      public ProjectionSynchronize overwriteProjectionConfiguration(boolean overwriteProjectionConfiguration) {
         this.overwriteProjectionConfiguration = overwriteProjectionConfiguration;
         return this;
      }

      public ProjectionSynchronize generateProjectionMetadata(boolean generateProjectionMetadata) {
         this.generateProjectionMetadata = generateProjectionMetadata;
         return this;
      }

      public ProjectionSynchronize synchronizeStrategyFactory(SynchronizeStrategyFactory synchronizeStrategyFactory) {
         this.synchronizeStrategyFactory = synchronizeStrategyFactory;
         return this;
      }

      public void synchronize(DocumentPath... projectDocuments) {
         synchronize(list(projectDocuments));
      }

      public void synchronize(List<DocumentPath> projectDocuments) {
         synchronize(projectDocuments == null || projectDocuments.isEmpty()
                     ? null
                     : new DocumentEntryCriteriaBuilder()
                           .documentPath(CriterionBuilder.in(list(stream(projectDocuments).map(DocumentPath::value))))
                           .build());
      }

      /**
       * Synchronizes all documents from template repository to project repository, only considering project
       * primary projection. Sub-projections are excluded from synchronization if existing.
       * <p>
       * Initial project configuration rules :
       * <ul>
       *    <li>GIT .git/objects are always excluded from content filtering</li>
       *    <li>GIT .git/ is excluded from synchronization if {@link #excludeGit} is set</li>
       *    <li>{@link #inferPlaceholders} is set in configuration</li>
       * </ul>
       * <p>
       * Some configuration parts are overridden from one-time options, however these options will be updated
       * into configuration only if {@link #overwriteProjectionConfiguration} is set.
       *
       * @param projectDocumentSpecification optional project document restriction, or {@code null}
       */
      public void synchronize(DocumentEntrySpecification projectDocumentSpecification) {
         verifyFormats(listConcat(formats, extraFormats));

         projectRepository = Projection.this
               .projectRepository()
               .orElseThrow(() -> new IllegalStateException("No project repository specified"));

         var keepProperties = properties.keepProperties();
         projectConfiguration = projectionConfiguration(projectConfiguration,
                                                        projectRepository,
                                                        this::newProjectConfiguration,
                                                        templateRepositoryUri().orElse(null),
                                                        projectAttributes,
                                                        extraFormats,
                                                        restrictedFormats,
                                                        keepProperties,
                                                        placeholderFormat,
                                                        inferPlaceholders,
                                                        excludeGit,
                                                        null);

         templateRepository = templateRepository().orElseGet(() -> {
            internalTemplateRepository = true;
            return templateRepositoryFromConfiguration(projectConfiguration);
         });

         verifyRepository(templateRepository, projectRepository);

         templateConfiguration = projectionConfiguration(templateConfiguration,
                                                         templateRepository,
                                                         () -> ConfigurableProjectionConfiguration.builder()
                                                                                                  // Optimize GIT filtering when template has no configuration
                                                                                                  .conditionalChain(
                                                                                                        __ -> nullable(
                                                                                                              excludeGit,
                                                                                                              DEFAULT_EXCLUDE_GIT),
                                                                                                        (Builder b) -> b.synchronizeFilters(
                                                                                                              ofUpstreamFilter(
                                                                                                                    ofExcludes(
                                                                                                                          list(GIT_SYNCHRONIZE_EXCLUDE_FILTER)))))
                                                                                                  .build(),
                                                         null,
                                                         templateAttributes,
                                                         list(),
                                                         list(),
                                                         keepProperties,
                                                         null,
                                                         null,
                                                         null,
                                                         excludeGit);

         verifyConfiguration(templateConfiguration, projectConfiguration);

         ProjectionProperties properties = new ProjectionProperties(projectConfiguration.properties());

         SynchronizeStrategy synchronizeStrategy =
               synchronizeStrategy(templateRepository, Projection.this.projectRepository, properties);

         processStream(Projection.this.synchronize(templateRepository,
                                                   projectRepository,
                                                   templateConfiguration,
                                                   projectConfiguration,
                                                   synchronizeStrategy,
                                                   noPlaceholders,
                                                   projectDocumentSpecification));

         if (generateProjectionConfiguration) {
            Projection.this.generateProjectionConfiguration(projectConfiguration,
                                                            synchronizeStrategy,
                                                            overwriteProjectionConfiguration);
         }
         if (generateProjectionMetadata) {
            Projection.this.generateProjectionMetadata(templateRepository,
                                                       projectConfiguration,
                                                       synchronizeStrategy);
         }

         synchronizeStrategy.finish();
      }

      /** Create a new project projection configuration when missing. */
      private ConfigurableProjectionConfiguration newProjectConfiguration() {
         return ConfigurableProjectionConfiguration
               .builder()
               .placeholderContext(b -> b.inferPlaceholders(inferPlaceholders).format(placeholderFormat))
               .synchronizeFilters(ofDownstreamFilter(ofExcludes(listConcat(DEFAULT_EXCLUDE_FILTER,
                                                                            nullable(excludeGit,
                                                                                     DEFAULT_EXCLUDE_GIT)
                                                                            ? list(
                                                                                  GIT_SYNCHRONIZE_EXCLUDE_FILTER)
                                                                            : emptyList()))))
               .replaceContentFilters(ofDefaultFilter(ofExcludes(list(GIT_REPLACE_CONTENT_EXCLUDE_FILTER))))
               .build();
      }

      /**
       * Returns template {@link DocumentRepository}. Repository is generated from template URI if required.
       *
       * @return template document repository, or {@link Optional#empty()} if no repository nor template URI
       *       is set.
       */
      private Optional<DocumentRepository> templateRepository() {
         return nullable(templateRepository).or(() -> nullable(templateUri).map(uri -> repository(uri)));
      }

      /**
       * Generates an {@link URI} to identify template repository for later references.
       * All parameters are exported with URI, including sensitive parameters.
       *
       * @return template repository URI
       */
      private Optional<URI> templateRepositoryUri() {
         return nullable(templateRepository)
               .filter(templateRepository -> templateRepository.hasCapability(REPOSITORY_URI))
               .map(templateRepository -> templateRepository.toUri(sensitiveParameters(false)).toURI())
               .or(() -> nullable(templateUri));
      }

      private SynchronizeStrategy synchronizeStrategy(DocumentRepository templateRepository,
                                                      DocumentRepository projectRepository,
                                                      ProjectionProperties properties) {
         return synchronizeStrategyFactory.synchronizeStrategy(overwrite,
                                                               templateRepository,
                                                               projectRepository,
                                                               properties);
      }

      @Override
      public void close() {
         try {
            if (templateRepository != null && internalTemplateRepository) {
               templateRepository.close();
            }
         } finally {
            if (projectRepository != null && internalProjectRepository) {
               projectRepository.close();
            }
         }
      }

   }

   /** Update template sub-command. */
   public class ProjectionUpdate implements AutoCloseable {

      private DocumentRepository templateRepository;
      private URI templateUri;
      private boolean internalTemplateRepository = false;
      private ProjectionConfiguration templateConfiguration;
      private ProjectionConfiguration projectConfiguration;
      private List<Attribute> templateAttributes = list();
      private List<Attribute> projectAttributes = list();
      private PlaceholderFormat placeholderFormat = PlaceholderFormat.ofDefault();
      private Boolean inferPlaceholders = false;
      private boolean noPlaceholders = false;
      private Boolean excludeGit;
      private Boolean createMissingDocuments = false;
      private SynchronizeStrategyFactory synchronizeStrategyFactory = new DirectSynchronizeStrategyFactory();

      public ProjectionUpdate templateRepository(DocumentRepository templateRepository) {
         validate(this.templateUri, isNull("template URI is already set")).orThrow();

         this.templateRepository = validate(templateRepository, "templateRepository", isNotNull()).orThrow();

         return this;
      }

      public ProjectionUpdate templateRepository(URI templateUri) {
         validate(this.templateRepository, isNull("template repository is already set")).orThrow();

         this.templateUri = validate(templateUri, "templateUri", isNotNull()).orThrow();
         this.internalTemplateRepository = true;

         return this;
      }

      public ProjectionUpdate templateRepository(String templateUri) {
         return templateRepository(URI.create(validate(templateUri, "templateUri", isNotBlank()).orThrow()));
      }

      public ProjectionUpdate projectRepository(DocumentRepository projectRepository) {
         validate(Projection.this.projectUri, isNull("project URI is already set")).orThrow();

         Projection.this.projectRepository =
               validate(projectRepository, "projectRepository", isNotNull()).orThrow();

         return this;
      }

      public ProjectionUpdate projectRepository(URI projectUri) {
         validate(Projection.this.projectRepository, isNull("project repository is already set")).orThrow();

         Projection.this.projectUri = validate(projectUri, "projectUri", isNotNull()).orThrow();
         Projection.this.internalProjectRepository = true;

         return this;
      }

      public ProjectionUpdate projectRepository(String projectUri) {
         return projectRepository(URI.create(validate(projectUri, "projectUri", isNotBlank()).orThrow()));
      }

      public ProjectionUpdate templateConfiguration(ProjectionConfiguration templateConfiguration) {
         this.templateConfiguration = templateConfiguration;
         return this;
      }

      public ProjectionUpdate projectConfiguration(ProjectionConfiguration projectConfiguration) {
         this.projectConfiguration = projectConfiguration;
         return this;
      }

      public ProjectionUpdate templateAttributes(List<Attribute> templateAttributes) {
         this.templateAttributes = templateAttributes;
         return this;
      }

      public ProjectionUpdate templateAttributes(Attribute... templateAttributes) {
         return templateAttributes(list(templateAttributes));
      }

      public ProjectionUpdate projectAttributes(List<Attribute> projectAttributes) {
         this.projectAttributes = projectAttributes;
         return this;
      }

      public ProjectionUpdate projectAttributes(Attribute... projectAttributes) {
         return projectAttributes(list(projectAttributes));
      }

      public ProjectionUpdate overwrite(boolean overwrite) {
         Projection.this.overwrite = overwrite;
         return this;
      }

      /** Configure placeholders format. */
      public ProjectionUpdate placeholderFormat(PlaceholderFormat placeholderFormat) {
         this.placeholderFormat = placeholderFormat;
         return this;
      }

      /**
       * Whether to replace template attribute values with an inferred placeholder, to create a
       * placeholdered template for example.
       * Retrieves value from template projection configuration, or default to {@code false} if unset.
       */
      public ProjectionUpdate inferPlaceholders(Boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
         return this;
      }

      /** Whether to disable placeholders replacement which are then left as-is. */
      public ProjectionUpdate noPlaceholders(boolean noPlaceholders) {
         this.noPlaceholders = noPlaceholders;
         return this;
      }

      /**
       * Whether to exclude {@code .git/} from synchronization.
       * Retrieves value from template projection configuration, or default to
       * {@value #DEFAULT_EXCLUDE_GIT} if unset.
       */
      public ProjectionUpdate excludeGit(Boolean excludeGit) {
         this.excludeGit = excludeGit;
         return this;
      }

      public ProjectionUpdate createMissingDocuments(boolean createMissingDocuments) {
         this.createMissingDocuments = createMissingDocuments;
         return this;
      }

      public ProjectionUpdate synchronizeStrategyFactory(SynchronizeStrategyFactory synchronizeStrategyFactory) {
         this.synchronizeStrategyFactory = synchronizeStrategyFactory;
         return this;
      }

      public void update(DocumentPath... projectDocuments) {
         update(list(projectDocuments));
      }

      public void update(List<DocumentPath> projectDocuments) {
         update(projectDocuments == null || projectDocuments.isEmpty()
                ? null
                : new DocumentEntryCriteriaBuilder()
                      .documentPath(CriterionBuilder.in(list(stream(projectDocuments).map(DocumentPath::value))))
                      .build());
      }

      /**
       * Update template repository from project repository.
       *
       * @param projectDocumentSpecification optional project document restriction, or {@code null}
       */
      public void update(DocumentEntrySpecification projectDocumentSpecification) {
         verifyFormats(listConcat(formats, extraFormats));

         projectRepository = Projection.this
               .projectRepository()
               .orElseThrow(() -> new IllegalStateException("No project repository specified"));

         var keepProperties = properties.keepProperties();
         projectConfiguration = projectionConfiguration(projectConfiguration,
                                                        projectRepository,
                                                        ConfigurableProjectionConfiguration::ofDefault,
                                                        templateRepositoryUri().orElse(null),
                                                        projectAttributes,
                                                        list(),
                                                        list(),
                                                        keepProperties,
                                                        null,
                                                        null,
                                                        null,
                                                        excludeGit);

         templateRepository = templateRepository().orElseGet(() -> {
            internalTemplateRepository = true;
            return templateRepositoryFromConfiguration(projectConfiguration);
         });

         verifyRepository(templateRepository, projectRepository);

         if (!createMissingDocuments) {
            log.info("Restrict synchronization to existing template documents");

            projectDocumentSpecification = restrictSpecificationToExistingTemplateDocuments(
                  projectDocumentSpecification,
                  templateRepository);
         }

         templateConfiguration = projectionConfiguration(templateConfiguration,
                                                         templateRepository,
                                                         ConfigurableProjectionConfiguration::ofDefault,
                                                         null,
                                                         templateAttributes,
                                                         extraFormats,
                                                         restrictedFormats,
                                                         keepProperties,
                                                         placeholderFormat,
                                                         inferPlaceholders,
                                                         excludeGit,
                                                         null);

         verifyConfiguration(templateConfiguration, projectConfiguration);

         SynchronizeStrategy synchronizeStrategy =
               synchronizeStrategy(Projection.this.projectRepository, templateRepository, properties);

         processStream(Projection.this.synchronize(projectRepository,
                                                   templateRepository,
                                                   projectConfiguration,
                                                   templateConfiguration,
                                                   synchronizeStrategy,
                                                   noPlaceholders,
                                                   projectDocumentSpecification));
      }

      /**
       * Returns a document specification that <em>restricts specified project specification</em> to existing
       * template repository documents (get repository status at call time, result is not atomic).
       *
       * @param projectDocumentSpecification project specification to restrict with existing template
       *       documents, or {@code null} to match all existing template documents
       * @param templateRepository template repository
       */
      private DocumentEntrySpecification restrictSpecificationToExistingTemplateDocuments(
            DocumentEntrySpecification projectDocumentSpecification,
            DocumentRepository templateRepository) {
         var templateDocuments = templateRepository.findDocumentEntriesBySpecification(nullable(
               projectDocumentSpecification,
               allDocuments()));

         return new DocumentEntryCriteriaBuilder()
               .documentPath(CriterionBuilder.in(list(templateDocuments.map(entry -> entry
                     .documentId()
                     .value()))))
               .build();
      }

      private SynchronizeStrategy synchronizeStrategy(DocumentRepository templateRepository,
                                                      DocumentRepository projectRepository,
                                                      ProjectionProperties properties) {
         return synchronizeStrategyFactory.synchronizeStrategy(overwrite,
                                                               templateRepository,
                                                               projectRepository,
                                                               properties);
      }

      /**
       * Returns template {@link DocumentRepository}. Repository is generated from template URI if required.
       *
       * @return template document repository, or {@link Optional#empty()} if no repository nor template URI
       *       is set.
       */
      private Optional<DocumentRepository> templateRepository() {
         return nullable(templateRepository).or(() -> nullable(templateUri).map(uri -> repository(uri)));
      }

      /**
       * Generates an {@link URI} to identify template repository for later references.
       * All parameters are exported with URI, including sensitive parameters.
       *
       * @return template repository URI
       */
      private Optional<URI> templateRepositoryUri() {
         return nullable(templateRepository)
               .filter(templateRepository -> templateRepository.hasCapability(REPOSITORY_URI))
               .map(documentRepository -> documentRepository.toUri(defaultParameters()).toURI());
      }

      @Override
      public void close() {
         try {
            if (templateRepository != null && internalTemplateRepository) {
               templateRepository.close();
            }
         } finally {
            if (projectRepository != null && internalProjectRepository) {
               projectRepository.close();
            }
         }
      }

   }

   private DocumentSynchronizeProcessor documentSynchronizeProcessor(ProjectionConfiguration templateConfiguration,
                                                                     ProjectionConfiguration projectConfiguration,
                                                                     boolean noPlaceholders,
                                                                     DocumentEntrySpecification projectionDocumentSpecification) {
      return new DocumentSynchronizeProcessor(templateConfiguration,
                                              projectConfiguration,
                                              (DocumentEntry projectDocumentEntry) -> !projectionDocumentSpecification.satisfiedBy(
                                                    projectDocumentEntry),
                                              formats,
                                              noPlaceholders);
   }

   /**
    * Effective documents synchronization and content filtering.
    *
    * @param templateRepository template source repository
    * @param projectRepository project target repository
    * @param templateConfiguration template configuration
    * @param projectConfiguration project configuration
    * @param synchronizeStrategy synchronization strategy
    * @param noPlaceholders whether to replace placeholders
    * @param restrictTemplateSpecification optional template entry specification restricting
    *       synchronization, or {@code null}
    *
    * @return synchronized document entries
    */
   private Stream<DocumentEntry> synchronize(DocumentRepository templateRepository,
                                             DocumentRepository projectRepository,
                                             ProjectionConfiguration templateConfiguration,
                                             ProjectionConfiguration projectConfiguration,
                                             SynchronizeStrategy synchronizeStrategy,
                                             boolean noPlaceholders,
                                             DocumentEntrySpecification restrictTemplateSpecification) {
      Check.notNull(templateRepository, "templateRepository");
      Check.notNull(projectRepository, "projectRepository");
      Check.notNull(templateConfiguration, "templateConfiguration");
      Check.notNull(projectConfiguration, "projectConfiguration");
      Check.notNull(synchronizeStrategy, "synchronizeStrategy");
      Check.notNull(noPlaceholders, "noPlaceholders");

      log.info("Synchronize '{}' to '{}' using '{}' strategy",
               () -> templateRepository.toUri().toURI(),
               () -> projectRepository.toUri().toURI(),
               synchronizeStrategy::name);

      if (projectConfiguration.placeholderContext().inferPlaceholders()) {
         log.info("Infer placeholders using attributes and enabled formats with '{}'",
                  projectConfiguration::placeholderContext);
      }

      DocumentEntrySpecification projectionDocumentSpecification =
            filterProjectDocuments(projectRepository, projectConfiguration, true);

      var processor = documentSynchronizeProcessor(templateConfiguration,
                                                   projectConfiguration,
                                                   noPlaceholders,
                                                   projectionDocumentSpecification);

      refreshRelativeContentLoader(templateRepository, processor);

      return templateRepository
            .findDocumentsBySpecification(filterTemplateDocuments(templateConfiguration).and(nullable(
                  restrictTemplateSpecification,
                  allDocuments())))
            .flatMap(templateDocument -> {
               Document projectDocument = processor.process(templateDocument);

               if (projectionDocumentSpecification.satisfiedBy(projectDocument.documentEntry())) {
                  try {
                     return stream(synchronizeStrategy.synchronizeDocument(templateDocument,
                                                                           projectDocument));
                  } catch (Exception e) {
                     throw new IllegalStateException(String.format("Error while processing '%s' -> '%s': %s",
                                                                   templateDocument
                                                                         .documentId()
                                                                         .stringValue(),
                                                                   projectDocument.documentId().stringValue(),
                                                                   e.getMessage()), e);
                  }
               } else {
                  return stream();
               }
            });
   }

   /**
    * Remove older {@link RelativeContentLoader} from static {@link ServiceLoaderContentLoader} and add new
    * one.
    *
    * @param templateRepository source repository for relative URI
    * @param processor optional processor to apply to relative document, or {@code null}
    */
   private static void refreshRelativeContentLoader(DocumentRepository templateRepository,
                                                    DocumentProcessor processor) {
      ServiceLoaderContentLoader
            .loadContentLoaders()
            .removeIf(cl -> cl.getClass().equals(RelativeContentLoader.class));
      ServiceLoaderContentLoader.loadContentLoaders(new RelativeContentLoader(templateRepository, processor));
   }

   /**
    * Returns project {@link DocumentRepository}. Repository is generated from project URI if required.
    *
    * @return project document repository, or {@link Optional#empty()} if no repository nor project URI
    *       is set.
    */
   private Optional<DocumentRepository> projectRepository() {
      return nullable(projectRepository).or(() -> nullable(projectUri).map(uri -> repository(uri)));
   }

   /**
    * Returns template repository instantiated from URI configured in project configuration.
    *
    * @param projectConfiguration project configuration
    *
    * @return new template document repository
    */
   private DocumentRepository templateRepositoryFromConfiguration(ProjectionConfiguration projectConfiguration) {
      return projectConfiguration
            .templateUri()
            .map(uri -> repository(uri))
            .orElseThrow(() -> new IllegalStateException("Missing template URI in project configuration"));
   }

   /**
    * Generates metadata in projection application path. Metadata are stored as different
    * documents :
    * <ul>
    *    <li>{@code LAST_SYNCHRONIZATION_DATE}</li>
    *    <li>{@code LAST_SYNCHRONIZATION_CONFIGURATION}</li>
    *    <li>{@code LAST_SYNCHRONIZATION_COMMIT}</li>
    * </ul>
    *  @param templateRepository template repository
    *
    * @param projectConfiguration project configuration
    * @param synchronizeStrategy synchronize strategy
    */
   private void generateProjectionMetadata(DocumentRepository templateRepository,
                                           ProjectionConfiguration projectConfiguration,
                                           SynchronizeStrategy synchronizeStrategy) {
      Document lastSynchronizationDate = new DocumentBuilder()
            .documentId(DocumentPath.of(projectionRepository
                                              .projectionDirectory()
                                              .resolve("LAST_SYNCHRONIZATION_DATE")))
            .loadedContent(DateTimeFormatter.ISO_INSTANT.format(Instant.now()), StandardCharsets.UTF_8)
            .build();

      synchronizeStrategy.saveDocument(lastSynchronizationDate, true);

      Document lastSynchronizationConfiguration = DocumentProjectionConfiguration
            .of(projectConfiguration)
            .toDocument(DocumentPath.of(projectionRepository
                                              .projectionDirectory()
                                              .resolve("LAST_SYNCHRONIZATION_CONFIGURATION")));

      synchronizeStrategy.saveDocument(lastSynchronizationConfiguration, true);

      if (templateRepository instanceof GitDocumentRepository) {
         ((GitDocumentRepository) templateRepository).resolveObjectId("HEAD").ifPresent(lastCommit -> {
            Document lastSynchronizationCommit = new DocumentBuilder()
                  .documentId(DocumentPath.of(projectionRepository
                                                    .projectionDirectory()
                                                    .resolve("LAST_SYNCHRONIZATION_COMMIT")))
                  .loadedContent(lastCommit, StandardCharsets.UTF_8)
                  .build();

            synchronizeStrategy.saveDocument(lastSynchronizationCommit, true);
         });
      }
   }

   /**
    * Generates the new project configuration using specified synchronization strategy.
    *
    * @param projectConfiguration project configuration
    * @param synchronizeStrategy synchronize strategy
    * @param overwrite whether to overwrite existing project configuration
    *
    * @return created document entry or {@link Optional#empty} if document not created
    */
   private Optional<DocumentEntry> generateProjectionConfiguration(ProjectionConfiguration projectConfiguration,
                                                                   SynchronizeStrategy synchronizeStrategy,
                                                                   boolean overwrite) {
      notNull(projectConfiguration, "projectConfiguration");
      notNull(synchronizeStrategy, "synchronizeStrategy");

      DocumentPath configurationPath = DocumentPath.of(projectionRepository.projectionConfiguration());

      return synchronizeStrategy.saveDocument(DocumentProjectionConfiguration
                                                    .of(projectConfiguration)
                                                    .toDocument(configurationPath), overwrite);
   }

   /**
    * Verifies repositories consistency.
    *
    * @param templateRepository template repository
    * @param projectRepository project repository
    */
   private void verifyRepository(DocumentRepository templateRepository,
                                 DocumentRepository projectRepository) {
      notNull(templateRepository, "templateRepository");
      notNull(projectRepository, "projectRepository");

      if (projectRepository.sameRepositoryAs(templateRepository)) {
         throw new IllegalStateException("Can't synchronize when template and project URIs are the same");
      }
   }

   /**
    * Verifies formats.
    *
    * @param formats formats to check
    */
   private static void verifyFormats(List<Format> formats) {
      Map<String, Format> indexedFormats = map();
      Set<String> duplicatedFormats = set();

      formats.forEach(format -> {
         format.names().forEach(name -> {
            if (indexedFormats.containsKey(name)) {
               duplicatedFormats.add(name);
            } else {
               indexedFormats.put(name, format);
            }
         });
      });

      if (!duplicatedFormats.isEmpty()) {
         throw new IllegalStateException(String.format("Duplicated format names : %s",
                                                       stream(duplicatedFormats)
                                                             .sorted()
                                                             .collect(joining(","))));
      }

   }

   /**
    * Verifies configurations completeness and consistency.
    *
    * @param templateConfiguration template configuration
    * @param projectConfiguration project configuration
    */
   private void verifyConfiguration(ProjectionConfiguration templateConfiguration,
                                    ProjectionConfiguration projectConfiguration) {
      notNull(templateConfiguration, "templateConfiguration");
      notNull(projectConfiguration, "projectConfiguration");

      log.debug("Template configuration = {}", () -> templateConfiguration);
      log.debug("Project configuration = {}", () -> projectConfiguration);

      if (!projectConfiguration.placeholderContext().inferPlaceholders()) {
         for (Attribute templateAttribute : templateConfiguration.attributes()) {
            Attribute projectAttribute = projectConfiguration
                  .attribute(templateAttribute.name())
                  .or(() -> {
                     if (templateAttribute.required()) {
                        return optional();
                     } else {
                        log.info("Missing optional '{}' attribute", templateAttribute::name);
                        return optional(templateAttribute);
                     }
                  })
                  .orElseThrow(() -> new IllegalStateException(String.format("Missing project '%s' attribute",
                                                                             templateAttribute.name())));

            var ambiguousFormats = attributeAmbiguousFormats(templateAttribute, projectAttribute);

            if (!ambiguousFormats.isEmpty()) {
               log.warn(
                     "Attribute '{}' : '{}' -> '{}' has ambiguous inference among formats within each group : {}. A solution is to make the template attribute value more complex, using a multi word for example",
                     templateAttribute::name,
                     templateAttribute::value,
                     projectAttribute::value,
                     () -> stream(ambiguousFormats)
                           .map(g -> stream(g).map(Format::name).sorted().collect(joining(",", "[", "]")))
                           .collect(joining(", ")));
            }
         }
      }
   }

   /**
    * Returns the list of ambiguous format groups for a given attribute.
    * <p>
    * An ambiguous format group is a template format group that is not
    * included in any project format group.
    */
   private List<List<Format>> attributeAmbiguousFormats(Attribute templateAttribute,
                                                        Attribute projectAttribute) {
      var templateGroupFormats = templateAttribute.groupFormats(formats);
      var projectGroupFormats = projectAttribute.groupFormats(formats);

      List<List<Format>> ambiguousFormats = list();
      for (List<Format> templateGroupFormat : templateGroupFormats) {
         var included = false;
         for (List<Format> projectGroupFormat : projectGroupFormats) {
            if (set(projectGroupFormat).containsAll(templateGroupFormat)) {
               included = true;
               break;
            }
         }
         if (!included) {
            ambiguousFormats.add(templateGroupFormat);
         }
      }
      return ambiguousFormats;
   }

   /**
    * Generates document specification filter when reading from template repository.
    * <p>
    * Managed exclusions :
    * <ul>
    *    <li>Projection configuration synchronization filter</li>
    *    <li>optional GIT directory</li>
    *    <li>Projection/Sub-projection directories</li>
    * </ul>
    *
    * @param templateConfiguration template configuration
    *
    * @return document specification
    */
   private DocumentEntrySpecification filterTemplateDocuments(ProjectionConfiguration templateConfiguration) {
      DocumentEntrySpecification templateFilter = templateConfiguration
            .synchronizeFilter()
            .upstreamDocumentEntrySpecification(null, null)
            .orElseGet(DocumentEntryCriteriaBuilder::allDocuments);

      return new DocumentEntryCriteriaBuilder()
            .documentPath(CriterionBuilder.notMatch(projectionRepository.projectionDirectoryGlobMatcher(true,
                                                                                                        true)))
            .build()
            .and(templateFilter);
   }

   /**
    * Generates document specification filter when writing to project repository.
    *
    * @param projectRepository project repository
    * @param projectConfiguration project configuration
    * @param excludeSubProjections if sub-projections must be filtered out
    *
    * @return document specification
    */
   private DocumentEntrySpecification filterProjectDocuments(DocumentRepository projectRepository,
                                                             ProjectionConfiguration projectConfiguration,
                                                             boolean excludeSubProjections) {
      DocumentEntrySpecification projectDocumentsSpecification = projectConfiguration
            .synchronizeFilter()
            .downstreamDocumentEntrySpecification(null, null)
            .orElseGet(DocumentEntryCriteriaBuilder::allDocuments);

      if (excludeSubProjections) {
         List<Path> subProjections = list(projectionRepository.subProjections(projectRepository,
                                                                              projectionRepository.projectionConfiguration(),
                                                                              false,
                                                                              true,
                                                                              true));

         if (!subProjections.isEmpty()) {
            log.info("Exclude {} project sub-projections from synchronization",
                     () -> subProjections.stream().map(Path::toString).collect(joining(",", "[", "]")));

            DocumentEntrySpecification excludeProjectionSubRootsFilter =
                  DocumentEntrySpecification.andSpecification(list(subProjections
                                                                         .stream()
                                                                         .map(subProjection -> new DocumentEntryCriteriaBuilder()
                                                                               .documentPath(CriterionBuilder.notStartWith(
                                                                                     subProjection))
                                                                               .build())));

            projectDocumentsSpecification =
                  projectDocumentsSpecification.and(excludeProjectionSubRootsFilter);
         }
      }

      return projectDocumentsSpecification;
   }

   /**
    * Instantiates a projection configuration from :
    * <ul>
    *    <li>provided project configuration if not {@code null}, or</li>
    *    <li>from repository's default configuration document</li>
    *    <li>from specified configuration supplier</li>
    * </ul>
    *
    * @param projectionConfiguration provided project configuration
    * @param repository repository to search for default configuration
    * @param newConfiguration new configuration builder used if configuration not found
    * @param templateRepository optional template repository overriding default configuration, or
    *       {@code null}
    * @param extraFormats optional extra formats appended to default configuration, or {@code null}
    * @param restrictedFormats optional extra restricted formats overriding default configuration, or
    *       {@code null}
    * @param attributes optional list of attributes appended to default configuration, or {@code null}
    * @param properties optional map of properties appended to default configuration, or {@code null}
    * @param placeholderFormat optional placeholder format configuration
    * @param inferPlaceholders optional flag to infer placeholders overriding default
    *       configuration, or {@code null}
    * @param downstreamExcludeGit optional flag to configure GIT exclusion in downstream filters
    * @param upstreamExcludeGit optional flag to configure GIT exclusion in upstream filters
    *
    * @return projection configuration
    */
   private ProjectionConfiguration projectionConfiguration(ProjectionConfiguration projectionConfiguration,
                                                           DocumentRepository repository,
                                                           Supplier<ProjectionConfiguration> newConfiguration,
                                                           URI templateRepository,
                                                           List<Attribute> attributes,
                                                           List<AliasFormat> extraFormats,
                                                           List<Format> restrictedFormats,
                                                           Map<String, String> properties,
                                                           PlaceholderFormat placeholderFormat,
                                                           Boolean inferPlaceholders,
                                                           Boolean downstreamExcludeGit,
                                                           Boolean upstreamExcludeGit) {
      ProjectionConfiguration configuration = nullable(projectionConfiguration)
            .or(() -> repository
                  .findDocumentById(DocumentPath.of(projectionRepository.projectionConfiguration()))
                  .<ProjectionConfiguration>map(DocumentProjectionConfiguration::ofDocument))
            .orElseGet(newConfiguration);

      var placeholderContext = PlaceholderContext
            .builder()
            .from(configuration.placeholderContext())
            .<PlaceholderContext.Builder, Boolean>optionalChain(nullable(inferPlaceholders),
                                                                PlaceholderContext.Builder::inferPlaceholders)
            .<PlaceholderContext.Builder, PlaceholderFormat>optionalChain(nullable(placeholderFormat),
                                                                          PlaceholderContext.Builder::format)
            .build();

      return ConfigurableProjectionConfiguration
            .builder()
            .from(configuration)
            .placeholderContext(placeholderContext)
            .<Builder, Boolean>optionalChain(nullable(downstreamExcludeGit), (b, eg) -> {
               BiDirectionalFilter synchronizeFilter;
               if (nullableBoolean(eg)) {
                  synchronizeFilter = configuration
                        .synchronizeFilter()
                        .downstreamFilter(df -> excludePattern(df, GIT_SYNCHRONIZE_EXCLUDE_FILTER));
               } else {
                  synchronizeFilter = configuration
                        .synchronizeFilter()
                        .defaultFilter(filter -> removePattern(filter, GIT_SYNCHRONIZE_EXCLUDE_FILTER))
                        .downstreamFilter(filter1 -> removePattern(filter1, GIT_SYNCHRONIZE_EXCLUDE_FILTER));
               }
               return b.synchronizeFilters(synchronizeFilter);
            })
            .<Builder, Boolean>optionalChain(nullable(upstreamExcludeGit), (b, eg) -> {
               BiDirectionalFilter synchronizeFilter;
               if (nullableBoolean(eg)) {
                  synchronizeFilter = configuration
                        .synchronizeFilter()
                        .upstreamFilter(df -> excludePattern(df, GIT_SYNCHRONIZE_EXCLUDE_FILTER));
               } else {
                  synchronizeFilter = configuration
                        .synchronizeFilter()
                        .defaultFilter(filter -> removePattern(filter, GIT_SYNCHRONIZE_EXCLUDE_FILTER))
                        .upstreamFilter(filter1 -> removePattern(filter1, GIT_SYNCHRONIZE_EXCLUDE_FILTER));
               }
               return b.synchronizeFilters(synchronizeFilter);
            })
            .optionalChain(nullable(templateRepository), Builder::templateRepository)
            .<Builder>conditionalChain(__ -> !attributes.isEmpty(), b -> b.attributes(attributes))
            .<Builder>conditionalChain(__ -> !extraFormats.isEmpty(), b -> b.extraFormats(extraFormats))
            .<Builder>conditionalChain(__ -> !restrictedFormats.isEmpty(),
                                       b -> b.restrictedFormats(set(restrictedFormats
                                                                          .stream()
                                                                          .map(Format::name))))
            .<Builder>conditionalChain(__ -> !properties.isEmpty(), b -> b.properties(properties))
            .build();
   }

   /**
    * Adds pattern to specified filter's excludes, if not already present.
    *
    * @return filtered filter
    */
   private static Filter excludePattern(Filter df, Path pattern) {
      return df.excludes(e -> list(streamUpdateOrAdd(stream(e), pattern)));
   }

   /**
    * Removes pattern from specified filter's excludes.
    *
    * @return filtered filter
    */
   private static Filter removePattern(Filter filter, Path pattern) {
      return Filter.of(filter.includes(), list(filter.excludes().stream().filter(p -> !p.equals(pattern))));
   }

   /**
    * Creates repository from URI.
    *
    * @param uri URI, can be absolute or relative.
    *
    * @return repository
    *
    * @implSpec Relative URIs are normalized to absolute {@code file:} URIs
    */
   private DocumentRepository repository(URI uri) {
      var absoluteUri = absoluteUri(uri);

      return DocumentRepositoryFactory
            .instance()
            .documentRepository(RepositoryUri.ofRepositoryUri(absoluteUri))
            .orElseThrow(() -> new IllegalStateException(String.format("Unsupported '%s' repository",
                                                                       absoluteUri)));
   }

   /**
    * Special support for relative file URI, transformed into absolute file URI using current work directory.
    */
   private URI absoluteUri(URI uri) {
      return compatibleUri(uri,
                           FileUriRestrictions.ofRestrictions(false, false, true, false, false),
                           FileUri::ofUri)
            .optional()
            .map(fileUri -> fileUri.conditionalPath(false, p -> Path.of(p).toAbsolutePath().toString()))
            .map(FileUri::toURI)
            .orElse(uri);
   }

}