/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.config;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.CollectionUtils.set;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.projection.core.Attribute;
import com.tinubu.projection.core.PlaceholderContext;
import com.tinubu.projection.core.PlaceholderFormat;
import com.tinubu.projection.core.format.AliasFormat;

/**
 * Configurable configuration.
 *
 * @implSpec Immutable class implementation
 */
public class ConfigurableProjectionConfiguration extends ProjectionConfiguration {
   private ConfigurableProjectionConfiguration(ConfigurableProjectionConfiguration.Builder builder) {
      super(new ProjectionConfiguration.Builder()
                  .templateUri(builder.templateRepository)
                  .attributes(list(builder.attributes.values()))
                  .extraFormats(builder.extraFormats)
                  .restrictedFormats(builder.restrictedFormats)
                  .placeholderContext(builder.placeholderContext)
                  .synchronizeFilter(builder.synchronizeFilters)
                  .replaceContentFilter(builder.replaceContentFilters)
                  .properties(builder.properties));
   }

   public static ConfigurableProjectionConfiguration ofDefault() {
      return new ConfigurableProjectionConfiguration(new ConfigurableProjectionConfiguration.Builder());
   }

   public static ConfigurableProjectionConfiguration.Builder builder() {
      return new ConfigurableProjectionConfiguration.Builder();
   }

   public static class Builder extends DomainBuilder<ConfigurableProjectionConfiguration> {
      private URI templateRepository;
      private Map<String, Attribute> attributes = map(LinkedHashMap::new);
      private Map<String, String> extraFormats = map(LinkedHashMap::new);
      private Set<String> restrictedFormats = set();
      private PlaceholderContext placeholderContext;
      private BiDirectionalFilter synchronizeFilters;
      private BiDirectionalFilter replaceContentFilters;
      private Map<String, String> properties = map(LinkedHashMap::new);

      public static Builder from(ProjectionConfiguration projectConfiguration) {
         return new Builder()
               .<Builder>reconstitute()
               .templateRepository(projectConfiguration.templateUri().orElse(null))
               .attributes(projectConfiguration.attributes())
               .extraFormats(projectConfiguration.extraFormats())
               .restrictedFormats(projectConfiguration.restrictedFormats())
               .placeholderContext(projectConfiguration.placeholderContext())
               .synchronizeFilters(projectConfiguration.synchronizeFilter())
               .replaceContentFilters(projectConfiguration.replaceContentFilter())
               .properties(projectConfiguration.properties());
      }

      public Builder templateRepository(URI templateRepository) {
         this.templateRepository = templateRepository;
         return this;
      }

      public Builder attributes(List<Attribute> attributes) {
         this.attributes = map(LinkedHashMap::new,
                               stream(attributes).map(attribute -> entry(attribute.name(), attribute)));
         return this;
      }

      public Builder addAttributes(List<Attribute> attributes) {
         attributes.forEach(attribute -> this.attributes.put(attribute.name(), attribute));
         return this;
      }

      public Builder addAttributes(Attribute... attributes) {
         return addAttributes(list(attributes));
      }

      public Builder extraFormats(Map<String, String> extraFormats) {
         this.extraFormats = map(LinkedHashMap::new, extraFormats);
         return this;
      }

      public Builder extraFormats(List<AliasFormat> extraFormats) {
         this.extraFormats = map(LinkedHashMap::new, extraFormats.stream().map(AliasFormat::toSpecification));
         return this;
      }

      public Builder addExtraFormats(Map<String, String> extraFormats) {
         this.extraFormats.putAll(map(LinkedHashMap::new, extraFormats));
         return this;
      }

      public Builder addExtraFormats(List<AliasFormat> extraFormats) {
         this.extraFormats.putAll(map(LinkedHashMap::new,
                                      extraFormats.stream().map(AliasFormat::toSpecification)));
         return this;
      }

      public Builder addExtraFormats(AliasFormat... extraFormats) {
         return addExtraFormats(list(extraFormats));
      }

      public Builder addExtraFormat(String name, String value) {
         this.extraFormats.put(name, value);
         return this;
      }

      public Builder addExtraFormat(AliasFormat extraFormat) {
         var entry = extraFormat.toSpecification();
         this.extraFormats.put(entry.getLeft(), entry.getRight());
         return this;
      }

      public Builder restrictedFormats(Set<String> restrictedFormats) {
         this.restrictedFormats = set(restrictedFormats);
         return this;
      }

      public Builder addRestrictedFormats(Set<String> restrictedFormats) {
         this.restrictedFormats.addAll(set(restrictedFormats));
         return this;
      }

      public Builder placeholderContext(PlaceholderContext placeholderContext) {
         this.placeholderContext = placeholderContext;
         return this;
      }

      public Builder placeholderContext(UnaryOperator<PlaceholderContext.Builder> placeholderContextBuilderMapper) {
         this.placeholderContext =
               PlaceholderContext.builder().chain(placeholderContextBuilderMapper).build();
         return this;
      }

      public Builder placeholderContext(UnaryOperator<PlaceholderFormat.Builder> placeholderFormatBuilderMapper,
                                        Boolean inferPlaceholders) {
         this.placeholderContext = PlaceholderContext
               .builder()
               .format(PlaceholderFormat.builder().chain(placeholderFormatBuilderMapper).build())
               .inferPlaceholders(inferPlaceholders)
               .build();
         return this;
      }

      public Builder synchronizeFilters(BiDirectionalFilter synchronizeFilters) {
         this.synchronizeFilters = synchronizeFilters;
         return this;
      }

      public Builder replaceContentFilters(BiDirectionalFilter replaceContentFilters) {
         this.replaceContentFilters = replaceContentFilters;
         return this;
      }

      public Builder properties(Map<String, String> properties) {
         this.properties = map(LinkedHashMap::new, properties);
         return this;
      }

      public Builder addProperties(Map<String, String> properties) {
         this.properties.putAll(map(properties));
         return this;
      }

      public Builder addProperty(String name, String value) {
         this.properties.put(name, value);
         return this;
      }

      @Override
      protected ConfigurableProjectionConfiguration buildDomainObject() {
         return new ConfigurableProjectionConfiguration(this);
      }
   }

}
