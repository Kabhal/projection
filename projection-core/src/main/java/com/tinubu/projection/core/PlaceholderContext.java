/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;

/**
 * Placeholder processing context.
 */
public class PlaceholderContext extends AbstractValue {
   private static final boolean DEFAULT_INFER_PLACEHOLDERS = false;

   private final PlaceholderFormat format;
   private final boolean inferPlaceholders;

   private PlaceholderContext(Builder builder) {
      this.format = nullable(builder.format, PlaceholderFormat.ofDefault());
      this.inferPlaceholders = nullable(builder.inferPlaceholders, DEFAULT_INFER_PLACEHOLDERS);
   }

   @Override
   protected Fields<? extends PlaceholderContext> defineDomainFields() {
      return Fields
            .<PlaceholderContext>builder()
            .field("format", v -> v.format, isNotNull())
            .field("inferPlaceholders", v -> v.inferPlaceholders)
            .build();
   }

   public static PlaceholderContext ofDefault() {
      return new PlaceholderContext(new Builder());
   }

   public static PlaceholderContext.Builder builder() {
      return new Builder();
   }

   /** Placeholder format. */
   public PlaceholderFormat format() {
      return format;
   }

   /**
    * Whether to infer placeholders when processing documents.
    */
   public boolean inferPlaceholders() {
      return inferPlaceholders;
   }

   public static class Builder extends DomainBuilder<PlaceholderContext> {
      private PlaceholderFormat format;
      private Boolean inferPlaceholders;

      public static Builder from(PlaceholderContext placeholderContext) {
         return new Builder()
               .format(placeholderContext.format())
               .inferPlaceholders(placeholderContext.inferPlaceholders());
      }

      public Builder format(PlaceholderFormat format) {
         this.format = format;
         return this;
      }

      public Builder format(UnaryOperator<PlaceholderFormat.Builder> placeholderFormatBuilderMapper) {
         this.format = PlaceholderFormat.builder().chain(placeholderFormatBuilderMapper).build();
         return this;
      }

      public Builder inferPlaceholders(Boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
         return this;
      }

      @Override
      protected PlaceholderContext buildDomainObject() {
         return new PlaceholderContext(this);
      }
   }
}
