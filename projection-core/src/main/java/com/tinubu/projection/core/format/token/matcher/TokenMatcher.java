/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token.matcher;

import java.util.Objects;
import java.util.Optional;

import com.tinubu.projection.core.format.token.Token;

/**
 * Creates a {@link Token} from parsing of input.
 * <p>
 * Matcher are identified by {@link #name()} to use resulting tokens.
 */
public interface TokenMatcher {

   /** Token matcher unqique identifier. */
   String name();

   /**
    * Parses input and returns resulting {@link Token}, or {@link Optional#empty()} if matcher does not match.
    *
    * @param input parsing input
    * @param fromIndex parses from index (inclusive)
    * @param toIndex parses until index (exclusive)
    *
    * @return matched {@link Token}, or {@link Optional#empty()}
    */
   Optional<Token> matches(String input, int fromIndex, int toIndex);

   /**
    * Returns {@code true} is specified token matcher as the same name (case-sensitive) as this token matcher.
    */
   default boolean sameEntityAs(TokenMatcher tm) {
      if (this == tm) return true;
      return Objects.equals(name(), tm.name());
   }

}
