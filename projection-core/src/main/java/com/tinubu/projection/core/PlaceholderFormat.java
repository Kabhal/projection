/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.NoopFormat;

public class PlaceholderFormat extends AbstractValue {
   private static final String DEFAULT_CONTENT_BEGIN = "$__";
   private static final String DEFAULT_CONTENT_END = "__";
   private static final String DEFAULT_CONTENT_FORMAT_SEPARATOR = "_";
   private static final String DEFAULT_PATH_BEGIN = "__";
   private static final String DEFAULT_PATH_END = "__";
   private static final String DEFAULT_PATH_FORMAT_SEPARATOR = "_";

   private final String contentBegin;
   private final String contentEnd;
   private final String contentFormatSeparator;
   private final String pathBegin;
   private final String pathEnd;
   private final String pathFormatSeparator;

   private PlaceholderFormat(Builder builder) {
      this.contentBegin = nullable(builder.contentBegin, DEFAULT_CONTENT_BEGIN);
      this.contentEnd = nullable(builder.contentEnd, DEFAULT_CONTENT_END);
      this.contentFormatSeparator =
            nullable(builder.contentFormatSeparator, DEFAULT_CONTENT_FORMAT_SEPARATOR);
      this.pathBegin = nullable(builder.pathBegin, DEFAULT_PATH_BEGIN);
      this.pathEnd = nullable(builder.pathEnd, DEFAULT_PATH_END);
      this.pathFormatSeparator = nullable(builder.pathFormatSeparator, DEFAULT_PATH_FORMAT_SEPARATOR);
   }

   @Override
   protected Fields<? extends PlaceholderFormat> defineDomainFields() {
      return Fields
            .<PlaceholderFormat>builder()
            .field("contentBegin", v -> v.contentBegin, isNotBlank())
            .field("contentEnd", v -> v.contentEnd, isNotBlank())
            .field("contentFormatSeparator", v -> v.contentFormatSeparator, isNotBlank())
            .field("pathBegin", v -> v.pathBegin, isNotBlank())
            .field("pathEnd", v -> v.pathEnd, isNotBlank())
            .field("pathFormatSeparator", v -> v.pathFormatSeparator, isNotBlank())
            .build();
   }

   public static PlaceholderFormat ofDefault() {
      return new PlaceholderFormat(new Builder());
   }

   public static PlaceholderFormat.Builder builder() {
      return new Builder();
   }

   /** Placeholder begin token for content replacement. */
   public String contentBegin() {
      return contentBegin;
   }

   /** Placeholder end token for content replacement. */
   public String contentEnd() {
      return contentEnd;
   }

   /** Placeholder format separator token for content replacement. */
   public String contentFormatSeparator() {
      return contentFormatSeparator;
   }

   /** Placeholder begin token for path replacement. */
   public String pathBegin() {
      return pathBegin;
   }

   /** Placeholder end token for path replacement. */
   public String pathEnd() {
      return pathEnd;
   }

   /** Placeholder format separator token for path replacement. */
   public String pathFormatSeparator() {
      return pathFormatSeparator;
   }

   public String contentPlaceholder(String attributeName, Format format) {
      return contentBegin()
             + attributeName
             + (format.equals(NoopFormat.instance())
                ? ""
                : contentFormatSeparator() + format.compactName())
             + contentEnd();
   }

   public String pathPlaceholder(String attributeName, Format format) {
      return pathBegin() + attributeName + (format.equals(NoopFormat.instance())
                                            ? ""
                                            : pathFormatSeparator() + format.compactName()) + pathEnd();
   }

   public static class Builder extends DomainBuilder<PlaceholderFormat> {
      private static final List<String> CONTENT_BEGIN_PROPERTY_KEYS =
            list("content-begin", "cb", "begin", "b");
      private static final List<String> CONTENT_END_PROPERTY_KEYS = list("content-end", "ce", "end", "e");
      private static final List<String> CONTENT_FORMAT_SEPARATOR_PROPERTY_KEYS =
            list("content-format-separator", "cs", "format-separator", "s");
      private static final List<String> PATH_BEGIN_PROPERTY_KEYS = list("path-begin", "pb", "begin", "b");
      private static final List<String> PATH_END_PROPERTY_KEYS = list("path-end", "pe", "end", "e");
      private static final List<String> PATH_FORMAT_SEPARATOR_PROPERTY_KEYS =
            list("path-format-separator", "ps", "format-separator", "s");

      private String contentBegin;
      private String contentEnd;
      private String contentFormatSeparator;
      private String pathBegin;
      private String pathEnd;
      private String pathFormatSeparator;

      public static Builder from(PlaceholderFormat placeholderContext) {
         return new Builder()
               .contentBegin(placeholderContext.contentBegin())
               .contentEnd(placeholderContext.contentEnd())
               .contentFormatSeparator(placeholderContext.contentFormatSeparator())
               .pathBegin(placeholderContext.pathBegin())
               .pathEnd(placeholderContext.pathEnd())
               .pathFormatSeparator(placeholderContext.pathFormatSeparator());
      }

      /**
       * Creates {@link PlaceholderFormat} builder from specified property map.
       * <p>
       * If a property prefix is specified, tokens are searched first using the prefix, then without prefix.
       * <p>
       * Supported property mappings for each token are, by search priority order :
       * <ul>
       *    <li>{@link PlaceholderFormat#contentBegin()} : {@code content-begin} | {@code cb} | {@code begin} | {@code b}</li>
       *    <li>{@link PlaceholderFormat#contentEnd()} : {@code content-end} | {@code ce} | {@code end} | {@code e}</li>
       *    <li>{@link PlaceholderFormat#contentFormatSeparator()} : {@code content-format-separator} | {@code cs} | {@code format-separator} | {@code s}</li>
       *    <li>{@link PlaceholderFormat#pathBegin()} : {@code path-begin} | {@code pb} | {@code begin} | {@code b}</li>
       *    <li>{@link PlaceholderFormat#pathEnd()} : {@code path-end} | {@code pe} | {@code end} | {@code e}</li>
       *    <li>{@link PlaceholderFormat#pathFormatSeparator()} : {@code path-format-separator} | {@code ps} | {@code format-separator} | {@code s}</li>
       * </ul>
       *
       * @param propertyPrefix optional property prefix for keys, or {@code null}, or {@code ""}
       * @param propertyMap property map
       *
       * @return placeholder format builder
       */
      public static Builder ofPropertyMap(String propertyPrefix, Map<String, String> propertyMap) {
         Check.notNull(propertyMap, "propertyMap");

         return new Builder()
               .contentBegin(propertyFormat(propertyPrefix, CONTENT_BEGIN_PROPERTY_KEYS, propertyMap).orElse(
                     null))
               .contentEnd(propertyFormat(propertyPrefix,
                                          CONTENT_END_PROPERTY_KEYS,
                                          propertyMap).orElse(null))
               .contentFormatSeparator(propertyFormat(propertyPrefix,
                                                      CONTENT_FORMAT_SEPARATOR_PROPERTY_KEYS,
                                                      propertyMap).orElse(null))
               .pathBegin(propertyFormat(propertyPrefix, PATH_BEGIN_PROPERTY_KEYS, propertyMap).orElse(null))
               .pathEnd(propertyFormat(propertyPrefix, PATH_END_PROPERTY_KEYS, propertyMap).orElse(null))
               .pathFormatSeparator(propertyFormat(propertyPrefix,
                                                   PATH_FORMAT_SEPARATOR_PROPERTY_KEYS,
                                                   propertyMap).orElse(null));
      }

      private static Optional<String> propertyFormat(String propertyPrefix,
                                                     List<String> keys,
                                                     Map<String, String> propertyMap) {
         for (String prefix : propertyPrefix == null || propertyPrefix.isEmpty()
                              ? list("")
                              : list(propertyPrefix, ""))
            for (String key : keys) {
               var format = propertyMap.get(prefix.isEmpty() ? key : prefix + "." + key);

               if (format != null) {
                  return optional(format);
               }
            }
         return optional();
      }

      public Builder contentBegin(String contentBegin) {
         this.contentBegin = contentBegin;
         return this;
      }

      public Builder contentEnd(String contentEnd) {
         this.contentEnd = contentEnd;
         return this;
      }

      public Builder contentFormatSeparator(String contentFormatSeparator) {
         this.contentFormatSeparator = contentFormatSeparator;
         return this;
      }

      public Builder pathBegin(String pathBegin) {
         this.pathBegin = pathBegin;
         return this;
      }

      public Builder pathEnd(String pathEnd) {
         this.pathEnd = pathEnd;
         return this;
      }

      public Builder pathFormatSeparator(String pathFormatSeparator) {
         this.pathFormatSeparator = pathFormatSeparator;
         return this;
      }

      @Override
      protected PlaceholderFormat buildDomainObject() {
         return new PlaceholderFormat(this);
      }
   }
}
