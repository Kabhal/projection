/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token;

import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.Optional;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.invariant.Validate.Check;

/**
 * Creates a {@link Token} stream from parsing of input.
 */
@FunctionalInterface
public interface Tokenizer {

   /**
    * Parses input and returns resulting {@link Token} stream.
    *
    * @param input parsing input
    * @param fromIndex parses from index (inclusive)
    * @param toIndex parses until index (exclusive)
    *
    * @return matched tokens, or {@link Optional#empty()}
    */
   Stream<Token> tokens(String input, int fromIndex, int toIndex);

   default Stream<Token> tokens(String input, int fromIndex) {
      return tokens(input, fromIndex, nullable(input).map(String::length).orElse(0));
   }

   default Stream<Token> tokens(String input) {
      return tokens(input, 0, nullable(input).map(String::length).orElse(0));
   }

   default Optional<Token> tokenAt(String input, int fromIndex, int toIndex) {
      Check.notNull(input, "input");
      Check.validate(fromIndex, "fromIndex", isPositive());

      return tokens(input, fromIndex, toIndex).findFirst();
   }

   default Optional<Token> tokenAt(String input, int fromIndex) {
      return tokenAt(input, fromIndex, nullable(input).map(String::length).orElse(0));
   }

   default Optional<Token> tokenAt(String input) {
      return tokenAt(input, 0, nullable(input).map(String::length).orElse(0));
   }

}
