/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.util.List;

import com.tinubu.projection.core.format.formats.CapitalizeFormat;
import com.tinubu.projection.core.format.formats.LowerCaseFormat;
import com.tinubu.projection.core.format.formats.LowerDotCaseFormat;
import com.tinubu.projection.core.format.formats.LowerSpaceCaseFormat;
import com.tinubu.projection.core.format.formats.PackageFormat;
import com.tinubu.projection.core.format.formats.ReverseDotFormat;
import com.tinubu.projection.core.format.formats.WordCapitalizeFormat;

/**
 * Additional compound formats.
 */
public class CompoundFormatFactory implements FormatFactory {

   @Override
   public List<Format> formats() {
      return list(lowercaseCapitalize(),
                  spaceCapitalize(),
                  spaceWordCapitalize(),
                  dotPackage(),
                  reverseDotPackage());
   }

   private static Format lowercaseCapitalize() {
      return new AliasFormat(list("lowercase-capitalize", "lowercap"),
                             list(new LowerCaseFormat(), new CapitalizeFormat()));
   }

   private static Format spaceCapitalize() {
      return new AliasFormat(list("space-capitalize", "spacecap"),
                             list(new LowerSpaceCaseFormat(), new CapitalizeFormat()));
   }

   private static Format spaceWordCapitalize() {
      return new AliasFormat(list("space-word-capitalize", "spacewordcap"),
                             list(new LowerSpaceCaseFormat(), new WordCapitalizeFormat()));
   }

   private static Format dotPackage() {
      return new AliasFormat(list("dot-package", "dotpack"),
                             list(new LowerDotCaseFormat(), new PackageFormat()));
   }

   private static Format reverseDotPackage() {
      return new AliasFormat(list("reverse-dot-package", "reversedotpack"),
                             list(new LowerDotCaseFormat(), new ReverseDotFormat(), new PackageFormat()));
   }
}
