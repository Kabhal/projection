/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token.codepointset;

import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static java.util.stream.IntStream.of;
import static java.util.stream.IntStream.rangeClosed;

import java.util.Iterator;

public class WordSet extends CodePointSet {

   @Override
   public boolean containsCodePoint(int i) {
      return (i >= 'a' && i <= 'z') || (i >= 'A' && i <= 'Z') || (i >= '0' && i <= '9') || (i == '_');
   }

   @Override
   public Iterator<Integer> iterator() {
      return streamConcat(rangeClosed('a', 'z'),
                          rangeClosed('A', 'Z'),
                          rangeClosed('0', '9'),
                          of('_')).iterator();
   }

   @Override
   public int size() {
      return 2 * 23 + 10 + 1;
   }
}
