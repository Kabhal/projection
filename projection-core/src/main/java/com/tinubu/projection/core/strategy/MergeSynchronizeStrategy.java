/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.strategy;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.OptionalUtils.instanceOf;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.ports.document.git.Constants.DEFAULT_BRANCH;
import static com.tinubu.commons.ports.document.git.ContentMergeStrategy.CONFLICT;
import static com.tinubu.commons.ports.document.git.FastForward.FAST_FORWARD;

import java.io.IOException;
import java.util.Optional;
import java.util.StringJoiner;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevSort;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.Constants;
import com.tinubu.commons.ports.document.git.ContentMergeStrategy;
import com.tinubu.commons.ports.document.git.FastForward;
import com.tinubu.commons.ports.document.git.GitDocumentRepository;
import com.tinubu.commons.ports.document.git.MergeStatus;
import com.tinubu.commons.ports.document.git.exception.GitException;
import com.tinubu.commons.ports.document.git.exception.MergeFailedGitException;
import com.tinubu.projection.core.ProjectionProperties;
import com.tinubu.projection.core.ProjectionProperties.Mappers;
import com.tinubu.projection.core.ProjectionProperties.View;

/**
 * Strategy relying on GIT and merge to synchronize changes.
 * <p>
 * The principle is to maintain a "projection"-dedicated branch starting from current "working" branch's
 * root.
 * The synchronization is made on this branch which is then merged into current branch, so that changes can
 * be made on current working branch, after synchronization, and memorized thanks to GIT.
 * <p>
 * It's normal that conflicts occurs in the process. In this case the synchronization fails with some
 * errors, and you have to manually correct the conflicts and finish the merge.
 * <p>
 * The following properties are used :
 * <ul>
 *    <li>{@code projection.strategy.merge.init-if-missing} [{@value DEFAULT_INIT_IF_MISSING}] : whether to init GIT repository if needed</li>
 *    <li>{@code projection.strategy.merge.initial-branch} [{@value Constants#DEFAULT_BRANCH}] : initial branch name to use when initializing repository</li>
 *    <li>{@code projection.strategy.merge.remote} [{@value DEFAULT_PROJECTION_REMOTE}] : remote name to use when synchronizing</li>
 *    <li>{@code projection.strategy.merge.branch} [{@code <current-branch>}] : optional working branch, use current branch if unset</li>
 *    <li>{@code projection.strategy.merge.projection-branch} [{@value DEFAULT_PROJECTION_BRANCH}] : projection branch name used for synchronization (optional {@code %s} is replaced with current branch name)</li>
 *    <li>{@code projection.strategy.merge.ff} [{@code FAST_FORWARD}] : merge fast-forward mode ({@code FAST_FORWARD} | {@code NO_FAST_FORWARD} | {@code FAST_FORWARD_ONLY})</li>
 *    <li>{@code projection.strategy.merge.content-merge-strategy} [{@code CONFLICT}] : merge conflict resolution strategy ({@code CONFLICT} | {@code OURS} | {@code THEIRS})</li>
 * </ul>
 */
public class MergeSynchronizeStrategy implements SynchronizeStrategy {

   /**
    * Default projection branch name to support strategy. Can contain '%s' to replace with current branch
    * name.
    */
   private static final String DEFAULT_PROJECTION_REMOTE = "projection";
   private static final String DEFAULT_PROJECTION_BRANCH = "projection";
   private static final FastForward DEFAULT_MERGE_FAST_FORWARD = FAST_FORWARD;
   private static final ContentMergeStrategy DEFAULT_CONTENT_MERGE_STRATEGY = CONFLICT;
   private static final String MERGE_STRATEGY_NAME = "merge";
   /** Whether to initialize GIT repository if missing. */
   private static final boolean DEFAULT_INIT_IF_MISSING = true;
   /** Prefix all commit messages with this prefix. */
   private static final String COMMIT_MESSAGE_PREFIX = "Projection: ";
   /** Prefix all messages (logs, exceptions) with this prefix. */
   private static final String MESSAGE_PREFIX = "[merge] ";

   private static final Logger log = LoggerFactory.getLogger(MergeSynchronizeStrategy.class);

   private final DocumentRepository templateRepository;
   private final FsDocumentRepository projectRepository;
   private final SynchronizeStrategy delegateStrategy;
   private final String currentBranch;
   private final GitDocumentRepository gitProjectRepository;
   private final String projectionBranch;
   private final FastForward mergeFastForward;
   private final ContentMergeStrategy contentMergeStrategy;

   public MergeSynchronizeStrategy(boolean overwrite,
                                   DocumentRepository templateRepository,
                                   DocumentRepository projectRepository,
                                   ProjectionProperties properties) {
      validate(projectRepository,
               "projectRepository",
               isNotNull().andValue(satisfies(isInstanceOf(value(FsDocumentRepository.class)).or(isInstanceOf(
                                                    value(GitDocumentRepository.class))),
                                              "project repository must be a file-system repository when using '%s' strategy",
                                              MessageValue.value(MERGE_STRATEGY_NAME))))
            .and(validate(templateRepository, "templateRepository", isNotNull()))
            .and(validate(properties, "properties", isNotNull()))
            .orThrow();

      this.templateRepository = templateRepository;
      this.projectRepository = instanceOf(projectRepository, GitDocumentRepository.class)
            .map(GitDocumentRepository::localRepository)
            .orElseGet(() -> (FsDocumentRepository) projectRepository);

      View mergeProperties = properties.view("projection.strategy.merge");

      var initIfMissing =
            mergeProperties.value("init-if-missing", Mappers::booleanValue).orElse(DEFAULT_INIT_IF_MISSING);
      var initialBranch = mergeProperties.value("initial-branch", isNotBlank()).orElse(DEFAULT_BRANCH);
      var remote = mergeProperties.value("remote", isNotBlank()).orElse(DEFAULT_PROJECTION_REMOTE);
      var branch = mergeProperties.value("branch", isNotBlank()).orElse(null);

      try {
         this.gitProjectRepository = GitDocumentRepository.ofLocalRepository(this.projectRepository,
                                                                             b -> b
                                                                                   .initIfMissing(
                                                                                         initIfMissing)
                                                                                   .remote(remote)
                                                                                   .initialBranch(
                                                                                         initialBranch)
                                                                                   .branch(branch)
                                                                                   .forceLocalUri(true));
      } catch (GitException e) {
         throw new IllegalStateException(String.format(MESSAGE_PREFIX
                                                       + "Can't initialize GIT repository : %s",
                                                       e.getMessage()), e);
      }

      this.delegateStrategy =
            new DirectSynchronizeStrategy(overwrite, templateRepository, gitProjectRepository, properties);

      this.currentBranch = gitProjectRepository.branch();

      this.projectionBranch =
            projectionBranch(mergeProperties.value("projection-branch").orElse(DEFAULT_PROJECTION_BRANCH),
                             currentBranch);
      this.mergeFastForward =
            mergeProperties.value("ff", Mappers::fastForward).orElse(DEFAULT_MERGE_FAST_FORWARD);
      this.contentMergeStrategy = mergeProperties
            .value("content-merge-strategy", Mappers::contentMergeStrategy)
            .orElse(DEFAULT_CONTENT_MERGE_STRATEGY);

      if (!gitProjectRepository.isClean()) {
         throw new SynchronizeStrategyException(String.format(MESSAGE_PREFIX
                                                              + "GIT '%s' repository is not clean, manually commit the changes first",
                                                              projectRepository));
      }

      RevCommit rootCommit = branchRootCommit()
            .or(() -> {
               if (initIfMissing) {
                  gitProjectRepository.commitIndexedDocuments(COMMIT_MESSAGE_PREFIX
                                                              + "Initial repository commit",
                                                              false,
                                                              true);
                  return branchRootCommit();
               } else {
                  return optional();
               }
            })
            .orElseThrow(() -> new SynchronizeStrategyException(String.format(MESSAGE_PREFIX
                                                                              + "Can't initialize GIT '%s' repository, manually create a default branch first",
                                                                              projectRepository)));
      var projectionStartRevision = rootCommit.getName();

      log.info(MESSAGE_PREFIX
               + "Set 'merge' strategy to synchronize current '{}' branch. Use '{}' as projection branch, branched from {} revision",
               currentBranch,
               projectionBranch,
               projectionStartRevision);

      gitProjectRepository.checkout(projectionBranch, true, projectionStartRevision);
   }

   private String projectionBranch(String projectionBranchPattern, String currentBranch) {
      return String.format(projectionBranchPattern, currentBranch);
   }

   @Override
   public String name() {
      return MERGE_STRATEGY_NAME;
   }

   /**
    * Returns the current branch root commit (first commit without parents). It's theoretically possible to
    * have several root commits, in this case, returns the first one of these.
    *
    * @return current branch root commit, or {@link Optional#empty} if none found
    */
   private Optional<RevCommit> branchRootCommit() {
      Git git = gitProjectRepository.git();

      try (RevWalk walk = new RevWalk(git.getRepository())) {
         ObjectId from = git.getRepository().resolve("HEAD");
         if (from == null) {
            return optional();
         }
         walk.markStart(walk.parseCommit(from));
         walk.setRetainBody(false);
         walk.sort(RevSort.REVERSE);
         walk.setRevFilter(new RevFilter() {
            @Override
            public boolean include(RevWalk walker, RevCommit c) {
               return c.getParentCount() == 0;
            }

            @Override
            public boolean requiresCommitBody() {
               return false;
            }

            @Override
            public RevFilter clone() {
               return this;
            }

            @Override
            public String toString() {
               return "BRANCH_ROOT";
            }
         });
         return optional(walk.next());
      } catch (IOException e) {
         throw new SynchronizeStrategyException(e);
      }
   }

   @Override
   public Optional<DocumentEntry> synchronizeDocument(Document templateDocument, Document projectDocument) {
      return this.delegateStrategy.synchronizeDocument(templateDocument, projectDocument);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document projectDocument) throws SynchronizeStrategyException {
      return this.delegateStrategy.saveDocument(projectDocument);
   }

   @Override
   public Optional<DocumentEntry> saveDocument(Document projectDocument, boolean overwrite) {
      return this.delegateStrategy.saveDocument(projectDocument, overwrite);
   }

   @Override
   public void finish() throws SynchronizeStrategyException {
      try {
         merge();
      } finally {
         gitProjectRepository.close();
      }
   }

   private void merge() {
      try {
         gitProjectRepository.addAllDocumentsToIndex();
         gitProjectRepository.commitIndexedDocuments(synchronizationCommitMessage(), false, true);
         gitProjectRepository.checkout(currentBranch, false);
         MergeStatus mergeStatus = gitProjectRepository.merge(projectionBranch,
                                                              mergeFastForward,
                                                              contentMergeStrategy,
                                                              COMMIT_MESSAGE_PREFIX
                                                              + "merge synchronization");
         log.info(MESSAGE_PREFIX + "Merge successful with '{}' status", mergeStatus);
      } catch (MergeFailedGitException e) {
         throw new SynchronizeStrategyException(String.format(MESSAGE_PREFIX
                                                              + "Merge failed with conflicts, please solve conflicts and merge manually > %s",
                                                              e.getMessage()), e);
      }
   }

   private String synchronizationCommitMessage() {
      return String.format(COMMIT_MESSAGE_PREFIX + "synchronize '%s' -> '%s'",
                           templateRepository.toUri(),
                           projectRepository.toUri());
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", MergeSynchronizeStrategy.class.getSimpleName() + "[", "]")
            .add("templateRepository=" + templateRepository)
            .add("projectRepository=" + projectRepository)
            .add("delegateStrategy=" + delegateStrategy)
            .toString();
   }

   public static class MergeSynchronizeStrategyFactory implements SynchronizeStrategyFactory {

      @Override
      public String name() {
         return MERGE_STRATEGY_NAME;
      }

      @Override
      public SynchronizeStrategy synchronizeStrategy(boolean overwrite,
                                                     DocumentRepository templateRepository,
                                                     DocumentRepository projectRepository,
                                                     ProjectionProperties properties) {
         return new MergeSynchronizeStrategy(overwrite, templateRepository, projectRepository, properties);
      }
   }

}