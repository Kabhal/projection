/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token.codepointset;

import static com.tinubu.commons.lang.util.CollectionUtils.set;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;

import java.util.AbstractSet;

import com.tinubu.commons.ddd2.invariant.Validate.Check;

/**
 * A set of Unicode code points.
 */
public abstract class CodePointSet extends AbstractSet<Integer> {

   @Override
   public boolean contains(Object o) {
      int i;
      if (o instanceof Integer) {
         i = (int) o;
      } else if (o instanceof Character) {
         i = (char) o;
      } else {
         return false;
      }
      return containsCodePoint(i);
   }

   protected abstract boolean containsCodePoint(int i);

   public static CodePointSet mergeCodePointSet(final CodePointSet... codePointSets) {
      Check.notNull(codePointSets, "codePointSets");

      if (codePointSets.length == 0) {
         return EmptySet.ofEmpty();
      } else if (codePointSets.length == 1 && codePointSets[0] != null) {
         return codePointSets[0];
      } else {
         return GenericSet.of(set(streamConcat(codePointSets)));
      }
   }
}
