/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token.matcher;

import static com.tinubu.projection.core.format.token.IdentifierTokenizer.IdentifierMatcher;

import com.tinubu.projection.core.format.token.codepointset.AlphaSet;
import com.tinubu.projection.core.format.token.codepointset.AlphanumSet;
import com.tinubu.projection.core.format.token.codepointset.BlankSet;
import com.tinubu.projection.core.format.token.codepointset.EmptySet;
import com.tinubu.projection.core.format.token.codepointset.NumSet;
import com.tinubu.projection.core.format.token.codepointset.WordSet;

/**
 * Pre-registered {@link TokenMatcher}.
 */
public final class TokenMatchers {

   private TokenMatchers() {
   }

   public static TokenMatcher empty() {
      return TokenClass.ofIncludeSets("empty", EmptySet.ofEmpty());
   }

   public static TokenMatcher blank() {
      return TokenClass.ofIncludeSets("blank", new BlankSet());
   }

   public static TokenMatcher any() {
      return TokenClass.ofExcludeSets("any", EmptySet.ofEmpty());
   }

   public static TokenMatcher word() {
      return TokenClass.ofIncludeSets("word", new WordSet());
   }

   public static TokenMatcher notWord() {
      return TokenClass.ofExcludeSets("not-word", new WordSet());
   }

   public static TokenMatcher alphanum() {
      return TokenClass.ofIncludeSets("alpha-num", new AlphanumSet());
   }

   public static TokenMatcher notAlphanum() {
      return TokenClass.ofExcludeSets("not-alphanum", new AlphanumSet());
   }

   public static TokenMatcher lowerAlphanum() {
      return TokenClass.ofIncludeSets("lower-alphanum", new AlphanumSet(true, false));
   }

   public static TokenMatcher notLowerAlphanum() {
      return TokenClass.ofExcludeSets("not-lower-alphanum", new AlphanumSet(true, false));
   }

   public static TokenMatcher upperAlphanum() {
      return TokenClass.ofIncludeSets("upper-alphanum", new AlphanumSet(false, true));
   }

   public static TokenMatcher notUpperAlphanum() {
      return TokenClass.ofExcludeSets("not-upper-alphanum", new AlphanumSet(false, true));
   }

   public static TokenMatcher alpha() {
      return TokenClass.ofIncludeSets("alpha", new AlphaSet());
   }

   public static TokenMatcher notAlpha() {
      return TokenClass.ofExcludeSets("not-alpha", new AlphaSet());
   }

   public static TokenMatcher lowerAlpha() {
      return TokenClass.ofIncludeSets("lower-alpha", new AlphaSet(true, false));
   }

   public static TokenMatcher notLowerAlpha() {
      return TokenClass.ofExcludeSets("not-lower-alpha", new AlphaSet(true, false));
   }

   public static TokenMatcher upperAlpha() {
      return TokenClass.ofIncludeSets("upper-alpha", new AlphaSet(false, true));
   }

   public static TokenMatcher notUpperAlpha() {
      return TokenClass.ofExcludeSets("not-upper-alpha", new AlphaSet(false, true));
   }

   public static TokenMatcher num() {
      return TokenClass.ofIncludeSets("num", new NumSet());
   }

   public static TokenMatcher notNum() {
      return TokenClass.ofExcludeSets("not-num", new NumSet());
   }

   public static IdentifierMatcher identifier(boolean splitNumbers) {
      return IdentifierMatcher.of(splitNumbers);
   }

   public static IdentifierMatcher identifier() {
      return IdentifierMatcher.ofDefault();
   }
}
