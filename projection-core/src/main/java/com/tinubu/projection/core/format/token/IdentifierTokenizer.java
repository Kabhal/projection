/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.Optional;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.projection.core.format.token.codepointset.AlphaSet;
import com.tinubu.projection.core.format.token.codepointset.NumSet;
import com.tinubu.projection.core.format.token.matcher.TokenMatcher;

/**
 * Specialized tokenizers for code "identifiers" (variable names, ...).
 * <p>
 * All non-alphanumeric character is discarded.
 * Upper/lower case is left untouched in resulting tokens.
 * <p>
 * This tokenizer split any identifier into tokens :
 * <ul>
 *    <li>space separated : {@code This value} -&gt; ({@code This}, {@code value})</li>
 *    <li>lower snake_case : {@code this_value} -&gt; ({@code this}, {@code value})</li>
 *    <li>upper snake_case : {@code THIS_VALUE} -&gt; ({@code THIS}, {@code VALUE})</li>
 *    <li>lower kebab-case : {@code this-value} -&gt; ({@code this}, {@code value})</li>
 *    <li>upper kebab-case : {@code THIS-VALUE} -&gt; ({@code THIS}, {@code VALUE})</li>
 *    <li>lower camelCase: {@code thisValue} -&gt; ({@code this}, {@code Value})</li>
 *    <li>upper camelCase: {@code ThisValue} -&gt; ({@code This}, {@code Value})</li>
 * </ul>
 * <p>
 * Advanced rules :
 * <ul>
 *    <li>Upper blocks are preserved : {@code ThisIOValue} -&gt; ({@code This}, {@code IO}, {@code Value})</li>
 *    <li>Numbers are tokenized following {@code splitNumbers} flag value :
 *    <ul>
 *       <li>{@code splitNumbers=true} (default) {@code This20Value} -&gt; ({@code This}, {@code 20}, {@code Value})</li>
 *       <li>{@code splitNumbers=false} {@code This20Value} -&gt; ({@code This20}, {@code Value})</li>
 *       <li>{@code splitNumbers=false} {@code THIS20Value} -&gt; ({@code THIS20}, {@code Value})</li>
 *       <li>{@code splitNumbers=false} {@code This-20Value} -&gt; ({@code This}, {@code 20Value})</li>
 *    </ul>
 *    </li>
 * </ul>
 *
 * @implSpec Leading and trailing underscores ({@code _}) are not supported in identifiers and are
 *       always discarded. There are ambiguities with underscores, for example : {@code __variable_name__}
 *       could be tokenized as ({@code __variable}, {@code name__}), then later converted to camelCase :
 *       {@code __variableName__} or kebab-case : {@code __variable-name__} which is not obvious.
 */
public class IdentifierTokenizer extends DefaultTokenizer {

   protected IdentifierTokenizer(TokenMatcher matcher) {
      super(list(matcher));
   }

   public static IdentifierTokenizer ofDefault() {
      return new IdentifierTokenizer(IdentifierMatcher.ofDefault());
   }

   public static IdentifierTokenizer of(boolean splitNumbers) {
      return new IdentifierTokenizer(IdentifierMatcher.of(splitNumbers));
   }

   public static class IdentifierMatcher implements TokenMatcher {

      private static final String NAME = "identifier";
      private static final boolean DEFAULT_SPLIT_NUMBERS = true;

      private static final AlphaSet LOWER_ALPHA = new AlphaSet(true, false);
      private static final AlphaSet UPPER_ALPHA = new AlphaSet(false, true);
      private static final NumSet NUM = new NumSet();

      private final boolean splitNumbers;

      protected IdentifierMatcher(boolean splitNumbers) {
         this.splitNumbers = splitNumbers;
      }

      public static IdentifierMatcher ofDefault() {
         return new IdentifierMatcher(DEFAULT_SPLIT_NUMBERS);
      }

      public static IdentifierMatcher of(boolean splitNumbers) {
         return new IdentifierMatcher(splitNumbers);
      }

      @Override
      public String name() {
         return NAME;
      }

      @Override
      public Optional<Token> matches(String input, int fromIndex, int toIndex) {
         Check.notNull(input, "input");
         Check.validate(fromIndex, "fromIndex", isPositive());
         Check.validate(toIndex,
                        "toIndex",
                        isGreaterThanOrEqualTo(value(fromIndex, "fromIndex")).andValue(isLessThanOrEqualTo(
                              lazyValue(input::length, "input.length"))));

         int mode = 0;
         var cpIndex = fromIndex;
         while (cpIndex < toIndex) {
            var cp = input.codePointAt(cpIndex);
            var cpMode = parseMode(cp);

            if (mode == 0) {
               /* initialization : ø -> *<any>* */
               mode = cpMode;
            } else if (mode == cpMode) {
               if (mode == 2 && toIndex - cpIndex > 1 && parseMode(input.codePointAt(cpIndex + 1)) == 3) {
                  /* upperAlpha -> *upperAlpha* -> lowerAlpha */
                  break;
               } else {
                  /* same mode : <any> -> *<any>* */
               }
            } else {
               if (mode == 2 && cpMode == 3 && (cpIndex - fromIndex) == 1) {
                  /* upperAlpha{1} -> *lowerAlpha* */
                  mode = 3;
               } else if (!splitNumbers && (mode == 2 || mode == 4) && (cpMode == 2 || cpMode == 4)) {
                  /* if!numericTokens( upperAlpha|num -> *upperAlpha|num* ) */
                  mode = 2;
               } else if (!splitNumbers && (mode == 3 || mode == 4) && (cpMode == 3 || cpMode == 4)) {
                  /* if!numericTokens( lowerAlpha|num -> *lowerAlpha|num* ) */
                  mode = 3;
               } else {
                  /* all other mode transitions */
                  break;
               }
            }

            cpIndex += Character.charCount(cp);
         }

         if (cpIndex - fromIndex == 0 || mode == 1) {
            return optional();
         } else {
            return optional(Token.of(this, fromIndex, cpIndex));
         }
      }

      /**
       * Selects parsing mode for code point.
       * <p>
       * <ul>
       * <li>0 = no mode</li>
       * <li>1 = notAlphanum</li>
       * <li>2 = upperAlpha</li>
       * <li>3 = lowerAlpha</li>
       * <li>4 = num</li>
       * </ul>
       */
      private int parseMode(int cp) {
         if (LOWER_ALPHA.containsCodePoint(cp)) {
            return 3;
         } else if (UPPER_ALPHA.containsCodePoint(cp)) {
            return 2;
         } else if (NUM.containsCodePoint(cp)) {
            return 4;
         } else {
            return 1;
         }
      }
   }

}
