/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Optional;
import java.util.stream.Stream;

import com.tinubu.commons.lang.io.contentloader.ContentLoader;
import com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader.SourceContentServiceLoader;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentFactory;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;

/**
 * {@link SourceContentServiceLoader} for {@link DocumentRepository} adapter.
 */
public class DocumentRepositorySourceContentLoader implements SourceContentServiceLoader {

   @Override
   public Stream<ContentLoader> loadSourceContentLoaders() {
      return stream(new DocumentRepositoryContentLoader());
   }

   public static class DocumentRepositoryContentLoader implements ContentLoader {

      private static final DocumentFactory documentFactory = new DocumentFactory();

      @Override
      public Optional<Content> loadContent(URI source) {
         var document = documentFactory.document(DocumentUri.ofDocumentUri(source));

         return document.flatMap(d -> d.document(true, false)).map(DocumentContent::new);
      }

      public static class DocumentContent implements Content {

         private final Document document;

         public DocumentContent(Document document) {
            this.document = document;
         }

         @Override
         public InputStream inputStream() {
            return document.content().inputStreamContent();
         }

         @Override
         public Optional<Charset> encoding() {
            return document.metadata().contentEncoding();
         }
      }
   }
}
