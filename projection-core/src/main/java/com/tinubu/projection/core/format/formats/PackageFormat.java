/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.formats;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.io.File;
import java.util.List;

import com.tinubu.projection.core.format.AbstractFormat;
import com.tinubu.projection.core.format.token.DefaultTokenizer;
import com.tinubu.projection.core.format.token.codepointset.GenericSet;
import com.tinubu.projection.core.format.token.matcher.TokenClass;
import com.tinubu.projection.core.format.token.matcher.TokenMatcher;

/**
 * Transforms dot-separated content into path.
 * <p>
 * By default, path separator comes from system's {@link File#separator}.
 * <p>
 * Main use case is to support package-to-path translation in directory paths.
 */
public class PackageFormat extends AbstractFormat {

   private static final List<String> NAMES = list("package", "pack");

   private static final GenericSet DOT_SET = GenericSet.of(".");
   private static final TokenMatcher DOT_MATCHER = TokenClass.ofIncludeSets("dot", DOT_SET);
   private static final TokenMatcher NOT_DOT_MATCHER = TokenClass.ofExcludeSets("not-dot", DOT_SET);
   private static final String DEFAULT_PATH_SEPARATOR = File.separator;

   private final String pathSeparator;

   public PackageFormat(String pathSeparator) {
      this.pathSeparator = pathSeparator;
   }

   public PackageFormat() {
      this(DEFAULT_PATH_SEPARATOR);
   }

   @Override
   public List<String> names() {
      return NAMES;
   }

   @Override
   public int inferenceOrder() {
      return 8000;
   }

   @Override
   public String lowFormat(String content) {
      var tokens = DefaultTokenizer.of(DOT_MATCHER, NOT_DOT_MATCHER).tokens(content);

      return stream(tokens).reduce("", (s, token) -> {
         var ts = token.hasTokenMatcher(NOT_DOT_MATCHER) ? token.subString(content) : pathSeparator;

         return s + ts;
      }, String::concat);
   }

}
