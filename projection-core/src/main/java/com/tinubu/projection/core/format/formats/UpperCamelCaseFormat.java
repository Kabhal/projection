/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.formats;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StringUtils.capitalize;

import java.util.List;

import com.tinubu.projection.core.format.AbstractFormat;
import com.tinubu.projection.core.format.token.IdentifierTokenizer;

/**
 * Upper camel case.
 * <p>
 * Non-alphanumeric characters are removed.
 */
public class UpperCamelCaseFormat extends AbstractFormat {

   private static final List<String> NAMES = list("upper-camelcase", "uppercamelcase", "CamelCase", "Camel");
   private static final boolean SPLIT_NUMBERS = true;

   @Override
   public List<String> names() {
      return NAMES;
   }

   @Override
   public int inferenceOrder() {
      return 3100;
   }

   @Override
   public String lowFormat(String content) {
      return IdentifierTokenizer.of(SPLIT_NUMBERS).tokens(content).reduce("", (s, token) -> {
         return s + capitalize(token.subString(content).toLowerCase());
      }, String::concat);
   }

}
