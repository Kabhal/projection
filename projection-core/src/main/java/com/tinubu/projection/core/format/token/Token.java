/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;

import java.util.StringJoiner;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.projection.core.format.token.matcher.TokenMatcher;

/**
 * A token is an index range [fromIndex, toIndex[ associated with a token class.
 * For example, it can identify a range of sequential characters from the same token matcher in a string.
 */
public class Token {
   protected final TokenMatcher tokenMatcher;
   protected final int fromIndex;
   protected final int toIndex;

   protected Token(TokenMatcher tokenMatcher, int fromIndex, int toIndex) {
      this.tokenMatcher = Check.notNull(tokenMatcher, "tokenMatcher");
      this.fromIndex = Check.validate(fromIndex, "fromIndex", isPositive());
      this.toIndex =
            Check.validate(toIndex, "toIndex", isGreaterThanOrEqualTo(value(fromIndex, "fromIndex")));
   }

   public static Token of(TokenMatcher tokenMatcher, int fromIndex, int toIndex) {
      return new Token(tokenMatcher, fromIndex, toIndex);
   }

   public TokenMatcher tokenMatcher() {
      return tokenMatcher;
   }

   public int length() {
      return toIndex - fromIndex;
   }

   public int fromIndex() {
      return fromIndex;
   }

   public int toIndex() {
      return toIndex;
   }

   public Pair<Integer, Integer> indexRange() {
      return Pair.of(fromIndex, toIndex);
   }

   /**
    * Returns the conjunction of this token with specified token, under conditions :
    * <ul>
    *    <li>specified token range immediately follow this token range</li>
    *    <li>matchers must be equal</li>
    * </ul>
    */
   public Token conjunction(Token token) {
      Check.validate(token,
                     "token",
                     property(Token::fromIndex, "fromIndex", isEqualTo(value(toIndex, "toIndex"))).andValue(
                           property(t -> t.tokenMatcher().name(),
                                    "tokenMatcher.name",
                                    isEqualTo(value(tokenMatcher.name(), "tokenMatcher.name")))));

      return Token.of(tokenMatcher, fromIndex, token.toIndex());
   }

   /**
    * Checks if this token results from specified token matcher.
    *
    * @param tokenMatcher originating token matcher
    *
    * @return {@code true} if this token results from specified token matcher
    */
   public boolean hasTokenMatcher(TokenMatcher tokenMatcher) {
      Check.notNull(tokenMatcher, "tokenMatcher");

      return this.tokenMatcher.sameEntityAs(tokenMatcher);
   }

   /**
    * Checks if this token results from specified token matcher.
    * <p>
    * For performance, prefer to use {@link #hasTokenMatcher(TokenMatcher)} if you own a reference on token
    * matcher
    *
    * @param name originating token matcher's name
    *
    * @return {@code true} if this token results from specified token matcher
    */
   public boolean hasTokenMatcher(String name) {
      Check.notBlank(name, "name");

      return this.tokenMatcher.name().equals(name);
   }

   public String subString(String string) {
      return string.substring(fromIndex, toIndex);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Token.class.getSimpleName() + "[", "]")
            .add("tokenMatcher=" + tokenMatcher)
            .add("fromIndex=" + fromIndex)
            .add("toIndex=" + toIndex)
            .toString();
   }
}
