/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import static com.tinubu.commons.lang.util.CollectionUtils.listIntersection;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.stream.Collectors.joining;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

public abstract class AbstractFormat implements Format {

   protected abstract String lowFormat(String content);

   @Override
   public String format(String content) {
      String formattedContent;

      formattedContent = lowFormat(content);

      return formattedContent;
   }

   protected String formatPath(String content,
                               String contentSeparator,
                               String pathSeparator,
                               Function<String, String> sectionFormatter) {
      String[] contentParts = StringUtils.split(content, contentSeparator);

      return stream(contentParts).map(sectionFormatter).collect(joining(pathSeparator));
   }

   public boolean anyNameMatch(Format format) {
      return !listIntersection(names(), format.names()).isEmpty();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Format that = (Format) o;
      return Objects.equals(names(), that.names());
   }

   @Override
   public int hashCode() {
      return Objects.hash(names());
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
            .add("names='" + names() + "'")
            .toString();
   }
}
