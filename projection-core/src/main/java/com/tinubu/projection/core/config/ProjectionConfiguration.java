/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.config;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoBlankElements;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoDuplicates;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullValues;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.keys;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;

import java.net.URI;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.ddd2.invariant.rules.MapRules;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.PathUtils;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.projection.core.Attribute;
import com.tinubu.projection.core.PlaceholderContext;
import com.tinubu.projection.core.PlaceholderFormat;
import com.tinubu.projection.core.format.Format;

public abstract class ProjectionConfiguration extends AbstractValue {

   private final URI templateUri;
   private final Map<String, Attribute> attributes;
   private final Map<String, String> extraFormats;
   private final Set<String> restrictedFormats;
   private final PlaceholderContext placeholderContext;
   private final BiDirectionalFilter synchronizeFilter;
   private final BiDirectionalFilter replaceContentFilter;
   private final Map<String, String> properties;

   protected ProjectionConfiguration(Builder builder) {
      this.templateUri = builder.templateUri;
      this.attributes = immutable(map(() -> new LinkedHashMap<>(),
                                      builder.attributes.stream().map(a -> Pair.of(a.decoratedName(), a))));
      this.extraFormats = immutable(builder.extraFormats);
      this.restrictedFormats = immutable(builder.restrictedFormats);
      this.placeholderContext = nullable(builder.placeholderContext, PlaceholderContext.ofDefault());
      this.synchronizeFilter = nullable(builder.synchronizeFilter, BiDirectionalFilter.empty());
      this.replaceContentFilter = nullable(builder.replaceContentFilter, BiDirectionalFilter.empty());
      this.properties = nullable(builder.properties, map());
   }

   @Override
   protected Fields<? extends ProjectionConfiguration> defineDomainFields() {
      return Fields
            .<ProjectionConfiguration>builder()
            .field("templateUri", v -> v.templateUri)
            .field("attributes", v -> v.attributes, MapRules.hasNoNullElements())
            .field("extraFormats", v -> v.extraFormats, MapRules.hasNoNullElements())
            .field("restrictedFormats", v -> v.restrictedFormats, hasNoBlankElements())
            .field("placeholderContext", v -> v.placeholderContext, isNotNull())
            .field("synchronizeIncludeExclude", v -> v.synchronizeFilter, isNotNull())
            .field("replaceContentIncludeExclude", v -> v.replaceContentFilter, isNotNull())
            .field("properties",
                   v -> v.properties,
                   hasNoNullValues().andValue(keys(allSatisfies(isNotBlank()))))
            .build();
   }

   public Optional<URI> templateUri() {
      return nullable(templateUri);
   }

   public List<Attribute> attributes() {
      return list(attributes.values());
   }

   public Optional<Attribute> attribute(String name) {
      return nullable(attributes.get(name));
   }

   /**
    * Additional list of formats, in their specification form :
    * {@code name1,[name2[,...]]} -&gt; {@code format1[,format2[,...]]}
    */
   public Map<String, String> extraFormats() {
      return extraFormats;
   }

   /**
    * Optional restricted set of {@link Format#name()} to use for placeholders inference.
    * Use all registered formats if set is empty.
    */
   public Set<String> restrictedFormats() {
      return restrictedFormats;
   }

   public PlaceholderContext placeholderContext() {
      return placeholderContext;
   }

   public PlaceholderFormat placeholderFormat() {
      return placeholderContext.format();
   }

   public BiDirectionalFilter synchronizeFilter() {
      return synchronizeFilter;
   }

   public BiDirectionalFilter replaceContentFilter() {
      return replaceContentFilter;
   }

   public Map<String, String> properties() {
      return properties;
   }

   public static class Builder {
      private URI templateUri;
      private List<Attribute> attributes;
      private Map<String, String> extraFormats;
      private Set<String> restrictedFormats;
      private PlaceholderContext placeholderContext;
      private BiDirectionalFilter synchronizeFilter;
      private BiDirectionalFilter replaceContentFilter;
      private Map<String, String> properties;

      public static Builder from(ProjectionConfiguration projectConfiguration) {
         return new Builder()
               .templateUri(projectConfiguration.templateUri().orElse(null))
               .attributes(projectConfiguration.attributes())
               .extraFormats(projectConfiguration.extraFormats())
               .restrictedFormats(projectConfiguration.restrictedFormats())
               .placeholderContext(projectConfiguration.placeholderContext())
               .synchronizeFilter(projectConfiguration.synchronizeFilter())
               .replaceContentFilter(projectConfiguration.replaceContentFilter())
               .properties(projectConfiguration.properties());
      }

      public Builder templateUri(URI templateUri) {
         this.templateUri = templateUri;
         return this;
      }

      public Builder attributes(List<Attribute> attributes) {
         this.attributes = attributes;
         return this;
      }

      public Builder extraFormats(Map<String, String> extraFormats) {
         this.extraFormats = extraFormats;
         return this;
      }

      public Builder restrictedFormats(Set<String> restrictedFormats) {
         this.restrictedFormats = restrictedFormats;
         return this;
      }

      public Builder placeholderContext(PlaceholderContext placeholderContext) {
         this.placeholderContext = placeholderContext;
         return this;
      }

      public Builder placeholderContext(UnaryOperator<PlaceholderContext.Builder> placeholderContextBuilderMapper) {
         this.placeholderContext =
               PlaceholderContext.builder().chain(placeholderContextBuilderMapper).build();
         return this;
      }

      public Builder synchronizeFilter(BiDirectionalFilter synchronizeFilter) {
         this.synchronizeFilter = synchronizeFilter;
         return this;
      }

      public Builder replaceContentFilter(BiDirectionalFilter replaceContentFilter) {
         this.replaceContentFilter = replaceContentFilter;
         return this;
      }

      public Builder properties(Map<String, String> properties) {
         this.properties = properties;
         return this;
      }
   }

   public static class Filter extends AbstractValue {
      private final List<Path> includes;
      private final List<Path> excludes;

      protected Filter(List<Path> includes, List<Path> excludes) {
         this.includes = immutable(list(stream(includes).map(Filter::relativizePath)));
         this.excludes = immutable(list(stream(excludes).map(Filter::relativizePath)));
      }

      public static Filter empty() {
         return Filter.of(list(), list());
      }

      public static Filter of(List<Path> includes, List<Path> excludes) {
         return new Filter(includes, excludes);
      }

      public static Filter ofIncludes(List<Path> includes) {
         return new Filter(includes, list());
      }

      public static Filter ofExcludes(List<Path> excludes) {
         return new Filter(list(), excludes);
      }

      @Override
      protected Fields<? extends Filter> defineDomainFields() {
         return Fields
               .<Filter>builder()
               .field("includes", v -> v.includes, isValidPathList())
               .field("excludes", v -> v.excludes, isValidPathList())
               .build();
      }

      public List<Path> includes() {
         return includes;
      }

      public Filter includes(List<Path> includes) {
         return Filter.of(includes, excludes);
      }

      public Filter includes(Function<? super List<Path>, ? extends List<Path>> mapper) {
         Check.notNull(mapper, "mapper");

         return Filter.of(mapper.apply(includes), excludes);
      }

      public List<Path> excludes() {
         return excludes;
      }

      public Filter excludes(List<Path> excludes) {
         return Filter.of(includes, excludes);
      }

      public Filter excludes(Function<? super List<Path>, ? extends List<Path>> mapper) {
         Check.notNull(mapper, "mapper");

         return Filter.of(includes, mapper.apply(excludes));
      }

      private static InvariantRule<List<Path>> isValidPathList() {
         return CollectionRules
               .<List<Path>, Path>hasNoNullElements()
               .andValue(hasNoDuplicates())
               .andValue(allSatisfies(isNotEmpty().andValue(isNotAbsolute())));
      }

      private static Path relativizePath(Path path) {
         String[] pathParts = StringUtils.split(path.toString(), ":", 2);
         if (pathParts.length > 1 && list("regex", "glob").contains(pathParts[0])) {
            return Path.of(pathParts[0] + ':' + relativizePath(Path.of(pathParts[1])));
         } else {
            return PathUtils.removeRoot(path);
         }
      }

      public boolean isEmpty() {
         return includes.isEmpty() && excludes.isEmpty();
      }
   }

   public static class BiDirectionalFilter extends AbstractValue {
      private final Filter defaultFilter;
      private final Filter downstreamFilter;
      private final Filter upstreamFilter;

      protected BiDirectionalFilter(Filter defaultFilter, Filter downstreamFilter, Filter upstreamFilter) {
         this.defaultFilter = defaultFilter;
         this.downstreamFilter = downstreamFilter;
         this.upstreamFilter = upstreamFilter;
      }

      public static BiDirectionalFilter empty() {
         return new BiDirectionalFilter(Filter.empty(), Filter.empty(), Filter.empty());
      }

      public static BiDirectionalFilter of(Filter defaultFilter,
                                           Filter downstreamFilter,
                                           Filter upstreamFilter) {
         return new BiDirectionalFilter(defaultFilter, downstreamFilter, upstreamFilter);
      }

      public static BiDirectionalFilter ofDefaultFilter(Filter defaultFilter) {
         return new BiDirectionalFilter(defaultFilter, Filter.empty(), Filter.empty());
      }

      public static BiDirectionalFilter ofDownstreamFilter(Filter downstreamFilter) {
         return new BiDirectionalFilter(Filter.empty(), downstreamFilter, Filter.empty());
      }

      public static BiDirectionalFilter ofUpstreamFilter(Filter upstreamFilter) {
         return new BiDirectionalFilter(Filter.empty(), Filter.empty(), upstreamFilter);
      }

      @Override
      protected Fields<? extends BiDirectionalFilter> defineDomainFields() {
         return Fields
               .<BiDirectionalFilter>builder()
               .field("defaultFilter", v -> v.defaultFilter, isNotNull())
               .field("downstreamFilter", v -> v.downstreamFilter, isNotNull())
               .field("upstreamFilter", v -> v.upstreamFilter, isNotNull())
               .build();
      }

      // public static BiDirectionalFilter from(BiDirectionalFilter biDirectionalFilter) {
      //    return new BiDirectionalFilter(biDirectionalFilter.defaultFilter,
      //                                   biDirectionalFilter.downstreamFilter,
      //                                   biDirectionalFilter.upstreamFilter);
      // }

      public Filter defaultFilter() {
         return defaultFilter;
      }

      public BiDirectionalFilter defaultFilter(Filter defaultFilter) {
         return new BiDirectionalFilter(defaultFilter, downstreamFilter, upstreamFilter);
      }

      public BiDirectionalFilter defaultFilter(Function<? super Filter, ? extends Filter> mapper) {
         Check.notNull(mapper, "mapper");

         return new BiDirectionalFilter(mapper.apply(defaultFilter), downstreamFilter, upstreamFilter);
      }

      public Filter downstreamFilter() {
         return downstreamFilter;
      }

      public BiDirectionalFilter downstreamFilter(Filter downstreamFilter) {
         return new BiDirectionalFilter(defaultFilter, downstreamFilter, upstreamFilter);
      }

      public BiDirectionalFilter downstreamFilter(Function<? super Filter, ? extends Filter> mapper) {
         Check.notNull(mapper, "mapper");

         return new BiDirectionalFilter(defaultFilter, mapper.apply(downstreamFilter), upstreamFilter);
      }

      public Filter upstreamFilter() {
         return upstreamFilter;
      }

      public BiDirectionalFilter upstreamFilter(Filter upstreamFilter) {
         return new BiDirectionalFilter(defaultFilter, downstreamFilter, upstreamFilter);
      }

      public BiDirectionalFilter upstreamFilter(Function<? super Filter, ? extends Filter> mapper) {
         Check.notNull(mapper, "mapper");

         return new BiDirectionalFilter(defaultFilter, downstreamFilter, mapper.apply(upstreamFilter));
      }

      public Optional<DocumentEntrySpecification> downstreamDocumentEntrySpecification(
            DocumentEntrySpecification defaultIncludeSpecification,
            DocumentEntrySpecification defaultExcludeSpecification) {
         return documentEntrySpecification(true,
                                           false,
                                           defaultIncludeSpecification,
                                           defaultExcludeSpecification);
      }

      public Optional<DocumentEntrySpecification> upstreamDocumentEntrySpecification(
            DocumentEntrySpecification defaultIncludeSpecification,
            DocumentEntrySpecification defaultExcludeSpecification) {
         return documentEntrySpecification(false,
                                           true,
                                           defaultIncludeSpecification,
                                           defaultExcludeSpecification);
      }

      private Optional<DocumentEntrySpecification> documentEntrySpecification(boolean downstream,
                                                                              boolean upstream,
                                                                              DocumentEntrySpecification defaultIncludeSpecification,
                                                                              DocumentEntrySpecification defaultExcludeSpecification) {
         List<Path> includes = list(streamConcat(stream(defaultFilter.includes()),
                                                 downstream ? stream(downstreamFilter.includes()) : stream(),
                                                 upstream ? stream(upstreamFilter.includes()) : stream()));

         Optional<DocumentEntrySpecification> includeSpecification =
               includes.isEmpty() && defaultIncludeSpecification == null
               ? optional()
               : optional(DocumentEntrySpecification.orSpecification(list(streamConcat(stream(nullable(
                                                                                             defaultIncludeSpecification)),
                                                                                       stream(includes).map(
                                                                                             BiDirectionalFilter::includeDocumentPathSpecification)))));

         List<Path> excludes = list(streamConcat(stream(defaultFilter.excludes()),
                                                 downstream ? stream(downstreamFilter.excludes()) : stream(),
                                                 upstream ? stream(upstreamFilter.excludes()) : stream()));
         Optional<DocumentEntrySpecification> excludeSpecification =
               excludes.isEmpty() && defaultExcludeSpecification == null
               ? optional()
               : optional(DocumentEntrySpecification.andSpecification(list(streamConcat(stream(nullable(
                                                                                              defaultExcludeSpecification)),
                                                                                        stream(excludes).map(
                                                                                              BiDirectionalFilter::excludeDocumentPathSpecification)))));

         if (includeSpecification.isEmpty() && excludeSpecification.isEmpty()) {
            return optional();
         } else {
            return optional(DocumentEntrySpecification.andSpecification(list(streamConcat(stream(
                  includeSpecification), stream(excludeSpecification)))));
         }
      }

      private static DocumentEntryCriteria includeDocumentPathSpecification(Path path) {
         if (path.toString().startsWith("glob:") || path.toString().startsWith("regex:")) {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.match(path)).build();
         } else {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.equal(path)).build();
         }
      }

      private static DocumentEntryCriteria excludeDocumentPathSpecification(Path path) {
         if (path.toString().startsWith("glob:") || path.toString().startsWith("regex:")) {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.notMatch(path)).build();
         } else {
            return new DocumentEntryCriteriaBuilder().documentPath(CriterionBuilder.notEqual(path)).build();
         }
      }

      public boolean isEmpty() {
         return defaultFilter.isEmpty() && upstreamFilter.isEmpty() && downstreamFilter.isEmpty();
      }
   }
}