/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.formats;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StringUtils.removeEnd;
import static com.tinubu.projection.core.format.token.matcher.TokenMatchers.notWord;
import static com.tinubu.projection.core.format.token.matcher.TokenMatchers.word;

import java.util.List;

import com.tinubu.projection.core.format.AbstractFormat;
import com.tinubu.projection.core.format.token.DefaultTokenizer;

/**
 * Replaces any non-word ([A-Za-z0-9_]) sequence with a single space.
 * <p>
 * Leading/trailing spaces are stripped
 */
public class WordSpaceFormat extends AbstractFormat {

   private static final List<String> NAMES = list("word-space", "wordspace");

   @Override
   public List<String> names() {
      return NAMES;
   }

   @Override
   public int inferenceOrder() {
      return 13000;
   }

   @Override
   public String lowFormat(String content) {
      var tokens = DefaultTokenizer.of(word(), notWord()).tokens(content);

      var result = stream(tokens).reduce("", (s, token) -> {
         if (token.hasTokenMatcher(notWord())) {
            if (s.isEmpty()) {
               return s;
            } else {
               return s + " ";
            }
         } else {
            return s + token.subString(content);
         }
      }, String::concat);

      return removeEnd(result, " ");
   }

}
