/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token.codepointset;

import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static java.util.stream.IntStream.empty;
import static java.util.stream.IntStream.rangeClosed;

import java.util.Iterator;

/**
 * Alphabetic code point set.
 * <p>
 * Set = {@code [A-Za-z]}
 */
public class AlphaSet extends CodePointSet {

   private final boolean lowerCase;
   private final boolean upperCase;

   public AlphaSet(boolean lowerCase, boolean upperCase) {
      this.lowerCase = lowerCase;
      this.upperCase = upperCase;
   }

   public AlphaSet() {
      this(true, true);
   }

   @Override
   public boolean containsCodePoint(int i) {
      return (lowerCase && i >= 'a' && i <= 'z') || (upperCase && i >= 'A' && i <= 'Z');
   }

   @Override
   public Iterator<Integer> iterator() {
      return streamConcat(lowerCase ? rangeClosed('a', 'z') : empty(),
                          upperCase ? rangeClosed('A', 'Z') : empty()).iterator();
   }

   @Override
   public int size() {
      return (lowerCase ? 23 : 0) + (upperCase ? 23 : 0);
   }
}
