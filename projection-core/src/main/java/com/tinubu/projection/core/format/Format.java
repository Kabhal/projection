/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import java.util.List;
import java.util.Objects;

import com.tinubu.commons.lang.util.Pair;

/**
 * Format specification for attribute values.
 */
public interface Format extends Comparable<Format> {

   /**
    * Format names.
    * List must be non-empty, principal name is in first position, then optional secondary names.
    * Compact name is in second position, otherwise principal name will be used.
    */
   List<String> names();

   /**
    * Format principal name.
    * <p>
    * Ideally, principal names should use a similar naming logic for all formats.
    */
   default String name() {
      return Objects.requireNonNull(names()).get(0);
   }

   /**
    * Format priority order when inferred from attribute values. Lowest value has the highest precedence.
    * Two formats with the same order value are differentiated deterministically using principal name.
    */
   int inferenceOrder();

   /**
    * Format compact name.
    * Compact name is not necessarily the smallest name, it's a moderately compact but still meaningful
    * name, and ideally does not contain separator (-, _, ...).
    * <p>
    * Ideally, compact names should use a similar naming logic for all formats.
    */
   default String compactName() {
      var names = Objects.requireNonNull(names());

      return names.size() > 1 ? names.get(1) : names.get(0);
   }

   /** Returns a display description for this format. */
   default String description() {
      if (this instanceof AliasFormat) {
         return String.format("%s(%s)",
                              getClass().getSimpleName(),
                              ((AliasFormat) this).toSpecification().getValue());
      } else {
         return getClass().getSimpleName();
      }
   }

   /**
    * Format specified content.
    *
    * @param content content to format
    *
    * @return formatted content
    */
   String format(String content);

   @Override
   default int compareTo(Format o) {
      return Pair.of(inferenceOrder(), name()).compareTo(Pair.of(o.inferenceOrder(), o.name()));
   }
}
