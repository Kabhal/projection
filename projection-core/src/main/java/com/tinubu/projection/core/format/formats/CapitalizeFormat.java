/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.formats;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StringUtils.capitalize;
import static com.tinubu.projection.core.format.token.matcher.TokenMatchers.notWord;
import static com.tinubu.projection.core.format.token.matcher.TokenMatchers.word;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.tinubu.projection.core.format.AbstractFormat;
import com.tinubu.projection.core.format.token.DefaultTokenizer;

/**
 * Capitalizes first alphabetic character, left all other characters unchanged.
 * <p>
 * Optionally lowercases all other characters if {@code lowercase} is set.
 */
public class CapitalizeFormat extends AbstractFormat {

   private static final List<String> NAMES = list("capitalize", "cap");
   private static final boolean DEFAULT_LOWERCASE = false;

   /** Whether only the first character must be capitalized, or all other characters must be lowercased. */
   private final boolean lowercase;

   public CapitalizeFormat(boolean lowercase) {
      this.lowercase = lowercase;
   }

   public CapitalizeFormat() {
      this(DEFAULT_LOWERCASE);
   }

   @Override
   public List<String> names() {
      return NAMES;
   }

   /**
    * @implSpec capitalize < lower-camelcase
    * @return
    */
   @Override
   public int inferenceOrder() {
      return 2000;
   }

   @Override
   public String lowFormat(String content) {
      var tokens = DefaultTokenizer.of(word(), notWord()).tokens(content);

      AtomicBoolean capitalized = new AtomicBoolean(false);
      return stream(tokens).reduce("", (s, token) -> {
         var ts = token.subString(content);
         if (token.hasTokenMatcher(word())) {
            if (lowercase) {
               ts = ts.toLowerCase();
            }
            if (!capitalized.get()) {
               ts = capitalize(ts);
               capitalized.set(true);
            }
         }
         return s + ts;
      }, String::concat);
   }
}
