/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.config;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_YAML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.CollectionUtils.set;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.nodes.NodeTuple;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.projection.core.Attribute;
import com.tinubu.projection.core.PlaceholderContext;
import com.tinubu.projection.core.PlaceholderFormat;

public class DocumentProjectionConfiguration extends ProjectionConfiguration {
   private static final Yaml YAML_PARSER = yamlParser();
   private static final Yaml YAML_DUMPER = yamlDumper();

   private DocumentProjectionConfiguration(Builder builder) {
      super(builder);
   }

   public static DocumentProjectionConfiguration of(ProjectionConfiguration projectionConfiguration) {
      return new DocumentProjectionConfiguration(Builder.from(projectionConfiguration));
   }

   public static DocumentProjectionConfiguration ofDocument(Document projectionConfiguration) {
      try (InputStream content = projectionConfiguration.content().inputStreamContent()) {
         ProjectionConfigurationModel projectionConfigurationModel;

         try {
            projectionConfigurationModel = projectionConfigurationModel(content).getProjection();
         } catch (Exception e) {
            throw new IllegalStateException(String.format("Can't parse '%s' projection configuration > %s",
                                                          projectionConfiguration.documentId().stringValue(),
                                                          e.getMessage()), e);
         }

         var attributes = list(projectionConfigurationModel
                                     .getAttributes()
                                     .entrySet()
                                     .stream()
                                     .map(e -> Attribute.of(e.getKey(), e.getValue())));

         return new DocumentProjectionConfiguration(new ProjectionConfiguration.Builder()
                                                          .templateUri(nullable(projectionConfigurationModel.getTemplateUri())
                                                                             .map(URI::create)
                                                                             .orElse(null))
                                                          .attributes(attributes)
                                                          .extraFormats(projectionConfigurationModel.getExtraFormats())
                                                          .restrictedFormats(set(projectionConfigurationModel.getRestrictedFormats()))
                                                          .placeholderContext(projectionConfigurationModel
                                                                                    .getPlaceholderContext()
                                                                                    .placeholderContext())
                                                          .synchronizeFilter(projectionConfigurationModel
                                                                                   .getSynchronize()
                                                                                   .biDirectionalFilter())
                                                          .replaceContentFilter(projectionConfigurationModel
                                                                                      .getReplaceContent()
                                                                                      .biDirectionalFilter())
                                                          .properties(projectionConfigurationModel.getProperties()));
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private static RootProjectionConfigurationModel projectionConfigurationModel(InputStream content) {
      return nullable(YAML_PARSER.loadAs(content, RootProjectionConfigurationModel.class),
                      new RootProjectionConfigurationModel());
   }

   public Document toDocument(DocumentPath documentId) {
      RootProjectionConfigurationModel rootProjectionConfiguration =
            new RootProjectionConfigurationModel(this);

      return new DocumentBuilder()
            .documentId(documentId)
            .contentType(APPLICATION_YAML)
            .loadedContent(YAML_DUMPER.dump(rootProjectionConfiguration), UTF_8)
            .build();
   }

   private static Yaml yamlParser() {
      return new Yaml();
   }

   private static Yaml yamlDumper() {
      var options = new DumperOptions();

      options.setCanonical(false);
      options.setAllowUnicode(true);
      options.setPrettyFlow(false);
      options.setDefaultFlowStyle(FlowStyle.BLOCK);
      options.setIndent(3);

      var representer = new HideEmptyRepresenter(options);
      representer.setDefaultFlowStyle(FlowStyle.BLOCK);
      representer.addClassTag(RootProjectionConfigurationModel.class, Tag.MAP);

      return new Yaml(representer, options);
   }

   private static class HideEmptyRepresenter extends Representer {
      public HideEmptyRepresenter(DumperOptions options) {
         super(options);
      }

      @Override
      protected NodeTuple representJavaBeanProperty(Object javaBean,
                                                    Property property,
                                                    Object propertyValue,
                                                    Tag customTag) {
         if (propertyValue instanceof List && ((List<?>) propertyValue).isEmpty()) {
            return null;
         } else if (propertyValue instanceof Map && ((Map<?, ?>) propertyValue).isEmpty()) {
            return null;
         } else if (propertyValue instanceof Set && ((Set<?>) propertyValue).isEmpty()) {
            return null;
         } else if (propertyValue instanceof RootFilterConfigurationModel
                    && ((RootFilterConfigurationModel) propertyValue).isEmpty()) {
            return null;
         } else if (propertyValue instanceof FilterConfigurationModel
                    && ((FilterConfigurationModel) propertyValue).isEmpty()) {
            return null;
         } else {
            return super.representJavaBeanProperty(javaBean, property, propertyValue, customTag);
         }
      }
   }

   public static class RootProjectionConfigurationModel {
      private ProjectionConfigurationModel projection;

      public ProjectionConfigurationModel getProjection() {
         return projection;
      }

      public RootProjectionConfigurationModel() {
         this.projection = new ProjectionConfigurationModel();
      }

      public RootProjectionConfigurationModel(ProjectionConfiguration projectionConfiguration) {
         this.projection = new ProjectionConfigurationModel(projectionConfiguration);
      }

      public void setProjection(ProjectionConfigurationModel projection) {
         this.projection = nullable(projection, new ProjectionConfigurationModel());
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this).append("projection", projection).toString();
      }
   }

   public static class ProjectionConfigurationModel {
      private String templateUri;
      private Map<String, String> attributes = map(LinkedHashMap::new);
      private Map<String, String> extraFormats = map(LinkedHashMap::new);
      private List<String> restrictedFormats = list();
      private PlaceholderContextModel placeholderContext = new PlaceholderContextModel();
      private RootFilterConfigurationModel synchronize = new RootFilterConfigurationModel();
      private RootFilterConfigurationModel replaceContent = new RootFilterConfigurationModel();
      private Map<String, String> properties = map(LinkedHashMap::new);

      public ProjectionConfigurationModel() {
      }

      public ProjectionConfigurationModel(ProjectionConfiguration projectionConfiguration) {
         this.templateUri = projectionConfiguration.templateUri().map(URI::toString).orElse(null);
         this.attributes = map(LinkedHashMap::new,
                               projectionConfiguration
                                     .attributes()
                                     .stream()
                                     .map(a -> Pair.of(a.decoratedName(), a.value())));
         this.extraFormats = projectionConfiguration.extraFormats();
         this.restrictedFormats = list(projectionConfiguration.restrictedFormats());
         this.placeholderContext = new PlaceholderContextModel(projectionConfiguration.placeholderContext());
         this.synchronize = new RootFilterConfigurationModel(projectionConfiguration.synchronizeFilter());
         this.replaceContent =
               new RootFilterConfigurationModel(projectionConfiguration.replaceContentFilter());
         this.properties = projectionConfiguration.properties();
      }

      public String getTemplateUri() {
         return templateUri;
      }

      public void setTemplateUri(String templateUri) {
         this.templateUri = templateUri;
      }

      public Map<String, String> getAttributes() {
         return attributes;
      }

      public void setAttributes(Map<String, String> attributes) {
         this.attributes = map(LinkedHashMap::new, attributes);
      }

      public List<Attribute> attributes() {
         return list(attributes.entrySet().stream().map(e -> Attribute.of(e.getKey(), e.getValue())));
      }

      public Map<String, String> getExtraFormats() {
         return extraFormats;
      }

      public void setExtraFormats(Map<String, String> extraFormats) {
         this.extraFormats = map(LinkedHashMap::new, extraFormats);
      }

      public List<String> getRestrictedFormats() {
         return restrictedFormats;
      }

      public void setRestrictedFormats(List<String> restrictedFormats) {
         this.restrictedFormats = list(restrictedFormats);
      }

      public PlaceholderContextModel getPlaceholderContext() {
         return placeholderContext;
      }

      public void setPlaceholderContext(PlaceholderContextModel placeholderContext) {
         this.placeholderContext = nullable(placeholderContext, new PlaceholderContextModel());
      }

      public RootFilterConfigurationModel getSynchronize() {
         return synchronize;
      }

      public void setSynchronize(RootFilterConfigurationModel synchronize) {
         this.synchronize = nullable(synchronize, new RootFilterConfigurationModel());
      }

      public RootFilterConfigurationModel getReplaceContent() {
         return replaceContent;
      }

      public void setReplaceContent(RootFilterConfigurationModel replaceContent) {
         this.replaceContent = nullable(replaceContent, new RootFilterConfigurationModel());
      }

      public Map<String, String> getProperties() {
         return properties;
      }

      public void setProperties(Map<String, String> properties) {
         this.properties = map(LinkedHashMap::new, properties);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", ProjectionConfigurationModel.class.getSimpleName() + "[", "]")
               .add("templateUri=" + templateUri)
               .add("attributes=" + attributes)
               .add("extraFormats=" + extraFormats)
               .add("placeholderContext='" + placeholderContext + "'")
               .add("synchronize='" + synchronize + "'")
               .add("replaceContent='" + replaceContent + "'")
               .add("properties='" + properties + "'")
               .toString();
      }
   }

   public static class FilterConfigurationModel {
      private List<String> includes = list();
      private List<String> excludes = list();

      public FilterConfigurationModel() {
      }

      public FilterConfigurationModel(Filter filter) {
         this.includes = list(filter.includes().stream().map(Path::toString));
         this.excludes = list(filter.excludes().stream().map(Path::toString));

      }

      public List<String> getIncludes() {
         return includes;
      }

      public void setIncludes(List<String> includes) {
         this.includes = list(includes);
      }

      public List<String> getExcludes() {
         return excludes;
      }

      public void setExcludes(List<String> excludes) {
         this.excludes = list(excludes);
      }

      public Filter filter() {
         return Filter.of(list(stream(includes).map(Path::of)), list(stream(excludes).map(Path::of)));
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", FilterConfigurationModel.class.getSimpleName() + "[", "]")
               .add("includes=" + includes)
               .add("excludes=" + excludes)
               .toString();
      }

      public boolean isEmpty() {
         return includes.isEmpty() && excludes.isEmpty();
      }
   }

   public static class RootFilterConfigurationModel {
      private List<String> includes = list();
      private List<String> excludes = list();
      private FilterConfigurationModel downstream = new FilterConfigurationModel();
      private FilterConfigurationModel upstream = new FilterConfigurationModel();

      public RootFilterConfigurationModel() {
      }

      public RootFilterConfigurationModel(BiDirectionalFilter biDirectionalFilter) {
         this.includes = list(biDirectionalFilter.defaultFilter().includes().stream().map(Path::toString));
         this.excludes = list(biDirectionalFilter.defaultFilter().excludes().stream().map(Path::toString));
         this.downstream = new FilterConfigurationModel(biDirectionalFilter.downstreamFilter());
         this.upstream = new FilterConfigurationModel(biDirectionalFilter.upstreamFilter());

      }

      public List<String> getIncludes() {
         return includes;
      }

      public void setIncludes(List<String> includes) {
         this.includes = list(includes);
      }

      public List<String> getExcludes() {
         return excludes;
      }

      public void setExcludes(List<String> excludes) {
         this.excludes = list(excludes);
      }

      public FilterConfigurationModel getDownstream() {
         return downstream;
      }

      public void setDownstream(FilterConfigurationModel downstream) {
         this.downstream = nullable(downstream, new FilterConfigurationModel());
      }

      public FilterConfigurationModel getUpstream() {
         return upstream;
      }

      public void setUpstream(FilterConfigurationModel upstream) {
         this.upstream = nullable(upstream, new FilterConfigurationModel());
      }

      public BiDirectionalFilter biDirectionalFilter() {
         return new BiDirectionalFilter(Filter.of(list(stream(includes).map(Path::of)),
                                                  list(stream(excludes).map(Path::of))),
                                        downstream.filter(),
                                        upstream.filter());
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", RootFilterConfigurationModel.class.getSimpleName() + "[", "]")
               .add("includes=" + includes)
               .add("excludes=" + excludes)
               .add("downstream=" + downstream)
               .add("upstream=" + upstream)
               .toString();
      }

      public boolean isEmpty() {
         return includes.isEmpty() && excludes.isEmpty() && downstream.isEmpty() && upstream.isEmpty();
      }
   }

   public static class PlaceholderContextModel {
      private String contentBegin;
      private String contentEnd;
      private String contentFormatSeparator;
      private String pathBegin;
      private String pathEnd;
      private String pathFormatSeparator;
      private Boolean inferPlaceholders;

      public PlaceholderContextModel() {
      }

      public PlaceholderContextModel(PlaceholderContext placeholderContext) {
         this.contentBegin = placeholderContext.format().contentBegin();
         this.contentEnd = placeholderContext.format().contentEnd();
         this.contentFormatSeparator = placeholderContext.format().contentFormatSeparator();
         this.pathBegin = placeholderContext.format().pathBegin();
         this.pathEnd = placeholderContext.format().pathEnd();
         this.pathFormatSeparator = placeholderContext.format().pathFormatSeparator();
         this.inferPlaceholders = placeholderContext.inferPlaceholders();
      }

      public String getContentBegin() {
         return contentBegin;
      }

      public void setContentBegin(String contentBegin) {
         this.contentBegin = contentBegin;
      }

      public String getContentEnd() {
         return contentEnd;
      }

      public void setContentEnd(String contentEnd) {
         this.contentEnd = contentEnd;
      }

      public String getContentFormatSeparator() {
         return contentFormatSeparator;
      }

      public void setContentFormatSeparator(String contentFormatSeparator) {
         this.contentFormatSeparator = contentFormatSeparator;
      }

      public String getPathBegin() {
         return pathBegin;
      }

      public void setPathBegin(String pathBegin) {
         this.pathBegin = pathBegin;
      }

      public String getPathEnd() {
         return pathEnd;
      }

      public void setPathEnd(String pathEnd) {
         this.pathEnd = pathEnd;
      }

      public String getPathFormatSeparator() {
         return pathFormatSeparator;
      }

      public void setPathFormatSeparator(String pathFormatSeparator) {
         this.pathFormatSeparator = pathFormatSeparator;
      }

      public Boolean getInferPlaceholders() {
         return inferPlaceholders;
      }

      public void setInferPlaceholders(Boolean inferPlaceholders) {
         this.inferPlaceholders = inferPlaceholders;
      }

      public PlaceholderContext placeholderContext() {
         return PlaceholderContext
               .builder()
               .format(PlaceholderFormat
                             .builder()
                             .contentBegin(contentBegin)
                             .contentEnd(contentEnd)
                             .contentFormatSeparator(contentFormatSeparator)
                             .pathBegin(pathBegin)
                             .pathEnd(pathEnd)
                             .pathFormatSeparator(pathFormatSeparator)
                             .build())
               .inferPlaceholders(inferPlaceholders)
               .build();
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", PlaceholderContextModel.class.getSimpleName() + "[", "]")
               .add("contentPlaceholderBegin='" + contentBegin + "'")
               .add("contentPlaceholderEnd='" + contentEnd + "'")
               .add("contentPlaceholderFormatSeparator='" + contentFormatSeparator + "'")
               .add("pathPlaceholderBegin='" + pathBegin + "'")
               .add("pathPlaceholderEnd='" + pathEnd + "'")
               .add("pathPlaceholderFormatSeparator='" + pathFormatSeparator + "'")
               .add("inferPlaceholders='" + inferPlaceholders + "'")
               .toString();
      }
   }
}
