/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.pValue;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isInRangeInclusive;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.lang.io.replacer.DirectiveReplacer.ErrorMode.LOG_WARN;
import static com.tinubu.commons.lang.io.replacer.DirectiveReplacer.ErrorMode.THROW;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.CollectionUtils.set;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;
import static com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.Validate.Check;
import com.tinubu.commons.lang.io.encoding.EncodingDetector.DetectedEncoding;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.TokenReplacer;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.TokenReplacerFactory;
import com.tinubu.commons.lang.io.replacer.MultiReplacer;
import com.tinubu.commons.lang.io.replacer.StringReplacer;
import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.projection.core.ProjectionProperties.Mappers;
import com.tinubu.projection.core.ProjectionProperties.View;
import com.tinubu.projection.core.config.ProjectionConfiguration;
import com.tinubu.projection.core.format.AliasFormat;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.NoopFormat;

/**
 * Document synchronization processor that generates a filtered, and renamed, project document.
 * <p>
 * The following properties are used :
 * <ul>
 *    <li>{@code projection.processor.replacer-buffer-size} [{@value DEFAULT_REPLACER_BUFFER_SIZE}] : replacement buffer size in bytes, replaced values can't outpass buffer size. Must be > 0</li>
 *    <li>{@code projection.processor.encoding-detection-threshold} [{@value DEFAULT_ENCODING_DETECTION_THRESHOLD}] : minimum encoding detection confidence for missing encoding auto-detection. Must be in [0, 1]</li>
 * </ul>
 */
public class DocumentSynchronizeProcessor implements DocumentProcessor {
   /**
    * Default working buffer size (in bytes) for replacers. Attributes size must not overpass this
    * value.
    * We assume here that all formats will always generate formatted value with a size lesser than or equal
    * to 2 times the source value.
    */
   private static final int DEFAULT_REPLACER_BUFFER_SIZE = 2 * Attribute.ATTRIBUTE_MAX_SIZE;
   /** Name prefix for dynamically generated alias formats from compound format specifiers in templates. */
   private static final String DYNAMIC_COMPOUND_FORMAT_NAME_PREFIX = "_dynamic_compound";
   /** Default auto-detection confidence minimum threshold. */
   private static final float DEFAULT_ENCODING_DETECTION_THRESHOLD = 0.3f;

   private static final ExtendedLogger log = ProjectionLogger.of(DocumentSynchronizeProcessor.class);

   private final ProjectionConfiguration templateConfiguration;
   private final ProjectionConfiguration projectConfiguration;
   /** Formats to use for inference, without restricted formats. Includes extra formats. */
   private final List<Format> inferenceFormats;
   /**
    * All formats indexed by {@link Format#names()}. Includes extra formats, does not exclude restricted
    * formats.
    */
   private final Map<String, Format> allFormats;
   private final boolean noPlaceholders;
   private final Predicate<? super DocumentEntry> shortCircuitProjectDocument;
   private final View processorProperties;
   private final int replacerBufferSize;

   private static final Map<String, Object> placeholderReplacerCache = new ConcurrentHashMap<>();

   private static final Set<String> missingPlaceholderAttributes = set();

   /**
    * @param templateConfiguration
    * @param projectConfiguration
    * @param shortCircuitProjectDocument callback to stop processing of
    *       project document, if predicate is satisfied, once project document path is processed
    * @param formats
    * @param noPlaceholders
    */
   public DocumentSynchronizeProcessor(ProjectionConfiguration templateConfiguration,
                                       ProjectionConfiguration projectConfiguration,
                                       Predicate<? super DocumentEntry> shortCircuitProjectDocument,
                                       List<Format> formats,
                                       boolean noPlaceholders) {
      this.templateConfiguration = Check.notNull(templateConfiguration, "templateConfiguration");
      this.projectConfiguration = Check.notNull(projectConfiguration, "projectConfiguration");
      this.shortCircuitProjectDocument =
            Check.notNull(shortCircuitProjectDocument, "shortCircuitProjectDocument");
      this.noPlaceholders = noPlaceholders;

      processorProperties =
            new ProjectionProperties(projectConfiguration.properties()).view("projection.processor");

      replacerBufferSize = processorProperties
            .value("replacer-buffer-size", Mappers::intValue, isStrictlyPositive())
            .orElse(DEFAULT_REPLACER_BUFFER_SIZE);

      this.allFormats = map(LinkedHashMap::new,
                            stream(formats).flatMap(f -> stream(f.names()).map(name -> entry(name, f))));

      this.inferenceFormats = inferenceFormats(projectConfiguration, formats);
   }

   /**
    * Returns all format that can be used for inference.
    * Returned list is distinct and sorted.
    *
    * <p>
    * {@link NoopFormat} is always added in last position in returned format list, even if already present in
    * any position of the list.
    *
    * @implNote If all restricted formats are ignored, the formats used for inference must be ø, not
    *       "all formats".
    */
   private List<Format> inferenceFormats(ProjectionConfiguration projectConfiguration, List<Format> formats) {
      List<Format> inferenceFormats;
      var restrictedFormats = restrictedFormats(projectConfiguration);

      if (!projectConfiguration.restrictedFormats().isEmpty()) {
         log.info("Restrict formats to : {}",
                  () -> restrictedFormats.stream().map(Format::name).collect(joining(", ")));
         inferenceFormats = list(stream(formats).filter(restrictedFormats::contains));
      } else {
         inferenceFormats = list(formats);
      }

      extraFormats(projectConfiguration).forEach(extraFormat -> {
         inferenceFormats.add(extraFormat);
         extraFormat.names().forEach(name -> {
            this.allFormats.put(name, extraFormat);
         });
      });

      inferenceFormats.add(NoopFormat.instance());

      var orderedInferenceFormats = list(stream(inferenceFormats).distinct().sorted());

      if (log.isInfoEnabled()) {
         orderedInferenceFormats.forEach(format -> {
            log.info("Use '{}' format [{}], inference order: {}, names: {}",
                     format::name,
                     format::description,
                     format::inferenceOrder,
                     () -> stream(format.names()).collect(joining(", ")));
         });
      }

      return orderedInferenceFormats;
   }

   public static void clearPlaceholderCache() {
      DocumentSynchronizeProcessor.placeholderReplacerCache.clear();
   }

   /**
    * Returns formats from project configuration restriction list, or empty list if configuration restriction
    * list is empty. Configuration restriction list can contain principal or aliased names.
    * Duplicated formats are removed. Missing formats are ignored.
    */
   private Set<Format> restrictedFormats(ProjectionConfiguration projectConfiguration) {
      return set(stream(projectConfiguration.restrictedFormats()).map(restrictedFormatName -> {
         var restrictedFormat = allFormats.get(restrictedFormatName);
         if (restrictedFormat != null) {
            return restrictedFormat;
         } else {
            log.warn("Missing '{}' restricted format", () -> restrictedFormatName);
            return null;
         }
      }));
   }

   /**
    * Returns extra formats defined in project configuration.
    *
    * @throws IllegalStateException if one defined extra format names is already registered
    * @implSpec Extra formats must also be checked for duplication between themselves.
    */
   private List<AliasFormat> extraFormats(ProjectionConfiguration projectConfiguration) {
      List<AliasFormat> extraFormats = list();
      Set<String> extraFormatNames = set();
      Set<String> duplicatedExtraFormats = set();

      projectConfiguration.extraFormats().forEach((nameSpec, delegateSpec) -> {
         try {
            var extraFormat = AliasFormat.ofSpecification(allFormats, nameSpec, delegateSpec);

            extraFormat.names().forEach(extraName -> {
               if (allFormats.containsKey(extraName) || extraFormatNames.contains(extraName)) {
                  duplicatedExtraFormats.add(extraName);
               }
            });

            extraFormats.add(extraFormat);
            extraFormatNames.addAll(extraFormat.names());
         } catch (IllegalStateException e) {
            log.warn("Can't register '{}' extra format : {}", () -> nameSpec, e::getMessage);
         }
      });

      if (!duplicatedExtraFormats.isEmpty()) {
         throw new IllegalStateException(String.format("Duplicated format names in project configuration : %s",
                                                       stream(duplicatedExtraFormats)
                                                             .sorted()
                                                             .collect(joining(","))));

      }

      return extraFormats;
   }

   /**
    * @implNote Replacement order is 1- placeholders 2- attribute inference, so that directives content
    *       itself is not replaced before evaluation. But consequence is include-directive injected text is
    *       attribute-inferred 2 times.
    */
   @Override
   public Document process(Document templateDocument) {
      try {
         DocumentEntry projectDocumentEntry = DocumentEntryBuilder
               .from(templateDocument.documentEntry())
               .documentId(filterDocumentId(templateDocument.documentId()))
               .build();

         DocumentBuilder projectDocumentBuilder = new DocumentBuilder()
               .documentId(projectDocumentEntry.documentId())
               .contentType(projectDocumentEntry.metadata().contentType().orElse(null))
               .attributes(projectDocumentEntry.metadata().attributes())
               .content(templateDocument.content());

         if (shortCircuitProjectDocument.test(projectDocumentEntry)) {
            return projectDocumentBuilder.build();
         }

         var templateContentEncoding = canReplaceContent(templateDocument, projectDocumentEntry);

         if (templateContentEncoding.isPresent()) {
            log.debug("Filter '{}' template document (encoding={})",
                      () -> templateDocument.documentId().stringValue(),
                      templateContentEncoding::get);

            Reader replacedContent = templateDocument.content().readerContent(templateContentEncoding.get());
            if (!noPlaceholders && !projectConfiguration.placeholderContext().inferPlaceholders()) {
               replacedContent = replaceContentPlaceholders(templateDocument.documentId(),
                                                            replacedContent,
                                                            templateConfiguration
                                                                  .placeholderFormat()
                                                                  .contentBegin(),
                                                            templateConfiguration
                                                                  .placeholderFormat()
                                                                  .contentEnd(),
                                                            templateConfiguration
                                                                  .placeholderFormat()
                                                                  .contentFormatSeparator());
            }
            replacedContent = replaceContentInferredAttributes(replacedContent, false);

            projectDocumentBuilder =
                  projectDocumentBuilder.streamContent(replacedContent, templateContentEncoding.get());
         }

         return projectDocumentBuilder.build();
      } catch (Exception e) {
         throw new IllegalStateException(String.format("Error while processing '%s' : %s",
                                                       templateDocument.documentId().stringValue(),
                                                       e.getMessage()), e);
      }
   }

   /**
    * Checks if processed document is a candidate for content replacement.
    * <p>
    * Processed document must :
    * <ul>
    *    <li>have a content-type that matches template configuration upstream content filter, if any configuration</li>
    *    <li>have a content-type that matches project configuration downstream content filter, if any configuration</li>
    *    <li>have a content-encoding (auto-detected if not set)</li>
    * </ul>
    * Template content encoding is auto-detected if not available.
    *
    * @param templateDocument template document being processed
    * @param projectDocument project document being processed
    *
    * @return template document content encoding if it is a candidate for content replacement, otherwise,
    *       returns {@link Optional#empty()}
    */
   private Optional<Charset> canReplaceContent(Document templateDocument, DocumentEntry projectDocument) {

      boolean canReplaceContentForTemplate = templateConfiguration
            .replaceContentFilter()
            .upstreamDocumentEntrySpecification(null, null)
            .map(criteria -> criteria.satisfiedBy(templateDocument.documentEntry()))
            .orElse(true);

      if (!canReplaceContentForTemplate) {
         return optional();
      }

      boolean canReplaceContentForProject = projectConfiguration
            .replaceContentFilter()
            .downstreamDocumentEntrySpecification(null, null)
            .map(criteria -> criteria.satisfiedBy(projectDocument))
            .orElse(true);

      if (!canReplaceContentForProject) {
         return optional();
      }

      var encodingDetectionThreshold = processorProperties
            .value("encoding-detection-threshold",
                   Mappers::floatValue,
                   isInRangeInclusive(pValue(0f), pValue(1f)))
            .orElse(DEFAULT_ENCODING_DETECTION_THRESHOLD);

      var templateContentEncoding = templateDocument
            .metadata()
            .contentEncoding()
            .or(checkedSupplier(() -> templateDocument
                  .content()
                  .hasEncoding(list())
                  .filter(this::validateCharset)
                  .filter(detectedEncoding -> {
                     if (detectedEncoding.confidence() >= encodingDetectionThreshold) {
                        log.debug("Auto-detected '{}' encoding : {}",
                                  () -> templateDocument.documentId().stringValue(),
                                  () -> detectedEncoding);
                        return true;
                     } else {
                        log.debug("Auto-detected '{}' encoding : {} - Rejected (threshold={})",
                                  () -> templateDocument.documentId().stringValue(),
                                  () -> detectedEncoding,
                                  () -> encodingDetectionThreshold);
                        return false;
                     }
                  })
                  .map(DetectedEncoding::encoding)));

      if (templateContentEncoding.isEmpty()) {
         return optional();
      }

      return templateContentEncoding;
   }

   /**
    * Some charset fails encoder instantiation (e.g. ISO2022_CN).
    */
   private boolean validateCharset(DetectedEncoding detectedEncoding) {
      try {
         detectedEncoding.encoding().newEncoder();
         return true;
      } catch (Exception e) {
         return false;
      }
   }

   /**
    * Cached {@link #replaceContentPlaceholders(DocumentPath, Reader, String, String, String)}.
    *
    * @return placeholder replacement, or {@code null} if no matching attribute found
    */
   private Object cachePlaceholderReplacerMapper(DocumentPath documentId,
                                                 String placeholder,
                                                 String formatSeparator,
                                                 Pattern formatSeparatorPattern) {
      return placeholderReplacerCache.computeIfAbsent(placeholder,
                                                      ph -> placeholderReplacerMapper(documentId,
                                                                                      ph,
                                                                                      formatSeparator,
                                                                                      formatSeparatorPattern));
   }

   /**
    * Generic placeholder replacement function. Replace placeholder, optionally containing a format
    * specification, by matching attribute value. Format specification must be registered, or it will be
    * considered as part of the attribute name.
    *
    * @return placeholder replacement, or {@code null} if no matching attribute found
    */
   private Object placeholderReplacerMapper(DocumentPath documentId,
                                            String placeholder,
                                            String formatSeparator,
                                            Pattern formatSeparatorPattern) {
      String[] placeholderParts = formatSeparatorPattern.split(placeholder, -1);

      if (placeholderParts.length > 0) {
         List<Format> formats = list();

         if (placeholderParts.length > 1) {
            for (int i = placeholderParts.length - 1; i > 0; i--) {
               var formatFromSpecifier = format(placeholderParts[i]);
               if (formatFromSpecifier.isPresent()) {
                  formats.add(0, formatFromSpecifier.get());
               } else {
                  break;
               }
            }
         }

         var attribute = StringUtils.join(placeholderParts,
                                          formatSeparator,
                                          0,
                                          placeholderParts.length - formats.size());
         var format = format(formats);

         return projectConfiguration.attribute(attribute).map(a -> format.format(a.value())).orElseGet(() -> {
            if (!missingPlaceholderAttributes.contains(attribute)) {
               log.warn("{} : Missing '{}' project attribute referenced from placeholder",
                        documentId::stringValue,
                        () -> attribute);
               missingPlaceholderAttributes.add(attribute);
            }
            return null;
         });
      } else {
         return null;
      }
   }

   /** Returns definitive format from identified formats. */
   private static Format format(List<Format> formats) {
      if (formats.isEmpty()) {
         return NoopFormat.instance();
      } else if (formats.size() == 1) {
         return formats.get(0);
      } else {
         return new AliasFormat(list(DYNAMIC_COMPOUND_FORMAT_NAME_PREFIX), formats);
      }
   }

   /**
    * Returns {@link Format} for one format specifier, or {@link Optional#empty} if format is
    * unknown. If format specifier is empty, returns {@link Optional#empty}. Compound formats are not managed
    * here.
    *
    * @param formatSpecifier format part of placeholder
    *
    * @return format, or {@link Optional#empty} if one or several formats are unknown
    */
   private Optional<Format> format(String formatSpecifier) {
      if (formatSpecifier.isEmpty()) {
         return optional();
      } else {
         return nullable(allFormats.get(formatSpecifier));
      }
   }

   /**
    * @param contentTypes content types to filter
    *
    * @implSpec {@code DocumentEntryCriteria::contentType(CriteriaBuilder.in(...)} does not support
    *       MimeType::matches operation.
    */
   private DocumentEntrySpecification filterContentTypeSpecification(List<MimeType> contentTypes) {
      return documentEntry -> {
         MimeType contentType =
               documentEntry.metadata().contentType().map(MimeType::strippedParameters).orElse(null);

         if (contentType == null) {
            return false;
         } else {
            return contentTypes.stream().anyMatch(contentType::matches);
         }
      };
   }

   /**
    * Filter document identifier with both attribute inference and placeholders.
    *
    * @param documentId document identifier to filter
    *
    * @return filtered document identifier
    */
   private DocumentPath filterDocumentId(DocumentPath documentId) {
      String documentPath = documentId.stringValue();

      Reader filteredDocumentPath = new StringReader(documentPath);
      filteredDocumentPath = replaceContentInferredAttributes(filteredDocumentPath, true);
      if (!projectConfiguration.placeholderContext().inferPlaceholders()) {
         filteredDocumentPath = replaceContentPlaceholders(documentId,
                                                           filteredDocumentPath,
                                                           templateConfiguration
                                                                 .placeholderFormat()
                                                                 .pathBegin(),
                                                           templateConfiguration
                                                                 .placeholderFormat()
                                                                 .pathEnd(),
                                                           templateConfiguration
                                                                 .placeholderFormat()
                                                                 .pathFormatSeparator());
      }

      return DocumentPath.of(readerToString(filteredDocumentPath));
   }

   private Reader replaceContentPlaceholders(DocumentPath documentId,
                                             Reader content,
                                             String placeholderBegin,
                                             String placeholderEnd,
                                             String formatSeparator) {
      return new GeneralReplacerReader(content,
                                       tokenReplacer(documentId,
                                                     placeholderBegin,
                                                     placeholderEnd,
                                                     formatSeparator),
                                       replacerBufferSize);
   }

   private TokenReplacerFactory tokenReplacer(DocumentPath documentId,
                                              String placeholderBegin,
                                              String placeholderEnd,
                                              String formatSeparator) {
      var formatSeparatorPattern = Pattern.compile(Pattern.quote(formatSeparator));

      return bs -> new DirectiveReplacer(bs,
                                         placeholderBegin,
                                         placeholderEnd,
                                         (ctx, placeholder) -> cachePlaceholderReplacerMapper(documentId,
                                                                                              placeholder,
                                                                                              formatSeparator,
                                                                                              formatSeparatorPattern))
            .errorMode(ProjectionLoggerCallback.failOnWarning() ? THROW : LOG_WARN)
            .loadDirectivePlugins(alwaysTrue());
   }

   private String readerToString(Reader reader) {
      try {
         return IOUtils.toString(reader);
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }
   }

   /**
    * Filter content with inferred attributes using all registered formats, and also a case-insensitive
    * format of original attribute value.
    *
    * @param content content to filter
    * @param toPath whether to format attributes for paths
    *
    * @return filtered content
    */
   private Reader replaceContentInferredAttributes(Reader content, boolean toPath) {
      List<TokenReplacer> replacers = list();
      for (Attribute templateAttribute : templateConfiguration.attributes()) {
         stream(inferenceFormats)
               .map(format -> inferredAttributeStringReplacer(templateAttribute, format, toPath))
               .forEach(replacers::add);
      }
      return new GeneralReplacerReader(content, __ -> new MultiReplacer(replacers), replacerBufferSize);
   }

   private StringReplacer inferredAttributeStringReplacer(Attribute templateAttribute,
                                                          Format format,
                                                          boolean toPath) {
      String templateAttributeValue = format.format(templateAttribute.value());
      String projectAttributeValue = filterContentInferredAttributeValue(templateAttribute, format, toPath);

      return new StringReplacer(templateAttributeValue, projectAttributeValue, false);
   }

   /**
    * Returns formatted value of project attribute associated to specified template attribute.
    * If template attribute is not present in project but is not required, then returns formatted template
    * attribute value.
    *
    * @param templateAttribute template attribute
    * @param format format
    * @param toPath whether value must be returned in a path context
    *
    * @throws IllegalStateException if template attribute is required and not present in project
    *       configuration
    */
   private String filterContentInferredAttributeValue(Attribute templateAttribute,
                                                      Format format,
                                                      boolean toPath) {
      if (projectConfiguration.placeholderContext().inferPlaceholders()) {
         return attributeToPlaceholder(templateAttribute.name(), format, toPath);
      } else {
         Attribute projectAttribute = projectConfiguration
               .attribute(templateAttribute.name())
               .or(() -> {
                  if (templateAttribute.required()) {
                     return optional();
                  } else {
                     return optional(templateAttribute);
                  }
               })
               .orElseThrow(() -> new IllegalStateException(String.format("Missing '%s' project attribute",
                                                                          templateAttribute.name())));

         return format.format(projectAttribute.value());
      }
   }

   /**
    * Generates the placeholder representation for the given attribute and format.
    * Use {@link Format#compactName()} for format name inference.
    *
    * @param attributeName attribute name to transform
    * @param format format to identify in transformed placeholder, not that the {@link NoopFormat} is
    *       not represented (no format specifier)
    * @param toPath whether to use the path-like representation
    *
    * @return placeholder representation
    */
   private String attributeToPlaceholder(String attributeName, Format format, boolean toPath) {
      var placeholderFormat = projectConfiguration.placeholderFormat();

      if (toPath) {
         return placeholderFormat.pathPlaceholder(attributeName, format);
      } else {
         return placeholderFormat.contentPlaceholder(attributeName, format);
      }
   }

}
