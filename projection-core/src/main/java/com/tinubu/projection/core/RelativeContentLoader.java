/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.ddd2.uri.Uri.compatibleUri;

import java.net.URI;
import java.util.Optional;

import com.tinubu.commons.ddd2.uri.uris.FileUri;
import com.tinubu.commons.ddd2.uri.uris.FileUri.FileUriRestrictions;
import com.tinubu.commons.lang.io.contentloader.ContentLoader;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.projection.core.DocumentRepositorySourceContentLoader.DocumentRepositoryContentLoader.DocumentContent;

/**
 * Specific {@link ContentLoader} to load "relative" URIs.
 */
public class RelativeContentLoader implements ContentLoader {

   private final DocumentRepository repository;
   private final DocumentProcessor processor;

   /**
    * @param repository source repository for relative URI
    * @param processor optional processor to apply to relative document, or {@code null}
    */
   public RelativeContentLoader(DocumentRepository repository, DocumentProcessor processor) {
      this.repository = repository;
      this.processor = processor;
   }

   @Override
   public Optional<Content> loadContent(URI source) {
      return compatibleUri(source,
                           FileUriRestrictions.ofRestrictions(false, false, true, true, true),
                           FileUri::ofUri).<Content>map(uri -> {
         if (uri.isAbsolute()) {
            return null;
         } else {
            var document = repository.findDocumentById(DocumentPath.of(uri.toPath()));

            return document
                  .map(d -> processor != null ? d.process(processor) : d)
                  .map(DocumentContent::new)
                  .orElse(null);
         }
      }).optional();
   }

}