/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.projection.core.format.token.codepointset.GenericSet;

public class GenericSetTest {

   @Test
   public void containsWhenNominal() {
      var set = GenericSet.of(":");

      assertThat(set.contains(':')).isTrue();
      assertThat(set.contains((int) ':')).isTrue();
      assertThat(set.contains(Character.codePointAt(":", 0))).isTrue();
      assertThat(set.contains('-')).isFalse();
      assertThat(set.contains((int) '-')).isFalse();
      assertThat(set.contains(Character.codePointAt("-", 0))).isFalse();
   }

   @Test
   public void containsWhenUnicodeSurrogatePair() {
      var set = GenericSet.of("\uD83D\uDE00");

      assertThat(set.contains(Character.codePointAt("\uD83D\uDE00", 0))).isTrue();
      assertThat(set.contains(Character.toCodePoint('\uD83D', '\uDE00'))).isTrue();
      assertThat(set.contains('\uD83D')).isFalse();
      assertThat(set.contains('\uDE00')).isFalse();
   }

   @Test
   public void sizeWhenNominal() {
      var set = GenericSet.of(" -:");

      assertThat(set.size()).isEqualTo(3);
   }

   @Test
   public void sizeWhenUnicodeSurrogatePair() {
      var set = GenericSet.of("\uD83D\uDE00 -:");

      assertThat(set.size()).isEqualTo(4);
   }

   @Test
   public void iterateWhenNominal() {
      var set = GenericSet.of(" -:");

      assertThat(set.stream()).containsExactlyInAnyOrder((int) ' ', (int) '-', (int) ':');
   }

   @Test
   public void iterateWhenUnicodeSurrogatePair() {
      var set = GenericSet.of("\uD83D\uDE00 -:");

      assertThat(set.stream()).containsExactlyInAnyOrder(Character.toCodePoint('\uD83D', '\uDE00'),
                                                         (int) ' ',
                                                         (int) '-',
                                                         (int) ':');
   }
}