/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.set;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.function.UnaryOperator.identity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.memory.MemoryDocumentRepository;
import com.tinubu.projection.core.format.AliasFormat;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.formats.CapitalizeFormat;
import com.tinubu.projection.core.format.formats.LowerCamelCaseFormat;
import com.tinubu.projection.core.format.formats.LowerCaseFormat;
import com.tinubu.projection.core.format.formats.LowerKebabCaseFormat;
import com.tinubu.projection.core.format.formats.UpperCaseFormat;

public class DocumentSynchronizeProcessorTest extends BaseProjectionTest {

   private DocumentRepository projectRepository;

   @BeforeEach
   public void initProjectRepository() {
      this.projectRepository = new MemoryDocumentRepository();
   }

   @BeforeEach
   public void clearProcessorCaches() {
      DocumentSynchronizeProcessor.clearPlaceholderCache();
   }

   @AfterEach
   public void closeProjectRepository() {
      this.projectRepository.close();
   }

   @Override
   public DocumentRepository projectRepository() {
      return projectRepository;
   }

   @Test
   public void processWhenNominal() {
      var processor = processor();

      try (var templateRepository = new MemoryDocumentRepository()) {
         var template1 =
               createDocument(templateRepository, DocumentPath.of("template1.txt"), UTF_8, "content");

         assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
            assertThat(d.documentId()).isEqualTo(DocumentPath.of("template1.txt"));
            assertThatDocumentContent(d, "content");
         });
      }
   }

   @Nested
   class WhenPlaceholders {

      @Test
      public void processWhenNominal() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute__Z.txt"),
                                           UTF_8,
                                           "> $__attribute__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AvalueZ.txt"));
               assertThatDocumentContent(d, "> value <");
            });
         }
      }

      @Test
      public void processWhenEmptyPlaceholder() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(identity());
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 =
                  createDocument(templateRepository, DocumentPath.of("A____Z.txt"), UTF_8, "> $____ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A____Z.txt"));
               assertThatDocumentContent(d, "> $____ <");
            });
         }
      }

      @Test
      public void processWhenNoPlaceholders() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration, formats(), true);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute__Z.txt"),
                                           UTF_8,
                                           "> $__attribute__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AvalueZ.txt"));
               assertThatDocumentContent(d, "> $__attribute__ <");
            });
         }
      }

      @Test
      public void processWhenMissingAttribute() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(identity());
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute__Z.txt"),
                                           UTF_8,
                                           "> $__attribute__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute__Z.txt"));
               assertThatDocumentContent(d, "> $__attribute__ <");
            });
         }
      }

      @Test
      @Disabled("FIXME test logs ?")
      public void processWhenMissingAttributeInPathAndFailOnWarning() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(identity());
         var processor = processor(templateConfiguration, projectConfiguration, formats(), false);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__document__Z.txt"),
                                           UTF_8,
                                           "> $__attribute__ <");

            assertThatThrownBy(() -> assertThatProcessedDocument(templateRepository, template1, processor))
                  .isInstanceOf(IllegalStateException.class)
                  .hasMessage(
                        "Error while processing 'A__document__Z.txt' : '&' directive error: A__document__Z.txt : Missing 'document' project attribute referenced from placeholder");
         }
      }

      @Test
      @Disabled("FIXME test logs ?")
      public void processWhenMissingAttributeInContentAndFailOnWarning() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("document", "value")));
         var processor = processor(templateConfiguration, projectConfiguration, formats(), false);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__document__Z.txt"),
                                           UTF_8,
                                           "> $__attribute__ <");

            assertThatThrownBy(() -> assertThatProcessedDocument(templateRepository, template1, processor))
                  .as("Error message is sent by test infrastructure content consumer, so error message has no context prefix")
                  .isInstanceOf(IllegalStateException.class)
                  .hasMessage(
                        "'&' directive error: A__document__Z.txt : Missing 'attribute' project attribute referenced from placeholder");
         }
      }

      @Test
      public void processWhenAttributeFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_uppercase__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_uppercase__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
         }
      }

      @Test
      public void processWhenAttributeFormatAlternativeName() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_upper__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_upper__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
         }
      }

      @Test
      public void processWhenAttributeNoopFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute__Z.txt"),
                                           UTF_8,
                                           "> $__attribute__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("B__attribute_noop__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_noop__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AvalueZ.txt"));
               assertThatDocumentContent(d, "> value <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("BvalueZ.txt"));
               assertThatDocumentContent(d, "> value <");
            });
         }
      }

      @Test
      public void processWhenMissingNoopFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration, list(), false);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute__Z.txt"),
                                           UTF_8,
                                           "> $__attribute__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("B__attribute_noop__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_noop__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AvalueZ.txt"));
               assertThatDocumentContent(d, "> value <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("B__attribute_noop__Z.txt"));
               assertThatDocumentContent(d, "> $__attribute_noop__ <");
            });
         }
      }

      @Test
      public void processWhenAttributeCompoundFormat() {
         var templateConfiguration = projectionConfiguration(b -> b.placeholderContext(pcb -> pcb
               .contentFormatSeparator(".")
               .pathFormatSeparator("."), false));
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "VALUE")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute.lower.cap__Z.txt"),
                                           UTF_8,
                                           "> $__attribute.lower.cap__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute.lower..cap__Z.txt"),
                                           UTF_8,
                                           "> $__attribute.lower..cap__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AValueZ.txt"));
               assertThatDocumentContent(d, "> Value <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute.lower..cap__Z.txt"));
               assertThatDocumentContent(d, "> $__attribute.lower..cap__ <");
            });
         }
      }

      @Test
      public void processWhenAttributeNameWithSeparator() {
         var templateConfiguration = projectionConfiguration(b -> b.placeholderContext(pcb -> pcb
               .contentFormatSeparator(".")
               .pathFormatSeparator("."), false));
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of(".attribute.", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__.attribute.__Z.txt"),
                                           UTF_8,
                                           "> $__.attribute.__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("A__.attribute.upper__Z.txt"),
                                           UTF_8,
                                           "> $__.attribute.upper__ <");
            var template3 = createDocument(templateRepository,
                                           DocumentPath.of("A__.attribute..upper__Z.txt"),
                                           UTF_8,
                                           "> $__.attribute..upper__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AvalueZ.txt"));
               assertThatDocumentContent(d, "> value <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__.attribute.upper__Z.txt"));
               assertThatDocumentContent(d, "> $__.attribute.upper__ <");
            });
            assertThatProcessedDocument(templateRepository, template3, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
         }
      }

      @Test
      public void processWhenAttributeCompoundFormatOrderPreserved() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "VALUE")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_cap_lower__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_cap_lower__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AvalueZ.txt"));
               assertThatDocumentContent(d, "> value <");
            });
         }
      }

      @Test
      public void processWhenMissingFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "VALUE"),
                                                            Attribute.of("attribute_other", "OTHER")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_unknown__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_unknown__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_other__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_other__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_unknown__Z.txt"));
               assertThatDocumentContent(d, "> $__attribute_unknown__ <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AOTHERZ.txt"));
               assertThatDocumentContent(d, "> OTHER <");
            });
         }
      }

      /** Restricted formats do not apply to placeholders. */
      @Test
      public void processWhenRestrictedFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(b -> b
               .addAttributes(Attribute.of("attribute", "VALUE"), Attribute.of("attribute_other", "OTHER"))
               .restrictedFormats(set("kebab")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_upper__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_upper__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_other__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_other__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AOTHERZ.txt"));
               assertThatDocumentContent(d, "> OTHER <");
            });
         }
      }

      @Test
      public void processWhenMissingCompoundFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "VALUE"),
                                                            Attribute.of("attribute_other", "OTHER")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_lower_unknown__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_lower_unknown__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_other_unknown__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_other_unknown__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_lower_unknown__Z.txt"));
               assertThatDocumentContent(d, "> $__attribute_lower_unknown__ <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_other_unknown__Z.txt"));
               assertThatDocumentContent(d, "> $__attribute_other_unknown__ <");
            });
         }
      }

      /** Restricted formats do not apply to placeholders. */
      @Test
      public void processWhenRestrictedCompoundFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(b -> b
               .addAttributes(Attribute.of("attribute", "VALUE"), Attribute.of("attribute_other", "OTHER"))
               .restrictedFormats(set("kebab")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_lower_upper__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_lower_upper__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_other_upper__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_other_upper__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AOTHERZ.txt"));
               assertThatDocumentContent(d, "> OTHER <");
            });
         }
      }

      @Test
      public void processWhenExtraFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var extraFormat = new AliasFormat("alias", list(new LowerCaseFormat(), new CapitalizeFormat()));
         var projectConfiguration = projectionConfiguration(b -> b
               .addExtraFormat(extraFormat)
               .addAttributes(Attribute.of("attribute", "VALUE")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_upper_alias__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_upper_alias__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AValueZ.txt"));
               assertThatDocumentContent(d, "> Value <");
            });
         }
      }

      /** Extra formats missing delegates are ignored. */
      @Test
      public void processWhenExtraFormatDelegateToMissingFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(b -> b
               .addExtraFormat("alias", "unknown")
               .addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_alias__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_alias__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_alias__Z.txt"));
               assertThatDocumentContent(d, "> $__attribute_alias__ <");
            });
         }
      }

      /** Restricted formats do not apply to placeholders. */
      @Test
      public void processWhenExtraFormatDelegateToRestrictedFormats() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(b -> b
               .addExtraFormat("alias", "upper")
               .addAttributes(Attribute.of("attribute", "value"))
               .restrictedFormats(set("kebab")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_alias__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_alias__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
         }
      }

      /** Extra formats are always appended to format list. */
      @Test
      public void processWhenMissingExtraFormat() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration = projectionConfiguration(b -> b
               .addExtraFormat("alias", "upper")
               .addAttributes(Attribute.of("attribute", "value")));
         var processor =
               processor(templateConfiguration, projectConfiguration, list(new UpperCaseFormat()), false);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_alias__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_alias__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
         }
      }

      @Test
      public void processWhenConfiguredPlaceholderFormat() {
         var templateConfiguration = projectionConfiguration(b -> b.placeholderContext(pcb -> pcb
               .contentBegin("$--")
               .contentFormatSeparator("-")
               .contentEnd("--")
               .pathBegin("--")
               .pathFormatSeparator("-")
               .pathEnd("--"), false));
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A--attribute-lower-cap--Z.txt"),
                                           UTF_8,
                                           "> $--attribute-lower-cap-- <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AValueZ.txt"));
               assertThatDocumentContent(d, "> Value <");
            });
         }
      }

      @Test
      public void processWhenPackageAttribute() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "com.projection")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("__attribute_package__", "template.txt"),
                                           UTF_8,
                                           "> $__attribute_package__ <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_package__Z.txt"),
                                           UTF_8,
                                           "> $__attribute_package__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("com", "projection", "template.txt"));
               assertThatDocumentContent(d, "> com/projection <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("Acom", "projectionZ.txt"));
               assertThatDocumentContent(d, "> com/projection <");
            });
         }
      }

      @Test
      public void processWhenExplicitValueDirective() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__&(attribute_lower_cap)__Z.txt"),
                                           UTF_8,
                                           "> $__&(attribute_lower_cap)__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AValueZ.txt"));
               assertThatDocumentContent(d, "> Value <");
            });
         }
      }

      @Test
      public void processWhenSuppressWhitespace() {
         var templateConfiguration = projectionConfiguration(identity());
         var projectConfiguration =
               projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute_upper-__  Z.txt"),
                                           UTF_8,
                                           "> $__attribute_upper-__  <");
            var template2 = createDocument(templateRepository,
                                           DocumentPath.of("B__attribute_upper-1__  Z.txt"),
                                           UTF_8,
                                           "> $__attribute_upper-1__  <");
            var template3 = createDocument(templateRepository,
                                           DocumentPath.of("C__attribute_upper-0__  Z.txt"),
                                           UTF_8,
                                           "> $__attribute_upper-0__  <");
            var template4 = createDocument(templateRepository,
                                           DocumentPath.of("D__attribute_upper-*__  Z.txt"),
                                           UTF_8,
                                           "> $__attribute_upper-*__  <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUE Z.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
            assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("BVALUE Z.txt"));
               assertThatDocumentContent(d, "> VALUE <");
            });
            assertThatProcessedDocument(templateRepository, template3, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("CVALUE  Z.txt"));
               assertThatDocumentContent(d, "> VALUE  <");
            });
            assertThatProcessedDocument(templateRepository, template4, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("DVALUEZ.txt"));
               assertThatDocumentContent(d, "> VALUE<");
            });
         }
      }

      @Test
      public void processWhenProjectConfigurationOverridePlaceholderContext() {
         var templateConfiguration = projectionConfiguration(b -> b.placeholderContext(pcb -> pcb
               .contentFormatSeparator(".")
               .pathFormatSeparator("."), false));
         var projectConfiguration = projectionConfiguration(b -> b
               .placeholderContext(pcb -> pcb.contentFormatSeparator("-").pathFormatSeparator("-"), false)
               .addAttributes(Attribute.of("attribute", "VALUE")));
         var processor = processor(templateConfiguration, projectConfiguration);

         try (var templateRepository = new MemoryDocumentRepository()) {
            var template1 = createDocument(templateRepository,
                                           DocumentPath.of("A__attribute.lower.cap__Z.txt"),
                                           UTF_8,
                                           "> $__attribute.lower.cap__ <");

            assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
               assertThat(d.documentId()).isEqualTo(DocumentPath.of("AValueZ.txt"));
               assertThatDocumentContent(d, "> Value <");
            });
         }
      }

      @Nested
      class WhenInferPlaceholders {

         @Test
         public void processWhenNominal() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration =
                  projectionConfiguration(b -> b.placeholderContext(pcb -> pcb.inferPlaceholders(true)));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("Value.txt"), UTF_8, "> Value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("__attribute_cap__.txt"));
                  assertThatDocumentContent(d, "> $__attribute_cap__ <");
               });
            }
         }

         /** Format compact name must be used for placeholder inference, when available. */
         @Test
         public void processWhenCompactName() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration =
                  projectionConfiguration(b -> b.placeholderContext(pcb -> pcb.inferPlaceholders(true)));
            var processor = processor(templateConfiguration,
                                      projectConfiguration,
                                      list(new AliasFormat(list("alias1", "alias1compact"),
                                                           list(new UpperCaseFormat())),
                                           new AliasFormat(list("alias2"), list(new LowerCaseFormat()))),
                                      false);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("VALUE.txt"), UTF_8, "> VALUE <");
               var template2 =
                     createDocument(templateRepository, DocumentPath.of("value.txt"), UTF_8, "> value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("__attribute_alias1compact__.txt"));
                  assertThatDocumentContent(d, "> $__attribute_alias1compact__ <");
               });
               assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("__attribute_alias2__.txt"));
                  assertThatDocumentContent(d, "> $__attribute_alias2__ <");
               });
            }
         }

         @Test
         public void processWhenOverridePlaceholderFormat() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b.placeholderContext(pcb -> pcb
                  .inferPlaceholders(true)
                  .format(PlaceholderFormat
                                .builder()
                                .contentBegin("${")
                                .contentEnd("}")
                                .contentFormatSeparator("-")
                                .pathBegin("_")
                                .pathEnd("_")
                                .pathFormatSeparator("-")
                                .build())));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("Value.txt"), UTF_8, "> Value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("_attribute-cap_.txt"));
                  assertThatDocumentContent(d, "> ${attribute-cap} <");
               });
            }
         }

         @Test
         public void processWhenConcurrentAttributesPreserveOrder() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute1", "value"),
                                                               Attribute.of("attribute2", "value")));
            var projectConfiguration =
                  projectionConfiguration(b -> b.placeholderContext(pcb -> pcb.inferPlaceholders(true)));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("Value.txt"), UTF_8, "> Value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("__attribute1_cap__.txt"));
                  assertThatDocumentContent(d, "> $__attribute1_cap__ <");
               });
            }
         }

         @Test
         public void processWhenConcurrentFormatsPreserveOrder() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b
                  .placeholderContext(pcb -> pcb.inferPlaceholders(true))
                  .addExtraFormats(new AliasFormat("alias", list(new LowerCamelCaseFormat()))));
            List<Format> formats =
                  list(new LowerCaseFormat(), new LowerKebabCaseFormat(), new LowerCamelCaseFormat());
            var processor = processor(templateConfiguration, projectConfiguration, formats, false);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("value.txt"), UTF_8, "> value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("__attribute_lower__.txt"));
                  assertThatDocumentContent(d, "> $__attribute_lower__ <");
               });
            }
         }

         /** Special noop must be inferred without format specifier, and in priority, when concurrent formats. */
         @Test
         public void processWhenNoopFormat() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration =
                  projectionConfiguration(b -> b.placeholderContext(pcb -> pcb.inferPlaceholders(true)));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("value.txt"), UTF_8, "> value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("__attribute__.txt"));
                  assertThatDocumentContent(d, "> $__attribute__ <");
               });
            }
         }

         /** Restricted format apply to inference, including placeholders inference. */
         @Test
         public void processWhenRestrictedFormat() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b
                  .placeholderContext(pcb -> pcb.inferPlaceholders(true))
                  .restrictedFormats(set("kebab")));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("AVALUEZ.txt"), UTF_8, "> VALUE <");
               var template2 =
                     createDocument(templateRepository, DocumentPath.of("AvalueZ.txt"), UTF_8, "> value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
                  assertThatDocumentContent(d, "> VALUE <");
               });
               assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_kebabcase__Z.txt"));
                  assertThatDocumentContent(d, "> $__attribute_kebabcase__ <");
               });
            }
         }

         /** Noop format must be always available for inference, even if restricted. */
         @Test
         public void processWhenRestrictedNoopFormat() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b
                  .placeholderContext(pcb -> pcb.inferPlaceholders(true))
                  .restrictedFormats(set("upper")));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("value.txt"), UTF_8, "> value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("__attribute__.txt"));
                  assertThatDocumentContent(d, "> $__attribute__ <");
               });
            }
         }

         /** Missing restricted formats are ignored. */
         @Test
         public void processWhenMissingRestrictedFormat() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b
                  .placeholderContext(pcb -> pcb.inferPlaceholders(true))
                  .restrictedFormats(set("kebab", "unknown")));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("AVALUEZ.txt"), UTF_8, "> VALUE <");
               var template2 =
                     createDocument(templateRepository, DocumentPath.of("AvalueZ.txt"), UTF_8, "> value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
                  assertThatDocumentContent(d, "> VALUE <");
               });
               assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_kebabcase__Z.txt"));
                  assertThatDocumentContent(d, "> $__attribute_kebabcase__ <");
               });
            }
         }

         /** Restricted format list must be empty if all restricted formats are missing. */
         @Test
         public void processWhenAllMissingRestrictedFormat() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b
                  .placeholderContext(pcb -> pcb.inferPlaceholders(true))
                  .restrictedFormats(set("unknown1", "unknown2")));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("AVALUEZ.txt"), UTF_8, "> VALUE <");
               var template2 =
                     createDocument(templateRepository, DocumentPath.of("AvalueZ.txt"), UTF_8, "> value <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("AVALUEZ.txt"));
                  assertThatDocumentContent(d, "> VALUE <");
               });
               assertThatProcessedDocument(templateRepository, template2, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute__Z.txt"));
                  assertThatDocumentContent(d, "> $__attribute__ <");
               });
            }
         }

         /** Extra formats are never restricted. */
         @Test
         public void processWhenRestrictedExtraFormat() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b
                  .addExtraFormat(new AliasFormat("alias", list(new UpperCaseFormat())))
                  .placeholderContext(pcb -> pcb.inferPlaceholders(true))
                  .restrictedFormats(set("kebab")));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("AVALUEZ.txt"), UTF_8, "> VALUE <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_alias__Z.txt"));
                  assertThatDocumentContent(d, "> $__attribute_alias__ <");
               });
            }
         }

         /** Extra format delegates are not restricted. */
         @Test
         public void processWhenRestrictedExtraFormatDelegate() {
            var templateConfiguration =
                  projectionConfiguration(b -> b.addAttributes(Attribute.of("attribute", "value")));
            var projectConfiguration = projectionConfiguration(b -> b
                  .addExtraFormat("alias", "upper")
                  .placeholderContext(pcb -> pcb.inferPlaceholders(true))
                  .restrictedFormats(set("kebab")));
            var processor = processor(templateConfiguration, projectConfiguration);

            try (var templateRepository = new MemoryDocumentRepository()) {
               var template1 =
                     createDocument(templateRepository, DocumentPath.of("AVALUEZ.txt"), UTF_8, "> VALUE <");

               assertThatProcessedDocument(templateRepository, template1, processor).satisfies(d -> {
                  assertThat(d.documentId()).isEqualTo(DocumentPath.of("A__attribute_alias__Z.txt"));
                  assertThatDocumentContent(d, "> $__attribute_alias__ <");
               });
            }
         }

      }

   }
}

