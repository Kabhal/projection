/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.MechanicalSympathy.generalBufferSize;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.function.UnaryOperator.identity;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;
import org.assertj.core.api.AbstractObjectAssert;
import org.junit.jupiter.api.BeforeEach;

import com.tinubu.commons.ddd2.uri.Uri;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.uri.DocumentUri;
import com.tinubu.commons.ports.document.domain.uri.RepositoryUri;
import com.tinubu.commons.ports.document.memory.MemoryDocumentRepository;
import com.tinubu.projection.core.config.ConfigurableProjectionConfiguration;
import com.tinubu.projection.core.config.ProjectionConfiguration;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.ServiceLoaderFormatFactory;

/**
 * Base test context and tooling projection tests.
 */
public abstract class BaseProjectionTest {

   protected static final List<Format> DEFAULT_FORMATS = immutable(new ServiceLoaderFormatFactory().formats());

   protected static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   protected List<Format> formats() {
      return DEFAULT_FORMATS;
   }

   protected DocumentSynchronizeProcessor processor(ProjectionConfiguration templateConfiguration,
                                                    ProjectionConfiguration protectConfiguration,
                                                    List<Format> formats,
                                                    boolean noPlaceholders) {
      return new DocumentSynchronizeProcessor(templateConfiguration,
                                              protectConfiguration,
                                              __ -> false,
                                              formats,
                                              noPlaceholders);
   }

   protected DocumentSynchronizeProcessor processor(ProjectionConfiguration templateConfiguration,
                                                    ProjectionConfiguration protectConfiguration) {
      return processor(templateConfiguration, protectConfiguration, formats(), false);
   }

   protected DocumentSynchronizeProcessor processor() {
      var templateConfiguration = projectionConfiguration(identity());
      var projectConfiguration = projectionConfiguration(identity());

      return processor(templateConfiguration, projectConfiguration);
   }

   protected ProjectionConfiguration projectionConfiguration(UnaryOperator<ConfigurableProjectionConfiguration.Builder> mapper) {
      ProjectionConfiguration templateConfiguration =
            new ConfigurableProjectionConfiguration.Builder().chain(mapper).build();
      return templateConfiguration;
   }

   @BeforeEach
   public void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   public abstract DocumentRepository projectRepository();

   protected void assertThatDocumentContent(Document document, InputStream expected) {
      assertThat(document.content().inputStreamContent())
            .as("Document '%s' content differs from expected content", document.documentId().stringValue())
            .hasSameContentAs(expected);
   }

   protected void assertThatDocumentContent(Document document, Charset encoding, Reader expected) {
      assertThatDocumentContent(document, new ReaderInputStream(expected, encoding));
   }

   protected void assertThatDocumentContent(Document document, Charset encoding, String expected) {
      assertThatDocumentContent(document, new ByteArrayInputStream(expected.getBytes(encoding)));
   }

   protected void assertThatDocumentContent(Document document, String expected) {
      assertThatDocumentContent(document, UTF_8, expected);
   }

   @SuppressWarnings("unchecked")
   protected AbstractObjectAssert<?, Document> assertThatProcessedDocument(MemoryDocumentRepository templateRepository,
                                                                           DocumentEntry templateDocument,
                                                                           DocumentSynchronizeProcessor processor) {
      var process =
            processDocument(templateRepository.findDocumentById(templateDocument.documentId()).orElseThrow(),
                            processor);

      return assertThat(process)
            .as("Processed document can't be saved because overwrite is disabled and test document already exists")
            .isPresent()
            .map(processed -> projectRepository().findDocumentById(processed.documentId()).get())
            .get();
   }

   public static Path path(String path) {
      return nullable(path).map(Paths::get).orElse(null);
   }

   public static DocumentUri documentUri(Uri uri) {
      return DocumentUri.ofDocumentUri(uri);
   }

   public static DocumentUri documentUri(URI uri) {
      return DocumentUri.ofDocumentUri(uri);
   }

   public static DocumentUri documentUri(String uri) {
      return DocumentUri.ofDocumentUri(uri);
   }

   public static RepositoryUri repositoryUri(Uri uri) {
      return RepositoryUri.ofRepositoryUri(uri);
   }

   public static RepositoryUri repositoryUri(URI uri) {
      return RepositoryUri.ofRepositoryUri(uri);
   }

   public static RepositoryUri repositoryUri(String uri) {
      return RepositoryUri.ofRepositoryUri(uri);
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId,
                                          UnaryOperator<DocumentMetadataBuilder> metadataBuilderOperation,
                                          UnaryOperator<InputStreamDocumentContentBuilder> contentBuilderOperation) {
      String content = documentId.stringValue();

      return new DocumentBuilder()
            .<DocumentBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder()
                            .<DocumentMetadataBuilder>reconstitute()
                            .documentPath(documentId.value())
                            .creationDate(stubCreationDate())
                            .lastUpdateDate(stubLastUpdateDate())
                            .chain(metadataBuilderOperation)
                            .build())
            .content(new InputStreamDocumentContentBuilder()
                           .<InputStreamDocumentContentBuilder>reconstitute()
                           .content(content, UTF_8)
                           .chain(contentBuilderOperation)
                           .build());
   }

   protected Instant stubLastUpdateDate() {
      return ApplicationClock.nowAsInstant().plusSeconds(60);
   }

   protected Instant stubCreationDate() {
      return ApplicationClock.nowAsInstant();
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId,
                                          UnaryOperator<DocumentMetadataBuilder> metadataBuilderOperation) {
      return stubDocument(documentId, metadataBuilderOperation, identity());
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, identity(), identity());
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, Document document) {
      return documentRepository
            .saveDocument(document, true)
            .orElseThrow(() -> new IllegalStateException(String.format("Can't create '%s' document",
                                                                       document.documentId().stringValue())));
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, Path documentPath) {
      return createDocument(documentRepository, stubDocument(DocumentPath.of(documentPath)).build());
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository,
                                          Path documentPath,
                                          Charset encoding,
                                          String content) {
      return createDocument(documentRepository,
                            stubDocument(DocumentPath.of(documentPath),
                                         identity(),
                                         cb -> cb.content(content, encoding)).build());
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository,
                                          Path documentPath,
                                          String content) {
      return createDocument(documentRepository, documentPath, UTF_8, content);
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, String documentPath) {
      return createDocument(documentRepository, path(documentPath));
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository,
                                          String documentPath,
                                          Charset encoding,
                                          String content) {
      return createDocument(documentRepository, path(documentPath), encoding, content);
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository,
                                          String documentPath,
                                          String content) {
      return createDocument(documentRepository, documentPath, UTF_8, content);
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, DocumentPath documentPath) {
      return createDocument(documentRepository, documentPath.value());
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository,
                                          DocumentPath documentPath,
                                          Charset encoding,
                                          String content) {
      return createDocument(documentRepository, documentPath.value(), encoding, content);
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository,
                                          DocumentPath documentPath,
                                          String content) {
      return createDocument(documentRepository, documentPath, UTF_8, content);
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository, Path... paths) {
      return createDocuments(documentRepository, list(paths));
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository,
                                                 DocumentPath... paths) {
      return createDocuments(documentRepository, list(stream(paths).map(DocumentPath::value)));
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository, String... paths) {
      return createDocuments(documentRepository, list(stream(paths).map(Paths::get)));
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository, List<Path> paths) {
      return list(stream(paths).map(documentPath -> createDocument(documentRepository, documentPath)));
   }

   protected void deleteDocument(DocumentRepository documentRepository, DocumentPath documentId) {
      if (documentRepository.hasCapability(RepositoryCapability.WRITABLE)) {
         documentRepository.deleteDocumentById(documentId);
      }
   }

   protected void deleteDocuments(DocumentRepository documentRepository, DocumentPath... documentIds) {
      stream(documentIds).forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   protected void deleteDocuments(DocumentRepository documentRepository, List<Path> paths) {
      stream(paths)
            .map(DocumentPath::of)
            .forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   protected void deleteDocuments(DocumentRepository documentRepository, String... paths) {
      stream(paths)
            .map(DocumentPath::of)
            .forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   protected void deleteDocuments(DocumentRepository documentRepository, Path... paths) {
      stream(paths)
            .map(DocumentPath::of)
            .forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   /**
    * Specialized document comparator that ignores if content is a stream or loaded content.
    *
    * @param <T> document type
    *
    * @return document comparator
    */
   protected <T extends Document> Comparator<? super T> contentAgnosticDocumentComparator() {
      return (d1, d2) -> d1.documentId().sameValueAs(d2.documentId()) && d1
            .metadata()
            .sameValueAs(d2.metadata()) && d1
                               .content()
                               .contentEncoding()
                               .equals(d2.content().contentEncoding()) && d1
                               .content()
                               .contentSize()
                               .equals(d2.content().contentSize()) && Arrays.equals(d1.content().content(),
                                                                                    d2.content().content())
                         ? 0
                         : -1;
   }

   public static DocumentContent writeContent(DocumentContent documentContent, Reader reader) {
      try (BufferedWriter writer = new BufferedWriter(documentContent.writerContent(UTF_8),
                                                      generalBufferSize())) {
         IOUtils.copy(reader, writer);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
      return documentContent;
   }

   public static DocumentContent writeContent(DocumentContent documentContent, InputStream inputStream) {
      try (BufferedOutputStream outputStream = new BufferedOutputStream(documentContent.outputStreamContent(),
                                                                        generalBufferSize())) {
         IOUtils.copy(inputStream, outputStream);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
      return documentContent;
   }

   public static DocumentContent writeContent(DocumentContent documentContent, String content) {
      return writeContent(documentContent, new StringReader(content));
   }

   private Optional<DocumentEntry> processDocument(Document document,
                                                   DocumentSynchronizeProcessor processor,
                                                   boolean overwrite) {
      return projectRepository().saveDocument(processor.process(document), overwrite);
   }

   protected Optional<DocumentEntry> processDocument(Document document,
                                                     DocumentSynchronizeProcessor processor) {
      return processDocument(document, processor, false);
   }
}