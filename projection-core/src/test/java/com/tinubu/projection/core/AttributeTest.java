/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.ThrowingConsumer;
import org.junit.jupiter.api.Test;

import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.formats.AlphanumOnlyFormat;
import com.tinubu.projection.core.format.formats.AlphanumSpaceFormat;
import com.tinubu.projection.core.format.formats.CapitalizeFormat;
import com.tinubu.projection.core.format.formats.HyphenateFormat;
import com.tinubu.projection.core.format.formats.LowerCamelCaseFormat;
import com.tinubu.projection.core.format.formats.LowerCaseFormat;
import com.tinubu.projection.core.format.formats.LowerKebabCaseFormat;
import com.tinubu.projection.core.format.formats.LowerSnakeCaseFormat;
import com.tinubu.projection.core.format.formats.UnCapitalizeFormat;
import com.tinubu.projection.core.format.formats.UpperCamelCaseFormat;
import com.tinubu.projection.core.format.formats.UpperCaseFormat;
import com.tinubu.projection.core.format.formats.UpperKebabCaseFormat;
import com.tinubu.projection.core.format.formats.UpperSnakeCaseFormat;
import com.tinubu.projection.core.format.formats.WordCapitalizeFormat;
import com.tinubu.projection.core.format.formats.WordOnlyFormat;
import com.tinubu.projection.core.format.formats.WordSpaceFormat;

public class AttributeTest {

   /** Formats should be sorted because their order affects grouping. */
   private static final List<Format> FORMATS = list(stream(new LowerCaseFormat(),
                                                           new UpperCaseFormat(),
                                                           new HyphenateFormat(),
                                                           new CapitalizeFormat(), new UnCapitalizeFormat(),
                                                           new AlphanumOnlyFormat(),
                                                           new AlphanumSpaceFormat(),
                                                           new WordOnlyFormat(),
                                                           new WordSpaceFormat(),
                                                           new WordCapitalizeFormat(),
                                                           new LowerCamelCaseFormat(),
                                                           new UpperCamelCaseFormat(),
                                                           new LowerSnakeCaseFormat(),
                                                           new UpperSnakeCaseFormat(),
                                                           new LowerKebabCaseFormat(),
                                                           new UpperKebabCaseFormat()).sorted(comparing(Format::name)));

   @Test
   public void testGroupFormats() {
      Attribute attribute1 = Attribute.of("com.group", "project");

      assertThat(attribute1.groupFormats(FORMATS)).satisfiesExactly(matchFormats("alphanum-only",
                                                                                 "alphanum-space",
                                                                                 "hyphenate",
                                                                                 "lower-camelcase",
                                                                                 "lower-kebabcase",
                                                                                 "lower-snakecase",
                                                                                 "lowercase", "uncapitalize",
                                                                                 "word-only",
                                                                                 "word-space"),
                                                                    matchFormats("capitalize",
                                                                                 "upper-camelcase",
                                                                                 "word-capitalize"),
                                                                    matchFormats("upper-kebabcase",
                                                                                 "upper-snakecase",
                                                                                 "uppercase"));

      Attribute attribute2 = Attribute.of("com.group", "my-project");

      assertThat(attribute2.groupFormats(FORMATS)).satisfiesExactly(matchFormats("alphanum-only",
                                                                                 "word-only"),
                                                                    matchFormats("alphanum-space",
                                                                                 "word-space"),
                                                                    matchFormats("capitalize"),
                                                                    matchFormats("hyphenate",
                                                                                 "lower-kebabcase",
                                                                                 "lowercase", "uncapitalize"),
                                                                    matchFormats("lower-camelcase"),
                                                                    matchFormats("lower-snakecase"),
                                                                    matchFormats("upper-camelcase"),
                                                                    matchFormats("upper-kebabcase",
                                                                                 "uppercase"),
                                                                    matchFormats("upper-snakecase"),
                                                                    matchFormats("word-capitalize"));
   }

   private static ThrowingConsumer<List<Format>> matchFormats(String... groups) {
      return group -> assertThat(group).extracting(Format::name).containsExactly(groups);
   }
}