/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.projection.core.format.formats.CapitalizeFormat;
import com.tinubu.projection.core.format.formats.LowerCaseFormat;
import com.tinubu.projection.core.format.formats.UpperCaseFormat;

public class AliasFormatTest {

   @Test
   public void aliasFormatWhenNominal() {
      var alias = new AliasFormat("alias", list(new UpperCaseFormat()));

      assertThat(alias.names()).containsExactly("alias");
      assertThat(alias.name()).isEqualTo("alias");
      assertThat(alias.compactName()).isEqualTo("alias");
      assertThat(alias.delegates()).containsExactly(new UpperCaseFormat());
   }

   @Test
   public void aliasFormatWhenAlternativeNames() {
      var alias = new AliasFormat(list("alias", "alt"), list(new UpperCaseFormat()));

      assertThat(alias.names()).containsExactly("alias", "alt");
      assertThat(alias.name()).isEqualTo("alias");
      assertThat(alias.compactName()).isEqualTo("alt");
      assertThat(alias.delegates()).containsExactly(new UpperCaseFormat());
   }

   @Test
   public void aliasFormatWhenDelegateLoop() {
      assertThatThrownBy(() -> new AliasFormat("alias", new AliasFormat("alias", new UpperCaseFormat())))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'delegates=[AliasFormat[names=[alias], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]]' > 'delegates[0]=AliasFormat[names=[alias], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]' must not have loop on [alias]");
      assertThatThrownBy(() -> new AliasFormat("alias",
                                               new AliasFormat("other",
                                                               new AliasFormat("alias",
                                                                               new UpperCaseFormat()))))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'delegates=[AliasFormat[names=[other], delegates=[AliasFormat[names=[alias], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]]]]' > 'delegates[0]=AliasFormat[names=[other], delegates=[AliasFormat[names=[alias], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]]]' must not have loop on [alias]");
   }

   @Test
   public void aliasFormatWhenDelegateLoopUsingAltName() {
      assertThatThrownBy(() -> new AliasFormat(list("alias", "alt"),
                                               new AliasFormat("alt", new UpperCaseFormat())))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'delegates=[AliasFormat[names=[alt], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]]' > 'delegates[0]=AliasFormat[names=[alt], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]' must not have loop on [alias, alt]");
      assertThatThrownBy(() -> new AliasFormat(list("alias", "alt"),
                                               new AliasFormat("other",
                                                               new AliasFormat("alt",
                                                                               new UpperCaseFormat()))))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'delegates=[AliasFormat[names=[other], delegates=[AliasFormat[names=[alt], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]]]]' > 'delegates[0]=AliasFormat[names=[other], delegates=[AliasFormat[names=[alt], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]]]' must not have loop on [alias, alt]");
   }

   @Test
   public void ofSpecificationWhenNominal() {
      var alias = AliasFormat.ofSpecification(map(entry("upper", new UpperCaseFormat())), "alias", "upper");

      assertThat(alias.names()).containsExactly("alias");
      assertThat(alias.name()).isEqualTo("alias");
      assertThat(alias.compactName()).isEqualTo("alias");
      assertThat(alias.delegates()).containsExactly(new UpperCaseFormat());
   }

   @Test
   public void ofSpecificationWhenAltName() {
      var alias =
            AliasFormat.ofSpecification(map(entry("upper", new UpperCaseFormat())), "alias;alt", "upper");

      assertThat(alias.names()).containsExactly("alias", "alt");
      assertThat(alias.name()).isEqualTo("alias");
      assertThat(alias.compactName()).isEqualTo("alt");
      assertThat(alias.delegates()).containsExactly(new UpperCaseFormat());
   }

   @Test
   public void ofSpecificationWhenCompoundFormat() {
      var alias = AliasFormat.ofSpecification(map(entry("lower", new LowerCaseFormat()),
                                                  entry("cap", new CapitalizeFormat())),
                                              "alias",
                                              "lower|cap");

      assertThat(alias.names()).containsExactly("alias");
      assertThat(alias.name()).isEqualTo("alias");
      assertThat(alias.compactName()).isEqualTo("alias");
      assertThat(alias.delegates()).containsExactly(new LowerCaseFormat(), new CapitalizeFormat());
   }

   @Test
   public void ofSpecificationWhenDelegateLoop() {
      assertThatThrownBy(() -> AliasFormat.ofSpecification(map(entry("lower", new LowerCaseFormat()),
                                                               entry("alias",
                                                                     new AliasFormat("alias",
                                                                                     new UpperCaseFormat()))),
                                                           "alias",
                                                           "lower|alias"))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'delegates=[LowerCaseFormat[names='[lowercase, lower]'],AliasFormat[names=[alias], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]]' > 'delegates[1]=AliasFormat[names=[alias], delegates=[UpperCaseFormat[names='[uppercase, upper]']]]' must not have loop on [alias]");
   }

   @Nested
   class InferenceOrder {

      @Test
      public void inferenceOrderWhenNominal() {
         var alias = new AliasFormat("alias", list(new LowerCaseFormat(), new CapitalizeFormat()));

         assertThat(alias.inferenceOrder())
               .isEqualTo(1000)
               .isEqualTo((new LowerCaseFormat().inferenceOrder() + new CapitalizeFormat().inferenceOrder())
                          / 2);
      }

      @Test
      public void inferenceOrderWhenUpdated() {
         var alias =
               new AliasFormat("alias", list(new LowerCaseFormat(), new CapitalizeFormat())).inferenceOrder(
                     4000);

         assertThat(alias.inferenceOrder()).isEqualTo(4000);
      }

      @Test
      public void inferenceOrderWhenShifted() {
         var alias = new AliasFormat("alias",
                                     list(new LowerCaseFormat(), new CapitalizeFormat())).inferenceOrder(0);

         assertThat(alias.shiftInferenceOrder(1000).inferenceOrder()).isEqualTo(1000);
         assertThat(alias.shiftInferenceOrder(-1000).inferenceOrder()).isEqualTo(-1000);
         assertThat(alias
                          .shiftInferenceOrder(Integer.MAX_VALUE)
                          .inferenceOrder()).isEqualTo(Integer.MAX_VALUE);
         assertThat(alias
                          .shiftInferenceOrder(Integer.MIN_VALUE)
                          .inferenceOrder()).isEqualTo(Integer.MIN_VALUE);
         assertThat(alias
                          .shiftInferenceOrder(Integer.MAX_VALUE)
                          .shiftInferenceOrder(1)
                          .inferenceOrder()).isEqualTo(Integer.MAX_VALUE);
         assertThat(alias
                          .shiftInferenceOrder(Integer.MIN_VALUE)
                          .shiftInferenceOrder(-1)
                          .inferenceOrder()).isEqualTo(Integer.MIN_VALUE);
      }

   }
}