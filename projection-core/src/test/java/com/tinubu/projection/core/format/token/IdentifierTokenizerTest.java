/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format.token;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.assertj.core.api.ListAssert;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

public class IdentifierTokenizerTest {

   @Test
   public void tokensWhenNominal() {
      assertThatTokens("This value").containsExactly("This", "value");
   }

   @Test
   public void tokensWhenNull() {
      assertThatThrownBy(() -> assertThatTokens(null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'input' must not be null");
   }

   @Test
   public void tokensWhenEmpty() {
      assertThatTokens("").isEmpty();
   }

   @Test
   public void tokensWhenOnlyNonAlphanum() {
      assertThatTokens("!- \t\r\n._").isEmpty();
   }

   @Test
   public void tokensWhenAllSeparators() {
      assertThatTokens("This value").containsExactly("This", "value");
      assertThatTokens("This_value").containsExactly("This", "value");
      assertThatTokens("This-value").containsExactly("This", "value");
      assertThatTokens("This.value").containsExactly("This", "value");
   }

   @Test
   public void tokensWhenPrefixSuffixSeparators() {
      assertThatTokens(" This value ").containsExactly("This", "value");
      assertThatTokens("_This_value_").containsExactly("This", "value");
      assertThatTokens("-This-value-").containsExactly("This", "value");
      assertThatTokens(".This.value.").containsExactly("This", "value");
   }

   @Test
   public void tokensWhenMultipleSeparators() {
      assertThatTokens(" - This - value - ").containsExactly("This", "value");
      assertThatTokens("_-_This_-_value_-_").containsExactly("This", "value");
      assertThatTokens("-_-This-_-value-_-").containsExactly("This", "value");
      assertThatTokens(".-.This.-.value.-.").containsExactly("This", "value");
   }

   @Test
   public void tokensWhenSplitNumbers() {
      assertThatTokens("This-va20lue", true).containsExactly("This", "va", "20", "lue");
      assertThatTokens("This-20-value", true).containsExactly("This", "20", "value");
      assertThatTokens("20This-value", true).containsExactly("20", "This", "value");
      assertThatTokens("T20his-value", true).containsExactly("T", "20", "his", "value");
   }

   @Test
   public void tokensWithDisabledSplitNumbers() {
      assertThatTokens("This-20value", false).containsExactly("This", "20value");
      assertThatTokens("This-va20lue", false).containsExactly("This", "va20lue");
      assertThatTokens("This-20-value", false).containsExactly("This", "20", "value");
      assertThatTokens("20This-value", false).containsExactly("20T", "his", "value");
      assertThatTokens("T20his-value", false).containsExactly("T20", "his", "value");
   }

   @Test
   public void tokensWithDisabledSplitNumbersWhenNumberInUpperCaseToken() {
      assertThatTokens("A2value", false).containsExactly("A2", "value");
      assertThatTokens("AA2value", false).containsExactly("AA2", "value");
      assertThatTokens("A20value", false).containsExactly("A20", "value");
      assertThatTokens("AA20value", false).containsExactly("AA20", "value");
      assertThatTokens("2Avalue", false).containsExactly("2A", "value");
      assertThatTokens("2AAvalue", false).containsExactly("2A", "Avalue");
      assertThatTokens("20AAvalue", false).containsExactly("20A", "Avalue");
   }

   @Test
   public void tokensWhenCamelCase() {
      assertThatTokens("ThisValue").containsExactly("This", "Value");
      assertThatTokens("thisValue").containsExactly("this", "Value");
      assertThatTokens("thisIOValue").containsExactly("this", "IO", "Value");
      assertThatTokens("thisIOV").containsExactly("this", "IOV");
      assertThatTokens("thisIsAValue").containsExactly("this", "Is", "A", "Value");
   }

   @Test
   public void tokensWhenCamelCaseWithSeparators() {
      assertThatTokens("-This-Value-").containsExactly("This", "Value");
      assertThatTokens("-this-Value-").containsExactly("this", "Value");
      assertThatTokens("-this-IO-Value-").containsExactly("this", "IO", "Value");
      assertThatTokens("-this-IOV-").containsExactly("this", "IOV");
      assertThatTokens("-this-IO-V-").containsExactly("this", "IO", "V");
      assertThatTokens("-this-Is-A-Value-").containsExactly("this", "Is", "A", "Value");
   }

   @Test
   public void tokensWhenCamelCaseWithNumbers() {
      assertThatTokens("0This1Value2").containsExactly("0", "This", "1", "Value", "2");
      assertThatTokens("0this1Value2").containsExactly("0", "this", "1", "Value", "2");
      assertThatTokens("0this1IO2Value3").containsExactly("0", "this", "1", "IO", "2", "Value", "3");
      assertThatTokens("0this1IO2V3").containsExactly("0", "this", "1", "IO", "2", "V", "3");
      assertThatTokens("0this1Is2A3Value4").containsExactly("0",
                                                            "this",
                                                            "1",
                                                            "Is",
                                                            "2",
                                                            "A",
                                                            "3",
                                                            "Value",
                                                            "4");
   }

   @Test
   public void tokensWhenCamelCaseWithNumbersWithDisabledSplitNumbers() {
      assertThatTokens("0This1Value2", false).containsExactly("0T", "his1", "Value2");
      assertThatTokens("0this1Value2", false).containsExactly("0this1", "Value2");
      assertThatTokens("0this1IO2Value3", false).containsExactly("0this1", "IO2", "Value3");
      assertThatTokens("0this1IO2value3", false).containsExactly("0this1", "IO2", "value3");
      assertThatTokens("0this1IO2V3", false).containsExactly("0this1", "IO2V3");
      assertThatTokens("0this1Is2A3Value4", false).containsExactly("0this1", "Is2", "A3", "Value4");
   }

   @Test
   public void tokensWhenCamelCaseWithSeparatorsAndNumbers() {
      assertThatTokens("0This1Value2", false).containsExactly("0T", "his1", "Value2");
      assertThatTokens("0this1Value2", false).containsExactly("0this1", "Value2");
      assertThatTokens("0this1IO2Value3", false).containsExactly("0this1", "IO2", "Value3");
      assertThatTokens("0this1IO2V3", false).containsExactly("0this1", "IO2V3");
      assertThatTokens("0this1Is2A3Value4", false).containsExactly("0this1", "Is2", "A3", "Value4");
   }

   @Test
   public void tokensWhenUnicodeSurrogatePair() {
      assertThatTokens("\uD83D\uDE00This \uD83D\uDE00 value\uD83D\uDE00").containsExactly("This", "value");
      assertThatTokens("\uD83D\uDE00This IO\uD83D\uDE00 value\uD83D\uDE00").containsExactly("This",
                                                                                            "IO",
                                                                                            "value");
      assertThatTokens("\uD83D\uDE00This 3\uD83D\uDE00 value\uD83D\uDE00").containsExactly("This",
                                                                                           "3",
                                                                                           "value");
   }

   @Test
   public void tokensWhenIndexRange() {
      assertThatTokens("This value", 0, 10).containsExactly("This", "value");
      assertThatTokens("This value", 2, 7).containsExactly("is", "va");
      assertThatTokens("This value", 0, 0).isEmpty();
      assertThatTokens("This value", 9, 9).isEmpty();
      assertThatTokens("This value", 10, 10).isEmpty();
      assertThatTokens("This value", 4, 5).isEmpty();
      assertThatTokens("This value", 1, 1).isEmpty();
      assertThatTokens("ThisIOValue", 0, 6).containsExactly("This", "IO");
      assertThatTokens("ThisIOValue", 0, 7).containsExactly("This", "IOV");
      assertThatTokens("ThisIOValue", 0, 8).containsExactly("This", "IO", "Va");
   }

   @Test
   public void tokensWhenIndexRangeOutOfRange() {
      assertThatThrownBy(() -> assertThatTokens("This value", -1, 4))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'fromIndex=-1' must be positive");
      assertThatThrownBy(() -> assertThatTokens("This value", 20, 21))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'toIndex=21' must be less than or equal to 'input.length=10'");
      assertThatThrownBy(() -> assertThatTokens("This value", 5, 4))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'toIndex=4' must be greater than or equal to 'fromIndex=5'");
      assertThatThrownBy(() -> assertThatTokens("This value", 5, 20))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > 'toIndex=20' must be less than or equal to 'input.length=10'");
   }

   @SuppressWarnings("unchecked")
   private static ListAssert<String> assertThatTokens(String input) {
      return (ListAssert<String>) assertThat(IdentifierTokenizer
                                                   .ofDefault()
                                                   .tokens(input)).extracting(t -> t.subString(input));
   }

   @SuppressWarnings("unchecked")
   private static ListAssert<String> assertThatTokens(String input, int fromIndex, int toIndex) {
      return (ListAssert<String>) assertThat(IdentifierTokenizer
                                                   .ofDefault()
                                                   .tokens(input,
                                                           fromIndex,
                                                           toIndex)).extracting(t -> t.subString(input));
   }

   @SuppressWarnings("unchecked")
   private static ListAssert<String> assertThatTokens(String input, boolean splitNumbers) {
      return (ListAssert<String>) assertThat(IdentifierTokenizer
                                                   .of(splitNumbers)
                                                   .tokens(input)).extracting(t -> t.subString(input));
   }
}