/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.core.format;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.projection.core.format.formats.CapitalizeFormat;
import com.tinubu.projection.core.format.formats.HyphenateFormat;
import com.tinubu.projection.core.format.formats.LowerCamelCaseFormat;
import com.tinubu.projection.core.format.formats.LowerCaseFormat;
import com.tinubu.projection.core.format.formats.LowerKebabCaseFormat;
import com.tinubu.projection.core.format.formats.LowerSnakeCaseFormat;
import com.tinubu.projection.core.format.formats.LowerSpaceCaseFormat;
import com.tinubu.projection.core.format.formats.PackageFormat;
import com.tinubu.projection.core.format.formats.ReverseDotFormat;
import com.tinubu.projection.core.format.formats.UpperCamelCaseFormat;
import com.tinubu.projection.core.format.formats.UpperCaseFormat;
import com.tinubu.projection.core.format.formats.UpperKebabCaseFormat;
import com.tinubu.projection.core.format.formats.UpperSnakeCaseFormat;
import com.tinubu.projection.core.format.formats.WordCapitalizeFormat;
import com.tinubu.projection.core.format.formats.WordOnlyFormat;
import com.tinubu.projection.core.format.formats.WordSpaceFormat;

class FormatTest {

   private String format(Format format, String value) {
      return format.format(value);
   }

   @Test
   public void testLowerCase() {
      assertThat(format(new LowerCaseFormat(), "my-project")).isEqualTo("my-project");
      assertThat(format(new LowerCaseFormat(), "my project")).isEqualTo("my project");
      assertThat(format(new LowerCaseFormat(), "my_project")).isEqualTo("my_project");
      assertThat(format(new LowerCaseFormat(), "my.project")).isEqualTo("my.project");
      assertThat(format(new LowerCaseFormat(), "my!project")).isEqualTo("my!project");
      assertThat(format(new LowerCaseFormat(), "myProject")).isEqualTo("myproject");
      assertThat(format(new LowerCaseFormat(), "my._Project!")).isEqualTo("my._project!");
   }

   @Test
   public void testUpperCase() {
      assertThat(format(new UpperCaseFormat(), "my-project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperCaseFormat(), "my project")).isEqualTo("MY PROJECT");
      assertThat(format(new UpperCaseFormat(), "my_project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperCaseFormat(), "my.project")).isEqualTo("MY.PROJECT");
      assertThat(format(new UpperCaseFormat(), "my!project")).isEqualTo("MY!PROJECT");
      assertThat(format(new UpperCaseFormat(), "myProject")).isEqualTo("MYPROJECT");
      assertThat(format(new UpperCaseFormat(), "my._Project!")).isEqualTo("MY._PROJECT!");
   }

   @Test
   public void testCapitalize() {
      assertThat(format(new CapitalizeFormat(), "my-project")).isEqualTo("My-project");
      assertThat(format(new CapitalizeFormat(), "my project")).isEqualTo("My project");
      assertThat(format(new CapitalizeFormat(), "my Project")).isEqualTo("My Project");
      assertThat(format(new CapitalizeFormat(), "my_project")).isEqualTo("My_project");
      assertThat(format(new CapitalizeFormat(), "my.project")).isEqualTo("My.project");
      assertThat(format(new CapitalizeFormat(), "my!project")).isEqualTo("My!project");
      assertThat(format(new CapitalizeFormat(), "myProject")).isEqualTo("MyProject");
      assertThat(format(new CapitalizeFormat(), "my._Project!")).isEqualTo("My._Project!");
      assertThat(format(new CapitalizeFormat(), "!my._Project!")).isEqualTo("!My._Project!");
   }

   @Test
   public void testCapitalizeWhenLowercase() {
      assertThat(format(new CapitalizeFormat(true), "my-project")).isEqualTo("My-project");
      assertThat(format(new CapitalizeFormat(true), "my project")).isEqualTo("My project");
      assertThat(format(new CapitalizeFormat(true), "my Project")).isEqualTo("My project");
      assertThat(format(new CapitalizeFormat(true), "my_project")).isEqualTo("My_project");
      assertThat(format(new CapitalizeFormat(true), "my.project")).isEqualTo("My.project");
      assertThat(format(new CapitalizeFormat(true), "my!project")).isEqualTo("My!project");
      assertThat(format(new CapitalizeFormat(true), "myProject")).isEqualTo("Myproject");
      assertThat(format(new CapitalizeFormat(true), "my._Project!")).isEqualTo("My._project!");
      assertThat(format(new CapitalizeFormat(true), "!my._Project!")).isEqualTo("!My._project!");
   }

   @Test
   public void testWordCapitalize() {
      assertThat(format(new WordCapitalizeFormat(), "my-project")).isEqualTo("My-Project");
      assertThat(format(new WordCapitalizeFormat(), "my project")).isEqualTo("My Project");
      assertThat(format(new WordCapitalizeFormat(), "my ProjecT")).isEqualTo("My Project");
      assertThat(format(new WordCapitalizeFormat(), "my_project")).isEqualTo("My_project");
      assertThat(format(new WordCapitalizeFormat(), "my.project")).isEqualTo("My.Project");
      assertThat(format(new WordCapitalizeFormat(), "my!project")).isEqualTo("My!Project");
      assertThat(format(new WordCapitalizeFormat(), "myProject")).isEqualTo("Myproject");
      assertThat(format(new WordCapitalizeFormat(), ".my._Project!")).isEqualTo(".My._project!");
      assertThat(format(new WordCapitalizeFormat(), "my.project!")).isEqualTo("My.Project!");
      assertThat(format(new WordCapitalizeFormat(), "my-project!")).isEqualTo("My-Project!");
      assertThat(format(new WordCapitalizeFormat(), "my_project!")).isEqualTo("My_project!");
      assertThat(format(new WordCapitalizeFormat(), "my!project!")).isEqualTo("My!Project!");
   }

   @Test
   public void testWordCapitalizeWhenNoLowerCase() {
      assertThat(format(new WordCapitalizeFormat(false), "my-project")).isEqualTo("My-Project");
      assertThat(format(new WordCapitalizeFormat(false), "my project")).isEqualTo("My Project");
      assertThat(format(new WordCapitalizeFormat(false), "my ProjecT")).isEqualTo("My ProjecT");
      assertThat(format(new WordCapitalizeFormat(false), "my_project")).isEqualTo("My_project");
      assertThat(format(new WordCapitalizeFormat(false), "my.project")).isEqualTo("My.Project");
      assertThat(format(new WordCapitalizeFormat(false), "my!project")).isEqualTo("My!Project");
      assertThat(format(new WordCapitalizeFormat(false), "myProject")).isEqualTo("MyProject");
      assertThat(format(new WordCapitalizeFormat(false), ".my._Project!")).isEqualTo(".My._Project!");
      assertThat(format(new WordCapitalizeFormat(false), "my.project!")).isEqualTo("My.Project!");
      assertThat(format(new WordCapitalizeFormat(false), "my-project!")).isEqualTo("My-Project!");
      assertThat(format(new WordCapitalizeFormat(false), "my_project!")).isEqualTo("My_project!");
      assertThat(format(new WordCapitalizeFormat(false), "my!project!")).isEqualTo("My!Project!");
   }

   @Test
   public void testLowerCamlCase() {
      assertThat(format(new LowerCamelCaseFormat(), "my-project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my_project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my.project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "my!project")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "myProject")).isEqualTo("myProject");
      assertThat(format(new LowerCamelCaseFormat(), "!my._Project!")).isEqualTo("myProject");
   }

   @Test
   public void testUpperCamlCase() {
      assertThat(format(new UpperCamelCaseFormat(), "my-project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my_project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my.project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "my!project")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "myProject")).isEqualTo("MyProject");
      assertThat(format(new UpperCamelCaseFormat(), "!my._Project!")).isEqualTo("MyProject");
   }

   @Test
   public void testLowerSnakeCase() {
      assertThat(format(new LowerSnakeCaseFormat(), "my-project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my_project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my.project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "my!project")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "myProject")).isEqualTo("my_project");
      assertThat(format(new LowerSnakeCaseFormat(), "!my._Project!")).isEqualTo("my_project");
   }

   @Test
   public void testUpperSnakeCase() {
      assertThat(format(new UpperSnakeCaseFormat(), "my-project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my_project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my.project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "my!project")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "myProject")).isEqualTo("MY_PROJECT");
      assertThat(format(new UpperSnakeCaseFormat(), "!my._Project!")).isEqualTo("MY_PROJECT");
   }

   @Test
   public void testLowerKebabCase() {
      assertThat(format(new LowerKebabCaseFormat(), "my-project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my_project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my.project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "my!project")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "myProject")).isEqualTo("my-project");
      assertThat(format(new LowerKebabCaseFormat(), "!my._Project!")).isEqualTo("my-project");
   }

   @Test
   public void testUpperKebabCase() {
      assertThat(format(new UpperKebabCaseFormat(), "my-project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my_project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my.project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "my!project")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "myProject")).isEqualTo("MY-PROJECT");
      assertThat(format(new UpperKebabCaseFormat(), "!my._Project!")).isEqualTo("MY-PROJECT");
   }

   @Test
   public void testLowerSpaceCase() {
      assertThat(format(new LowerSpaceCaseFormat(), "my-project")).isEqualTo("my project");
      assertThat(format(new LowerSpaceCaseFormat(), "my project")).isEqualTo("my project");
      assertThat(format(new LowerSpaceCaseFormat(), "my_project")).isEqualTo("my project");
      assertThat(format(new LowerSpaceCaseFormat(), "my.project")).isEqualTo("my project");
      assertThat(format(new LowerSpaceCaseFormat(), "my!project")).isEqualTo("my project");
      assertThat(format(new LowerSpaceCaseFormat(), "myProject")).isEqualTo("my project");
      assertThat(format(new LowerSpaceCaseFormat(), "!my._Project!")).isEqualTo("my project");
   }

   @Test
   public void testLowerSpaceCaseWhenCapitalize() {
      assertThat(format(new LowerSpaceCaseFormat(true, false), "my-project")).isEqualTo("My project");
      assertThat(format(new LowerSpaceCaseFormat(true, false), "my project")).isEqualTo("My project");
      assertThat(format(new LowerSpaceCaseFormat(true, false), "my_project")).isEqualTo("My project");
      assertThat(format(new LowerSpaceCaseFormat(true, false), "my.project")).isEqualTo("My project");
      assertThat(format(new LowerSpaceCaseFormat(true, false), "my!project")).isEqualTo("My project");
      assertThat(format(new LowerSpaceCaseFormat(true, false), "myProject")).isEqualTo("My project");
      assertThat(format(new LowerSpaceCaseFormat(true, false), "!my._Project!")).isEqualTo("My project");
   }

   @Test
   public void testLowerSpaceCaseWhenWordCapitalize() {
      assertThat(format(new LowerSpaceCaseFormat(false, true), "my-project")).isEqualTo("My Project");

      assertThat(format(new LowerSpaceCaseFormat(true, true), "my-project")).isEqualTo("My Project");
      assertThat(format(new LowerSpaceCaseFormat(true, true), "my project")).isEqualTo("My Project");
      assertThat(format(new LowerSpaceCaseFormat(true, true), "my_project")).isEqualTo("My Project");
      assertThat(format(new LowerSpaceCaseFormat(true, true), "my.project")).isEqualTo("My Project");
      assertThat(format(new LowerSpaceCaseFormat(true, true), "my!project")).isEqualTo("My Project");
      assertThat(format(new LowerSpaceCaseFormat(true, true), "myProject")).isEqualTo("My Project");
      assertThat(format(new LowerSpaceCaseFormat(true, true), "!my._Project!")).isEqualTo("My Project");
   }

   @Test
   public void testHyphenate() {
      assertThat(format(HyphenateFormat.ofStrict(), "my-project")).isEqualTo("my-project");
      assertThat(format(HyphenateFormat.ofStrict(), "my project")).isEqualTo("my-project");
      assertThat(format(HyphenateFormat.ofStrict(), "my_project")).isEqualTo("my_project");
      assertThat(format(HyphenateFormat.ofStrict(), "my.project")).isEqualTo("my.project");
      assertThat(format(HyphenateFormat.ofStrict(), "my!project")).isEqualTo("my!project");
      assertThat(format(HyphenateFormat.ofStrict(), "myProject")).isEqualTo("myProject");
      assertThat(format(HyphenateFormat.ofStrict(), "!my._Project!")).isEqualTo("!my._Project!");
   }

   @Test
   public void testHyphenateWhenBlankSequence() {
      assertThat(format(HyphenateFormat.ofStrict(), " my    project ")).isEqualTo("my-project");
      assertThat(format(HyphenateFormat.of(" ", true, true), "-my \t project-")).isEqualTo("my-\t-project");
   }

   @Test
   public void testHyphenateWhenStrip() {
      assertThat(format(HyphenateFormat.ofStrict(), " my project ")).isEqualTo("my-project");
      assertThat(format(HyphenateFormat.ofStrict(), "-my project-")).isEqualTo("my-project");
   }

   @Test
   public void testHyphenateWhenNotStripStart() {
      assertThat(format(HyphenateFormat.of(" ", false, true), " my project ")).isEqualTo("-my-project");
      assertThat(format(HyphenateFormat.of(" ", false, true), "-my project-")).isEqualTo("-my-project");
   }

   @Test
   public void testHyphenateWhenNotStripEnd() {
      assertThat(format(HyphenateFormat.of(" ", true, false), " my project ")).isEqualTo("my-project-");
      assertThat(format(HyphenateFormat.of(" ", true, false), "-my project-")).isEqualTo("my-project-");
   }

   @Test
   public void testWordOnly() {
      assertThat(format(new WordOnlyFormat(), "my-project")).isEqualTo("myproject");
      assertThat(format(new WordOnlyFormat(), "my project")).isEqualTo("myproject");
      assertThat(format(new WordOnlyFormat(), "my Project")).isEqualTo("myProject");
      assertThat(format(new WordOnlyFormat(), "my_project")).isEqualTo("my_project");
      assertThat(format(new WordOnlyFormat(), "my.project")).isEqualTo("myproject");
      assertThat(format(new WordOnlyFormat(), "my!project")).isEqualTo("myproject");
      assertThat(format(new WordOnlyFormat(), "myProject")).isEqualTo("myProject");
      assertThat(format(new WordOnlyFormat(), "my._Project!")).isEqualTo("my_Project");
   }

   @Test
   public void testWordSpace() {
      assertThat(format(new WordSpaceFormat(), "my-project")).isEqualTo("my project");
      assertThat(format(new WordSpaceFormat(), "my project")).isEqualTo("my project");
      assertThat(format(new WordSpaceFormat(), "my Project")).isEqualTo("my Project");
      assertThat(format(new WordSpaceFormat(), "my_project")).isEqualTo("my_project");
      assertThat(format(new WordSpaceFormat(), "my.project")).isEqualTo("my project");
      assertThat(format(new WordSpaceFormat(), "my!project")).isEqualTo("my project");
      assertThat(format(new WordSpaceFormat(), "myProject")).isEqualTo("myProject");
      assertThat(format(new WordSpaceFormat(), "my._Project!")).isEqualTo("my _Project");
   }

   @Test
   public void testPackage() {
      assertThat(format(new PackageFormat(), "my-project")).isEqualTo("my-project");
      assertThat(format(new PackageFormat(), "my.project.path")).isEqualTo("my/project/path");
      assertThat(format(new PackageFormat(), "my.project/path")).isEqualTo("my/project/path");
      assertThat(format(new PackageFormat(), ".my.project.path.")).isEqualTo("/my/project/path/");
      assertThat(format(new PackageFormat(), "/my.project.path/")).isEqualTo("/my/project/path/");
   }

   @Test
   public void testPackageWhenAlternativeSeparator() {
      assertThat(format(new PackageFormat("\\"), "my-project")).isEqualTo("my-project");
      assertThat(format(new PackageFormat("\\"), "my.project.path")).isEqualTo("my\\project\\path");
      assertThat(format(new PackageFormat("\\"), "my.project/path")).isEqualTo("my\\project/path");
      assertThat(format(new PackageFormat("\\"), ".my.project.path.")).isEqualTo("\\my\\project\\path\\");
      assertThat(format(new PackageFormat("\\"), "/my.project.path/")).isEqualTo("/my\\project\\path/");
   }

   @Test
   public void testReverseDot() {
      assertThat(format(new ReverseDotFormat(), "my-project")).isEqualTo("my-project");
      assertThat(format(new ReverseDotFormat(), "my.project.path")).isEqualTo("path.project.my");
      assertThat(format(new ReverseDotFormat(), "my.project/path")).isEqualTo("project/path.my");
      assertThat(format(new ReverseDotFormat(), ".my.project.path.")).isEqualTo(".path.project.my.");
      assertThat(format(new ReverseDotFormat(), "/my.project.path/")).isEqualTo("path/.project./my");
   }

}