#!/bin/bash

#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# Projection launch script

test -n "$DEBUG" && set -x

WORKING_DIR="$(pwd)"
test -n "$JARFILE" && jarfile="$JARFILE"

# Follow symlinks to find the real jar and detect init.d script
cd "$(dirname "$0")" || exit 1
test -z "$jarfile" && jarfile=$(pwd)/$(basename "$0")
while test -L "$jarfile"; do
  configfile="${jarfile%.*}.conf"
  # shellcheck source=/dev/null
  test -r ${configfile} && . "${configfile}"
  jarfile=$(readlink "$jarfile")
  cd "$(dirname "$jarfile")" || exit 1
  jarfile=$(pwd)/$(basename "$jarfile")
done
cd "$WORKING_DIR" || exit 1

# Find Java
if test -n "$JAVA_HOME" && test -x "$JAVA_HOME/bin/java"; then
  javaexe="$JAVA_HOME/bin/java"
elif type -p java >/dev/null 2>&1; then
  javaexe=$(type -p java)
elif test -x "/usr/bin/java"; then
  javaexe="/usr/bin/java"
else
  echo "Unable to find Java"
  exit 1
fi

arguments=(-Dsun.misc.URLClassPath.disableJarChecking=true $JAVA_OPTS -jar "$jarfile" "$@")

"$javaexe" "${arguments[@]}"
exit $?
