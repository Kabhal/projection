<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Licensed to the Apache Software Foundation (ASF) under one
  ~ or more contributor license agreements.  See the NOTICE file
  ~ distributed with this work for additional information
  ~ regarding copyright ownership.  The ASF licenses this file
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>

   <parent>
      <groupId>com.tinubu.projection</groupId>
      <artifactId>projection</artifactId>
      <version>1.0.0-SNAPSHOT</version>
   </parent>

   <artifactId>projection-cli</artifactId>

   <properties>
      <skipNativeBuild>false</skipNativeBuild>
      <main.class>com.tinubu.projection.cli.ProjectionCli</main.class>
      <final.name>projection-${project.version}</final.name>
   </properties>

   <dependencies>

      <!-- Internal -->

      <dependency>
         <groupId>com.tinubu.projection</groupId>
         <artifactId>projection-core</artifactId>
         <version>1.0.0-SNAPSHOT</version>
      </dependency>

      <!-- External -->

      <dependency>
         <groupId>org.slf4j</groupId>
         <artifactId>slf4j-api</artifactId>
      </dependency>
      <dependency>
         <groupId>com.tinubu.commons-lang</groupId>
         <artifactId>commons-lang</artifactId>
      </dependency>
      <dependency>
         <groupId>org.apache.commons</groupId>
         <artifactId>commons-lang3</artifactId>
      </dependency>
      <dependency>
         <groupId>info.picocli</groupId>
         <artifactId>picocli</artifactId>
      </dependency>

      <!-- Runtime -->

      <dependency>
         <groupId>ch.qos.logback</groupId>
         <artifactId>logback-classic</artifactId>
         <scope>compile</scope> <!-- compile scope required to programmatically set logback level -->
      </dependency>

      <!-- Tests -->

      <dependency>
         <groupId>org.junit.jupiter</groupId>
         <artifactId>junit-jupiter-api</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.junit.jupiter</groupId>
         <artifactId>junit-jupiter-engine</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.junit.jupiter</groupId>
         <artifactId>junit-jupiter-params</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.assertj</groupId>
         <artifactId>assertj-core</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.mockito</groupId>
         <artifactId>mockito-junit-jupiter</artifactId>
         <scope>test</scope>
      </dependency>

   </dependencies>

   <build>
      <finalName>${final.name}</finalName>
      <plugins>
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <configuration>
               <archive>
                  <manifest>
                     <mainClass>${main.class}</mainClass>
                     <addClasspath>true</addClasspath>
                  </manifest>
               </archive>
            </configuration>
         </plugin>
         <plugin>
            <groupId>org.graalvm.buildtools</groupId>
            <artifactId>native-maven-plugin</artifactId>
            <configuration>
               <imageName>${final.name}</imageName>
            </configuration>
         </plugin>

      </plugins>
   </build>

   <profiles>
      <!-- Generate an executable JAR using SB plugin -->
      <profile>
         <id>projection-executable-jar</id>
         <activation>
            <activeByDefault>true</activeByDefault>
         </activation>
         <build>
            <plugins>
               <plugin>
                  <groupId>org.springframework.boot</groupId>
                  <artifactId>spring-boot-maven-plugin</artifactId>
                  <configuration>
                     <!-- Override the default run script which sadly change the working directory to JAR installation directory -->
                     <embeddedLaunchScript>projection.sh</embeddedLaunchScript>
                  </configuration>
               </plugin>
            </plugins>
         </build>
      </profile>
      <!-- Generate a native image using GraalVM native -->
      <profile>
         <id>projection-native-image</id>
         <activation>
            <property>
               <name>enableNativeBuild</name>
               <value>true</value>
            </property>
         </activation>
         <build>
            <plugins>
               <plugin>
                  <groupId>org.apache.maven.plugins</groupId>
                  <artifactId>maven-compiler-plugin</artifactId>
                  <configuration>
                     <annotationProcessorPaths>
                        <annotationProcessorPath>
                           <groupId>info.picocli</groupId>
                           <artifactId>picocli-codegen</artifactId>
                           <version>${picocli.version}</version>
                        </annotationProcessorPath>
                     </annotationProcessorPaths>
                     <annotationProcessors>
                        <annotationProcessor>picocli.codegen.aot.graalvm.processor.NativeImageConfigGeneratorProcessor</annotationProcessor>
                     </annotationProcessors>
                  </configuration>
               </plugin>
            </plugins>
         </build>
      </profile>
   </profiles>

</project>