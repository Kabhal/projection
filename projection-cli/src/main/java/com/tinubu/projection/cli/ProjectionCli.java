/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.projection.cli;

import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder.equal;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder.in;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder.lesserOrEqual;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder.startWith;
import static com.tinubu.commons.lang.log.ExtendedLogger.lazy;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.PathUtils.isEmpty;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.DocumentEntrySpecification.allDocuments;
import static com.tinubu.commons.ports.document.domain.DocumentEntrySpecification.noneDocuments;
import static java.lang.String.format;
import static java.lang.String.join;
import static java.lang.System.exit;
import static java.util.stream.Collectors.joining;
import static picocli.CommandLine.Command;
import static picocli.CommandLine.Model.CommandSpec;
import static picocli.CommandLine.Option.NULL_VALUE;
import static picocli.CommandLine.ParentCommand;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.projection.cli.ProjectionCli.FormatSubCommand;
import com.tinubu.projection.cli.ProjectionCli.SynchronizeSubCommand;
import com.tinubu.projection.cli.ProjectionCli.UpdateSubCommand;
import com.tinubu.projection.core.Attribute;
import com.tinubu.projection.core.PlaceholderFormat;
import com.tinubu.projection.core.Projection;
import com.tinubu.projection.core.Projection.ProjectionSynchronize;
import com.tinubu.projection.core.Projection.ProjectionUpdate;
import com.tinubu.projection.core.format.AliasFormat;
import com.tinubu.projection.core.format.Format;
import com.tinubu.projection.core.format.ServiceLoaderFormatFactory;
import com.tinubu.projection.core.strategy.ServiceLoaderSynchronizeStrategyFactory;

import picocli.CommandLine;
import picocli.CommandLine.Help.Ansi.Style;
import picocli.CommandLine.Help.ColorScheme;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Spec;

@Command(name = "projection", description = "Project template synchronization", version = {
      "projection ${git.build.version:--} (${git.build.time:--})",
      "Git: ${git.commit.id.abbrev:--} (${git.commit.time:--})",
      "JVM: ${java.version} (${java.vendor} ${java.vm.name} ${java.vm.version})" },
         mixinStandardHelpOptions = true, sortOptions = false, parameterListHeading = "\n",
         optionListHeading = "\n", commandListHeading = "%nCommands:%n",
         subcommands = { SynchronizeSubCommand.class, UpdateSubCommand.class, FormatSubCommand.class })
public class ProjectionCli implements Runnable {

   private static final Logger log = LoggerFactory.getLogger(ProjectionCli.class);

   private static final int USAGE_HELP_WIDTH = 120;
   /** Projection application common roots for all logs. */
   private static final List<String> LOG_ROOTS =
         list("com.tinubu.projection", "com.tinubu.commons.lang.io.replacer");

   static {
      ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
   }

   @Spec
   CommandSpec spec;

   public static void main(String[] args) {
      exit(new ProjectionCli().commandLine(args));
   }

   public int commandLine(String[] args) {
      loadGitProperties();

      CommandLine commandLine = new CommandLine(this);
      commandLine.setToggleBooleanFlags(false);
      commandLine.setCaseInsensitiveEnumValuesAllowed(true);
      commandLine.setUsageHelpWidth(USAGE_HELP_WIDTH);
      commandLine.setColorScheme(colorScheme());

      return commandLine.execute(args);
   }

   @Override
   public void run() {
      spec.commandLine().usage(System.out);
   }

   /**
    * Defines a restricting document criteria from specified specifiers :
    * <ul>
    *    <li>{@code document} : a document name</li>
    *    <li>{@code .} : the project subtree</li>
    *    <li>{@code ./[0-9]} : the project subtree but limited to the specified depth level</li>
    *    <li>{@code directory/} : a document contained in directory subtree</li>
    *    <li>{@code directory/[0-9]} : a document contained in directory subtree but limited to the specified depth level from directory</li>
    * </ul>
    */
   protected static DocumentEntrySpecification restrictDocumentsSpecification(List<String> documents) {
      var depthPattern = Pattern.compile("(.*)/([0-9]+)");
      return nullable(documents).filter(docs -> !docs.isEmpty()).map(docs -> {
         DocumentEntrySpecification specification = noneDocuments();

         var regularDocuments = CollectionUtils.<Path>list();
         for (String doc : documents) {
            if (doc.equals(".")) {
               return allDocuments();
            } else if (doc.endsWith("/")) {
               specification = specification.or(new DocumentEntryCriteriaBuilder()
                                                      .documentPath(startWith(Path.of(doc).normalize()))
                                                      .build());
            } else if (doc.endsWith("/.")) {
               specification = specification.or(new DocumentEntryCriteriaBuilder()
                                                      .documentParentPath(equal(Path.of(doc).normalize()))
                                                      .build());
            } else {
               var depthMatcher = depthPattern.matcher(doc);

               if (depthMatcher.matches()) {
                  var directory = Path.of(depthMatcher.group(1)).normalize();
                  var depth = depthMatcher.group(2);

                  if (isEmpty(directory)) {
                     specification = specification.or(new DocumentEntryCriteriaBuilder()
                                                            .documentPathDepth(lesserOrEqual(Integer.parseInt(
                                                                  depth)))
                                                            .build());
                  } else {
                     var adaptedDepth = directory.getNameCount() + Integer.parseInt(depth);
                     specification = specification.or(new DocumentEntryCriteriaBuilder()
                                                            .documentPath(startWith(directory))
                                                            .documentPathDepth(lesserOrEqual(adaptedDepth))
                                                            .build());
                  }
               } else {
                  regularDocuments.add(Path.of(doc).normalize());
               }
            }
         }

         if (!regularDocuments.isEmpty()) {
            specification = specification.or(new DocumentEntryCriteriaBuilder()
                                                   .documentPath(in(regularDocuments))
                                                   .build());
         }

         log.info("Restrict synchronization to {}", lazy(() -> join(" ∪ ", documents)));

         return specification;
      }).orElse(null);
   }

   @Command(name = "synchronize", aliases = { "sync" }, description = "Synchronize project from template",
            mixinStandardHelpOptions = true, sortOptions = false, parameterListHeading = "\n",
            optionListHeading = "\n")
   public static class SynchronizeSubCommand implements Runnable {

      @ParentCommand
      ProjectionCli projection;

      @Parameters(description = "Project URI [${DEFAULT-VALUE}]", paramLabel = "<project URI>",
                  defaultValue = ".")
      private URI projectUri;

      @Parameters(
            description = "Restrict to 'path/document', or '.', or 'subtree/' (paths are relative to project root; limit path depth using './[0-9]', or 'subtree/[0-9]' syntax)",
            paramLabel = "<path>", index = "1..*", defaultValue = ".")
      private List<String> restrictPaths;

      @Option(names = { "-t", "--template" }, description = "Template URI [<project-config>]",
              paramLabel = "<URI>")
      private URI templateUri;

      @Option(names = { "--strategy" }, description = "Synchronize strategy [${DEFAULT-VALUE}]",
              paramLabel = "<strategy>", defaultValue = "direct")
      private String synchronizeStrategy;

      @Option(names = { "--include-git" },
              description = "Include template .git/ directory in synchronization [<project-config>|false]",
              defaultValue = NULL_VALUE)
      private Boolean includeGit;

      @Option(names = { "--no-placeholders" }, description = "Disable placeholders replacement [false]",
              defaultValue = "false", fallbackValue = "true")
      private boolean noPlaceholders;

      @Option(names = { "--infer-placeholders" },
              description = "Replace template attribute values with inferred placeholders [<project-config>|false]",
              defaultValue = NULL_VALUE)
      private Boolean inferPlaceholders;

      @Option(names = { "--placeholders-format", "--pf" },
              description = "Placeholders format tokens configuration [<project-config>|cb=$__,cs=_,ce=__,pb=__,ps=_,pe=__]",
              paramLabel = "<token-key>=<token-value>")
      private Map<String, String> placeholdersFormat = map();

      @Option(names = { "-A" },
              description = "Template attributes. Required attributes are suffixed with * [<template-config>]",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> templateAttributes = map(LinkedHashMap::new);

      @Option(names = { "-a" },
              description = "Project attributes. Required attributes are suffixed with * [<project-config>]",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> projectAttributes = map(LinkedHashMap::new);

      @Option(names = { "-F", "--format" },
              description = "Restrict format names to use | add extra alias formats as a compound of one or more formats [<project-config>]",
              paramLabel = "<name> | <name1>[;<name2>]=<format>[|<format2>]", mapFallbackValue = NULL_VALUE,
              split = "\s*,\s*", splitSynopsisLabel = ",")
      private Map<String, String> formats = map(LinkedHashMap::new);

      @Option(names = { "-p" },
              description = "Global properties. Sensitive properties can be suffixed with ! [<project-config>]",
              paramLabel = "<name>[!]=<value>", mapFallbackValue = "true")
      private Map<String, String> properties = map(LinkedHashMap::new);

      @Option(names = { "-O", "--overwrite" }, description = "Overwrite documents [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean overwrite;

      @Option(names = { "--generate-configuration" },
              description = "Generate projection configuration [${DEFAULT-VALUE}]", defaultValue = "true",
              fallbackValue = "true")
      private boolean generateProjectionConfiguration;

      @Option(names = { "--overwrite-configuration" },
              description = "Overwrite projection configuration with specified parameters [${DEFAULT-VALUE}]",
              defaultValue = "false", fallbackValue = "true")
      private boolean overwriteProjectionConfiguration;

      @Option(names = { "--generate-metadata" },
              description = "Generate projection metadata [${DEFAULT-VALUE}]", defaultValue = "true",
              fallbackValue = "true")
      private boolean generateProjectionMetadata;

      @Option(names = { "--fail-on-warning" }, description = "Fail on warning [${DEFAULT-VALUE}]",
              defaultValue = "false", fallbackValue = "true")
      private boolean failOnWarning;

      @Option(names = { "-v", "--verbose" },
              description = "Verbose level (off|error|warn|info|debug|trace) [${DEFAULT-VALUE}]",
              defaultValue = "warn", arity = "0..1", paramLabel = "<level>", fallbackValue = "info")
      private ExtendedLogger.Level verbose;

      @Override
      public void run() {
         verboseLevel(verbose);

         var formatLoader = new FormatLoader(this.formats);

         try {
            Projection projection = new Projection(formatLoader.registeredFormats())
                  .extraFormats(formatLoader.extraFormats())
                  .restrictedFormats(formatLoader.restrictedFormatNames())
                  .failOnWarning(failOnWarning)
                  .properties(properties);

            try (ProjectionSynchronize synchronize = projection.synchronize()) {
               synchronize
                     .projectRepository(projectUri)
                     .templateAttributes(attributes(templateAttributes))
                     .projectAttributes(attributes(projectAttributes))
                     .overwrite(overwrite)
                     .noPlaceholders(noPlaceholders)
                     .placeholderFormat(PlaceholderFormat
                                              .builder()
                                              .ofPropertyMap(null, placeholdersFormat)
                                              .build())
                     .inferPlaceholders(inferPlaceholders)
                     .excludeGit(nullable(includeGit, v -> !v))
                     .generateProjectionConfiguration(generateProjectionConfiguration)
                     .overwriteProjectionConfiguration(overwriteProjectionConfiguration)
                     .generateProjectionMetadata(generateProjectionMetadata)
                     .synchronizeStrategyFactory(new ServiceLoaderSynchronizeStrategyFactory(
                           synchronizeStrategy));
               if (templateUri != null) {
                  synchronize.templateRepository(templateUri);
               }

               synchronize.synchronize(restrictDocumentsSpecification(restrictPaths));
            }
         } catch (Exception e) {
            ProjectionCli.generalError(e);
         }
      }

   }

   @Command(name = "update", description = "Update template from project selected documents",
            mixinStandardHelpOptions = true, sortOptions = false, parameterListHeading = "\n",
            optionListHeading = "\n")
   public static class UpdateSubCommand implements Runnable {

      @ParentCommand
      ProjectionCli projection;

      @Parameters(description = "Project URI [${DEFAULT-VALUE}]", paramLabel = "<project URI>",
                  defaultValue = ".", index = "0")
      private URI projectUri;

      @Parameters(
            description = "Restrict to 'path/document', or '.', or 'subtree/' (paths are relative to project root; limit path depth using './[0-9]', or 'subtree/[0-9]' syntax)",
            paramLabel = "<path>", index = "1..*", defaultValue = ".")
      private List<String> restrictPaths;

      @Option(names = { "-t", "--template" }, description = "Template URI [<project-config>]",
              paramLabel = "<URI>")
      private URI templateUri;

      @Option(names = { "--strategy" }, description = "Synchronize strategy [${DEFAULT-VALUE}]",
              paramLabel = "<strategy>", defaultValue = "direct")
      private String synchronizeStrategy;

      @Option(names = { "--include-git" },
              description = "Include template .git/ directory in synchronization [<project-config>|false]",
              defaultValue = NULL_VALUE)
      private Boolean includeGit;

      @Option(names = { "--no-placeholders" }, description = "Disable placeholders replacement [false]",
              defaultValue = "false", fallbackValue = "true")
      private boolean noPlaceholders;

      @Option(names = { "--infer-placeholders" },
              description = "Replace template attribute values with inferred placeholders [<project-config>|false]",
              defaultValue = NULL_VALUE)
      private Boolean inferPlaceholders;

      @Option(names = { "--placeholders-format", "--pf" },
              description = "Placeholders format tokens configuration [<project-config>|cb=$__,cs=_,ce=__,pb=__,ps=_,pe=__]",
              paramLabel = "<token-key>=<token-value>")
      private Map<String, String> placeholdersFormat = map();

      @Option(names = { "-A" },
              description = "Template attributes. Required attributes are suffixed with * [<template-config>]",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> templateAttributes = map(LinkedHashMap::new);

      @Option(names = { "-a" },
              description = "Project attributes. Required attributes are suffixed with * [<project-config>]",
              paramLabel = "<name>[*]=<value>")
      private Map<String, String> projectAttributes = map(LinkedHashMap::new);

      @Option(names = { "-F", "--format" },
              description = "Restrict format names to use, and/or add extra alias formats as a compound of one or more formats [<project-config>]",
              paramLabel = "<name> | <name1>[;<name2>]=<format>[|<format2>]", mapFallbackValue = NULL_VALUE,
              split = "\s*,\s*", splitSynopsisLabel = ",")
      private Map<String, String> formats = map(LinkedHashMap::new);

      @Option(names = { "-p" },
              description = "Global properties. Sensitive properties can be suffixed with ! [<project-config>]",
              paramLabel = "<name>[!]=<value>", mapFallbackValue = "true")
      private Map<String, String> properties = map(LinkedHashMap::new);

      @Option(names = { "--missing-documents", "--missing" },
              description = "Create missing documents in target template repository, by default only existing documents are updated [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean createMissingDocuments;

      @Option(names = { "-O", "--overwrite" }, description = "Overwrite documents [${DEFAULT-VALUE}]",
              defaultValue = "false")
      private boolean overwrite;

      @Option(names = { "--fail-on-warning" }, description = "Fail on warning [${DEFAULT-VALUE}]",
              defaultValue = "false", fallbackValue = "true")
      private boolean failOnWarning;

      @Option(names = { "-v", "--verbose" },
              description = "Verbose level (off|error|warn|info|debug|trace) [${DEFAULT-VALUE}]",
              defaultValue = "warn", arity = "0..1", paramLabel = "<level>", fallbackValue = "info")
      private ExtendedLogger.Level verbose;

      @Override
      public void run() {
         verboseLevel(verbose);

         var formatLoader = new FormatLoader(this.formats);

         try {
            Projection projection = new Projection(formatLoader.registeredFormats())
                  .extraFormats(formatLoader.extraFormats())
                  .restrictedFormats(formatLoader.restrictedFormatNames())
                  .failOnWarning(failOnWarning)
                  .properties(properties);

            try (ProjectionUpdate update = projection.update()) {
               update
                     .projectRepository(projectUri)
                     .templateAttributes(attributes(templateAttributes))
                     .projectAttributes(attributes(projectAttributes))
                     .overwrite(overwrite)
                     .noPlaceholders(noPlaceholders)
                     .placeholderFormat(PlaceholderFormat
                                              .builder()
                                              .ofPropertyMap(null, placeholdersFormat)
                                              .build())
                     .inferPlaceholders(inferPlaceholders)
                     .excludeGit(nullable(includeGit, v -> !v))
                     .createMissingDocuments(createMissingDocuments)
                     .synchronizeStrategyFactory(new ServiceLoaderSynchronizeStrategyFactory(
                           synchronizeStrategy));

               if (templateUri != null) {
                  update.templateRepository(templateUri);
               }

               update.update(restrictDocumentsSpecification(restrictPaths));
            }
         } catch (Exception e) {
            ProjectionCli.generalError(e);
         }
      }

   }

   @Command(name = "format", description = "Describe available formats", mixinStandardHelpOptions = true,
            sortOptions = false, parameterListHeading = "\n", optionListHeading = "\n")
   public static class FormatSubCommand implements Runnable {

      @ParentCommand
      ProjectionCli projection;

      @Option(names = { "-a" }, description = "Sample attributes to format", paramLabel = "<name>=<value>")
      private Map<String, String> attributes = map(LinkedHashMap::new);

      @Option(names = { "-F", "--format" },
              description = "Restrict format names to use, and/or add extra alias formats as a compound of one or more formats",
              paramLabel = "<name> | <name1>[;<name2>]=<format>[|<format2>]", mapFallbackValue = NULL_VALUE,
              split = "\s*,\s*", splitSynopsisLabel = ",")
      private Map<String, String> formats = map(LinkedHashMap::new);

      @Option(names = { "-v", "--verbose" },
              description = "Verbose level (off|error|warn|info|debug|trace) [${DEFAULT-VALUE}]",
              defaultValue = "warn", arity = "0..1", paramLabel = "<level>", fallbackValue = "info")
      private ExtendedLogger.Level verbose;

      @Override
      public void run() {
         verboseLevel(verbose);

         var formatLoader = new FormatLoader(this.formats);

         try {
            System.out.println("Formats :\n");
            stream(formatLoader.formats())
                  .map(format -> format("  Use '%s' format [%s], names: %s",
                                        format.name(),
                                        format.description(),
                                        stream(format.names()).collect(joining(", "))))
                  .forEach(System.out::println);

            if (!attributes.isEmpty()) {
               System.out.println();
               System.out.println("Attributes formatting :");
               attributes.forEach((name, value) -> {
                  System.out.println();
                  stream(formatLoader.formats())
                        .map(format -> format("  Attribute '%s' | %s ('%s') = '%s'",
                                              name,
                                              format.name(),
                                              value,
                                              format.format(value)))
                        .forEach(System.out::println);

               });
            }
         } catch (Exception e) {
            ProjectionCli.generalError(e);
         }
      }

   }

   private static class FormatLoader {
      private final List<Format> registeredFormats;
      private final Map<String, Format> indexedRegisteredFormats;
      private final List<AliasFormat> extraFormats;
      private final List<Format> restrictedFormats;

      public FormatLoader(List<Format> registeredFormats, Map<String, String> formatSpecs) {
         this.registeredFormats = registeredFormats;
         this.indexedRegisteredFormats = indexedFormats(registeredFormats);
         this.extraFormats = extraFormats(indexedRegisteredFormats, formatSpecs);
         this.restrictedFormats = restrictedFormats(indexedRegisteredFormats, formatSpecs);
      }

      private static LinkedHashMap<String, Format> indexedFormats(List<Format> registeredFormats) {
         return map(LinkedHashMap::new,
                    stream(registeredFormats).flatMap(f -> stream(f.names()).map(name -> entry(name, f))));
      }

      public FormatLoader(Map<String, String> formatSpecs) {
         this(new ServiceLoaderFormatFactory().formats(), formatSpecs);
      }

      public List<Format> registeredFormats() {
         return registeredFormats;
      }

      public Map<String, Format> indexedRegisteredFormats() {
         return indexedRegisteredFormats;
      }

      public List<AliasFormat> extraFormats() {
         return extraFormats;
      }

      public List<Format> restrictedFormats() {
         return restrictedFormats;
      }

      public List<String> restrictedFormatNames() {
         return list(restrictedFormats.stream().map(Format::name));
      }

      public List<Format> formats() {
         return listConcat(restrictedFormats.isEmpty() ? registeredFormats : restrictedFormats, extraFormats);
      }

      /**
       * Creates the extra format list to use from CLI format specification.
       *
       * @param registeredFormats registered formats indexed by {@link Format#names()} (including
       *       aliases)
       * @param formatSpecs format specification. Map value is {@code null} if format is not an alias
       *
       * @return extra alias format list, possibly empty
       */
      private static List<AliasFormat> extraFormats(final Map<String, Format> registeredFormats,
                                                    final Map<String, String> formatSpecs) {
         List<AliasFormat> extraFormats = list();
         for (Entry<String, String> formatSpec : formatSpecs.entrySet()) {
            if (formatSpec.getValue() != null) {
               if (formatSpec.getValue().isEmpty()) {
                  throw new IllegalStateException(format("Missing formats for '%s' alias",
                                                         formatSpec.getKey()));
               }

               extraFormats.add(AliasFormat.ofSpecification(registeredFormats,
                                                            formatSpec.getKey(),
                                                            formatSpec.getValue()));
            }
         }
         return extraFormats;
      }

      /**
       * Creates the restricted format list to use from CLI format specification.
       *
       * @param registeredFormats registered formats indexed by {@link Format#names()} (including
       *       aliases)
       * @param formatSpecs format specification. Map value is {@code null} if format is not an alias
       *
       * @return format list, or an empty list to do not restrict formats
       */
      private static List<Format> restrictedFormats(Map<String, Format> registeredFormats,
                                                    Map<String, String> formatSpecs) {
         List<Format> restrictedFormats = list();
         for (Entry<String, String> formatSpec : formatSpecs.entrySet()) {
            if (formatSpec.getValue() == null) {
               var name = formatSpec.getKey();
               var format = registeredFormats.get(name);
               if (format == null) {
                  throw new IllegalStateException(format("Unknown '%s' format name", name));
               } else {
                  restrictedFormats.add(format);
               }
            }
         }

         return restrictedFormats;
      }

   }

   /**
    * Loads {@code git.properties} file entries as new system properties.
    */
   private static void loadGitProperties() {
      try (InputStream gitPropertiesStream = Thread
            .currentThread()
            .getContextClassLoader()
            .getResourceAsStream("git-projection.properties")) {
         if (gitPropertiesStream != null) {
            Properties gitProperties = new Properties();
            gitProperties.load(gitPropertiesStream);
            gitProperties.forEach((k, v) -> System.setProperty(k.toString(), v.toString()));
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Manages uncaught exception.
    *
    * @param e uncaught exception
    */
   public static void generalError(Exception e) {
      System.err.println("[E] " + e.getMessage());
      if (log.isDebugEnabled()) {
         log.debug(e.getMessage(), e);
      }
      exit(1);
   }

   private static List<Attribute> attributes(Map<String, String> attributes) {
      return list(map(attributes).entrySet().stream().map(e -> Attribute.of(e.getKey(), e.getValue())));
   }

   private ColorScheme colorScheme() {
      return new ColorScheme.Builder()
            .commands(Style.fg_green, Style.bold)
            .options(Style.fg_cyan)
            .parameters(Style.fg_cyan)
            .optionParams(Style.italic)
            .errors(Style.fg_red, Style.bold)
            .stackTraces(Style.italic)
            .build();
   }

   public static void verboseLevel(ExtendedLogger.Level level) {
      LOG_ROOTS.forEach(root -> {
         ExtendedLogger.setLoggerLevel(root, level);
      });
   }

}

