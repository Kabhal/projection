#!/bin/sh

set -ex

test -z "$MODULES" && MODULES="projection-core projection-cli"

mvn install -DskipTests
for module in $MODULES; do
  mvn -DenableNativeAgent test -pl ":${module}"
done
